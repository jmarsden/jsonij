/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Set;

/**
 * Value is extended by every JSON internalType implementation. This class
 * provides generic access to all values to make life a little easier when
 * traversing the JSON Document.
 *
 * @author J.W.Marsden
 */
public abstract class Value implements Serializable, Comparable<Value> {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 2774919263250085887L;
	/**
	 * Current Value Type
	 */
	protected ValueType valueType;

	/**
	 * Constructor for Value. As a Value constructs it must have a internalType or
	 * it cannot construct.
	 */
	public Value() {
		this.valueType = internalType();
		if (this.valueType == null) {
			throw new NullPointerException("type() method must be implemented and return a valid type.");
		}
	}

	/**
	 * Internal Method to find the internalType for the Object. Must be implemented
	 * and not return null.
	 *
	 * @return The internalType of this JSON Value.
	 */
	public abstract ValueType internalType();

	/**
	 * Accessor for the Value TYPE.
	 *
	 * @return TYPE The value valueType.
	 */
	public ValueType getValueType() {
		return valueType;
	}

	/**
	 * Accessor for the Value TYPE.
	 *
	 * @return TYPE The value valueType.
	 */
	public ValueType type() {
		return valueType;
	}

	/**
	 * Retrieves the size of the Value. If string internalType, this will return the
	 * length of the String. If this internalType is an Array or an Object then it
	 * will return the number of elements in the Object. If this Value is not a
	 * String, Array or Object then this will return -1.
	 *
	 * @return size of the Value or -1 if this Value has no size.
	 */
	public int size() {
		switch (valueType) {
		case STRING:
			return ((JSON.String) this).length();
		case ARRAY:
			return ((JSON.Array<?>) this).size();
		case OBJECT:
			return ((JSON.Object<?, ?>) this).size();
		default:
			return -1;
		}
	}

	/**
	 * Finds the nested elements under this Value. This is effectively the count of
	 * all JSON Values attached to this Value. This number does not include this
	 * value itself.
	 *
	 * @return int The count of all Values attached to this Value.
	 */
	public abstract int nestedSize();

	public boolean isNull() {
		switch (valueType) {
		case NULL:
			return true;
		default:
			return !getBoolean();
		}
	}

	/**
	 * Finds the boolean representation for the Value. If the value is JSON.TRUE or
	 * JSON.FALSE then those booleans are returned. If the value is Numeric then
	 * true is returned for all values that are not zero. If the value is a String
	 * then an empty String returns 0. If the value is an Object or Array then all
	 * sizes that are not zero return true.
	 *
	 * @return boolean The boolean for the Value.
	 */
	public boolean getBoolean() {
		boolean result = false;
		switch (valueType) {
		case TRUE:
			result = true;
			break;
		case FALSE:
			result = false;
			break;
		case NUMERIC:
			result = ((JSON.Numeric) this).intValue() != 0;
			break;
		case STRING:
			result = ((JSON.String) this).length() != 0;
			break;
		case ARRAY:
			result = !((JSON.Array<?>) this).isEmpty();
			break;
		case OBJECT:
			result = !((JSON.Object<?, ?>) this).isEmpty();
			break;
		default:
			result = false;
		}
		return result;
	}

	/**
	 * Finds the int representation for the Value. Returns 1 when the value is JSON
	 * True and 0 when the value is JSON False. When the value is Numeric it will
	 * return the intValue from Number. If the value is a String then an attempt is
	 * made to parse the String value into an integer and return it. All other types
	 * return -1.
	 *
	 * @return int The int for the Value.
	 */
	public int getInt() {
		int result;
		switch (valueType) {
		case NUMERIC:
			result = ((JSON.Numeric) this).intValue();
			break;
		case TRUE:
			result = 1;
			break;
		case FALSE:
			result = 0;
			break;
		case STRING:
			result = Integer.parseInt((this).toString());
			break;
		default:
			result = -1;
		}
		return result;
	}

	/**
	 * Finds the double representation for the Value. Returns 1D when the value is
	 * JSON True and 0D when the value is JSON False. When the value is Numeric it
	 * will return the doubleValue from Number. If the value is a String then an
	 * attempt is made to parse the String value into an Double and return it. All
	 * other types return -1D.
	 *
	 * @return double The double value for the Value.
	 */
	public double getDouble() {
		double result;
		switch (valueType) {
		case NUMERIC:
			result = ((JSON.Numeric) this).doubleValue();
			break;
		case TRUE:
			result = 1D;
			break;
		case FALSE:
			result = 0D;
			break;
		case STRING:
			result = Double.parseDouble((this).toString());
			break;
		default:
			result = -1D;
		}
		return result;
	}

	/**
	 * Finds the long representation for the Value. Returns 1L when the value is
	 * JSON True and 0L when the value is JSON False. When the value is Numeric it
	 * will return the doubleValue from Number. If the value is a String then an
	 * attempt is made to parse the String value into an Long and return it. All
	 * other types return -1L.
	 *
	 * @return long The long value for the Value.
	 */
	public long getLong() {
		long result;
		switch (valueType) {
		case NUMERIC:
			result = ((JSON.Numeric) this).longValue();
			break;
		case TRUE:
			result = 1L;
			break;
		case FALSE:
			result = 0L;
			break;
		case STRING:
			result = Long.parseLong((this).toString());
			break;
		default:
			result = -1L;
		}
		return result;
	}

	/**
	 * Finds the Number representation for the Value. Returns 1D when the value is
	 * JSON True and 0D when the value is JSON False. When the value is Numeric it
	 * will return the Number. If the value is a String then an attempt is made to
	 * parse the String value into an Double and return it. All other types return
	 * -1D.
	 *
	 * @return double The double value for the Value.
	 */
	public Number getNumber() {
		Number result;
		switch (valueType) {
		case NUMERIC:
			result = ((JSON.Numeric) this).getNumber();
			break;
		case TRUE:
			result = 1D;
			break;
		case FALSE:
			result = 0D;
			break;
		case STRING:
			result = Double.parseDouble((this).toString());
			break;
		default:
			result = -1D;
			break;
		}
		return result;
	}

	/**
	 * Finds the String representation for the Value. When the value is true or
	 * false it will return "true" or "false" respectively. If the value is Numeric
	 * it will return the toString() version of the Number instance or the String
	 * itself. All other values return null.
	 *
	 * @return String The String value for the Value.
	 */
	public String getString() {
		String result;
		switch (valueType) {
		case STRING:
			result = (this).toString();
			break;
		case TRUE:
			result = "true";
			break;
		case FALSE:
			result = "false";
			break;
		case NUMERIC:
			result = ((JSON.Numeric) this).toString();
			break;
		case OBJECT:
			result = ((JSON.Object<?, ?>) this).toString();
			break;
		case ARRAY:
			result = ((JSON.Array<?>) this).toString();
			break;
		default:
			result = null;
		}
		return result;
	}

	/**
	 * Extracts a Value instance at an Index. This method only returns values when
	 * the internalType is OBJECT or ARRAY. All other cases will return null.
	 *
	 * @param i The index to get the value for.
	 * @return Value The Value at the index or null.
	 */
	public Value get(int i) {
		Value result;
		switch (valueType) {
		case ARRAY:
			result = ((JSON.Array<?>) this).get(i);
			break;
		case OBJECT:
			result = ((JSON.Object<?, ?>) this).get(i);
			break;
		default:
			result = null;
		}
		return result;
	}

	/**
	 * Tests if there is a Value at the specified key. This method only responds
	 * when the Value is of internalType OBJECT. All other cases will return false.
	 *
	 * @param key Key to extract the value from.
	 * @return true when there is a value at the key.
	 */
	public boolean has(String key) {
		boolean result;
		switch (valueType) {
		case OBJECT:
			result = ((JSON.Object<?, ?>) this).containsKey(new JSON.String(key));
			break;
		default:
			result = false;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public Set<CharSequence> valueKeySet() {
		Set<CharSequence> keys;
		switch (valueType) {
		case OBJECT:
			keys = ((JSON.Object<CharSequence, Value>) this).mapValue.keySet();
			break;
		default:
			keys = null;
		}
		return keys;
	}

	/**
	 * Gets a Value at a key for the current Value. This method only responds when
	 * the Value is of internalType OBJECT. All other cases will return null.
	 *
	 * @param key Key to extract the value from.
	 * @return Value The value at the given Key.
	 */
	public Value get(CharSequence key) {
		Value result;
		switch (valueType) {
		case OBJECT:
			result = ((JSON.Object<?, ?>) this).safeGet(key.toString());
			break;
		default:
			result = null;
		}
		return result;
	}

	/**
	 * Converts the current Value into a JSON String that represents it.
	 *
	 * @return The JSON Value as JSON String.
	 */
	public abstract String toJSON();

	public abstract ByteBuffer toBSON(ByteBuffer buffer);

	public abstract ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer);

	/**
	 * Default toString for a JSON Value. Returns the JSON string for the Value.
	 *
	 * @return json String
	 */
	@Override
	public String toString() {
		return toJSON();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Value o = (Value) obj;
		boolean equalsResult = false;
		if (null != internalType()) {
			switch (internalType()) {
			case OBJECT:
				if (o.internalType() == ValueType.OBJECT) {
					if (o.size() == size()) {
						equalsResult = true;
						Iterator<CharSequence> keySetIterator = valueKeySet().iterator();
						while (keySetIterator.hasNext()) {
							CharSequence key = keySetIterator.next();
							Value thisValue = get(key);
							Value otherValue = o.get(key);
							if (otherValue != null) {
								if (!thisValue.equals(otherValue)) {
									equalsResult = false;
									break;
								}
							}
						}
					} else {
						equalsResult = false;
					}
				} else {
					equalsResult = toString().equals(o.toString());
				}
				break;
			case ARRAY:
				if (o.internalType() == ValueType.ARRAY) {
					if (o.size() == size()) {
						equalsResult = true;
						for (int i = 0; i < size(); i++) {
							if (!get(i).equals(o.get(i))) {
								equalsResult = false;
								break;
							}
						}
					} else {
						equalsResult = false;
					}
				} else {
					equalsResult = toString().equals(o.toString());
				}
				break;
			case NULL:
				equalsResult = (isNull() && o.isNull());
				break;
			case TRUE:
			case FALSE:
				equalsResult = (getBoolean() == o.getBoolean());
				break;
			case NUMERIC:
				Number thisNumber = getNumber();
				Number otherNumber = o.getNumber();
				if (thisNumber != null && otherNumber != null) {
					equalsResult = thisNumber.equals(otherNumber);
				}
				break;
			case STRING:
				equalsResult = getString().equals(o.getString());
				break;
			default:
				break;
			}
		}
		return equalsResult;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 13 * hash + (this.valueType != null ? this.valueType.hashCode() : 0);
		if (null != internalType()) {
			switch (internalType()) {
			case OBJECT:
				for (int i = 0; i < size(); i++) {
					hash = 13 * hash + (get(i).hashCode());
				}
				break;
			case ARRAY:
				for (int i = 0; i < size(); i++) {
					hash = 13 * hash + (get(i).hashCode());
				}
				break;
			case NULL:
				hash = 13 * hash + (JSON.NULL.hashCode());
				break;
			case TRUE:
				hash = 13 * hash + (JSON.TRUE.hashCode());
				break;
			case FALSE:
				hash = 13 * hash + (JSON.FALSE.hashCode());
				break;
			case NUMERIC:
				hash = 13 * hash + (getNumber().hashCode());
				break;
			case STRING:
				hash = (getString() != null) ? getString().hashCode() : 0;
				break;
			default:
				break;
			}
		}
		return hash;
	}

	@Override
	public int compareTo(Value o) {
		int compare = 0;
		if (null != internalType()) {
			switch (internalType()) {
			case OBJECT:
				if (o.internalType() == ValueType.NULL || o.internalType() == ValueType.TRUE
						|| o.internalType() == ValueType.FALSE) {
					compare = 1;
				} else {
					compare = toJSON().compareTo(o.toJSON());
				}
				break;
			case ARRAY:
				if (o.internalType() == ValueType.NULL || o.internalType() == ValueType.TRUE
						|| o.internalType() == ValueType.FALSE || o.internalType() == ValueType.OBJECT) {
					compare = 1;
				} else {
					compare = toJSON().compareTo(o.toJSON());
				}
				break;
			case NULL:
				if (o.internalType() == ValueType.NULL) {
					compare = 0;
				} else {
					compare = -1;
				}
				break;
			case TRUE:
				if (o.internalType() == ValueType.NULL) {
					compare = 1;
				} else if (o.internalType() == ValueType.TRUE) {
					compare = 0;
				} else {
					compare = -1;
				}
				break;
			case FALSE:
				if (o.internalType() == ValueType.NULL) {
					compare = 1;
				} else if (o.internalType() == ValueType.TRUE) {
					compare = 1;
				} else if (o.internalType() == ValueType.FALSE) {
					compare = 0;
				} else {
					compare = -1;
				}
				break;
			case NUMERIC:
				double thisDouble = getDouble();
				double thatDouble = o.getDouble();
				if (thisDouble < thatDouble) {
					compare = -1;
				} else if (thisDouble == thatDouble) {
					compare = 0;
				} else if (thisDouble > thatDouble) {
					compare = 1;
				}
				break;
			case STRING:
				compare = getString().compareTo(o.getString());
				break;
			default:
				break;
			}
		}
		return compare;
	}
}
