/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * JSON String Implementation.
 *
 * @author J.W.Marsden
 */
public class StringImp extends Value implements Serializable, CharSequence {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -9098386245749540342L;

	public final Charset UTF8_CHARSET = Charset.forName("UTF-8");

	/**
	 * Container for the String.
	 */
	protected String value;

	/**
	 * Constructor using java.lang.String.
	 *
	 * @param orig The original String for this Value
	 */
	public StringImp(String orig) {
		if (orig == null) {
			value = "";
		} else {
			value = orig;
		}
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#internalType()
	 */
	@Override
	public ValueType internalType() {
		return ValueType.STRING;
	}

	/*
	 * (non-Javadoc) @see java.lang.CharSequence#charAt(int)
	 */
	@Override
	public char charAt(int index) {
		return value.charAt(index);
	}

	/*
	 * (non-Javadoc) @see java.lang.CharSequence#length()
	 */
	@Override
	public int length() {
		return value.length();
	}

	/*
	 * (non-Javadoc) @see java.lang.CharSequence#subSequence(int, int)
	 */
	@Override
	public CharSequence subSequence(int start, int end) {
		return value.subSequence(start, end);
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof CharSequence || o instanceof JSON.String) {
			return o.toString().equals(value);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (value != null) ? value.hashCode() : 0;
	}

	/*
	 * (non-Javadoc) @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return value;
	}

	@Override
	public int nestedSize() {
		return 0;
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#toJSON()
	 */
	@Override
	public String toJSON() {
		StringBuilder outputStringBuilder = new StringBuilder();
		outputStringBuilder.append('"');
		char c;
		for (int i = 0; i < value.length(); i++) {
			c = value.charAt(i);
			switch (c) {
			case '"':
				outputStringBuilder.append("\\\"");
				break;
			case '\\':
				outputStringBuilder.append("\\\\");
				break;
			case '/':
				outputStringBuilder.append("\\/");
				break;
			case '\b':
				outputStringBuilder.append("\\b");
				break;
			case '\f':
				outputStringBuilder.append("\\f");
				break;
			case '\n':
				outputStringBuilder.append("\\n");
				break;
			case '\r':
				outputStringBuilder.append("\\r");
				break;
			case '\t':
				outputStringBuilder.append("\\t");
				break;
			default:
				int a = (int) c;
				/**
				 * TODO: Make Constants.
				 */
				if (a == 32 || a == 33 || ((a >= 35) && (a <= 91)) || ((a >= 93) && (a <= 127))) {
					outputStringBuilder.append(c);
				} else {
					String hex = Integer.toHexString(a);
					outputStringBuilder.append("\\u");
					for (int j = hex.length(); j < 4; j++) {
						outputStringBuilder.append("0");
					}
					outputStringBuilder.append(hex);
				}
				break;
			}
		}
		outputStringBuilder.append('"');
		return outputStringBuilder.toString();
	}

	/**
	 * Convert to a JSON ByteBuffer.
	 *
	 * @param buffer The buffer to convert to String
	 * @return The String
	 */
	@Override
	public ByteBuffer toBSON(ByteBuffer buffer) {
		BSON.CODEC.encodeUTF8String(buffer, value);
		return buffer;
	}

	@Override
	public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
		UBJSON.CODEC.encodeString(buffer, value);
		return buffer;
	}
}
