/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import static jsonij.ObjectIdentifier.ISO8601_FORMAT;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import jsonij.parser.BSONParserException;

public class BSON extends JSON {

	public static BSONCodec CODEC;

	public static final MinKey MIN_KEY;
	public static final MinKey MAX_KEY;
	public static final Undefined1 UNDEFINED_1;

	static {
		CODEC = new BSONCodec();

		MIN_KEY = new BSON.MinKey();
		MAX_KEY = new BSON.MinKey();

		UNDEFINED_1 = new BSON.Undefined1();
	}

	public BSON(Value root) {
		super(root);
	}

	public static ByteBuffer allocateBuffer(int size) {
		return allocateBuffer(size, true);
	}

	public static ByteBuffer allocateBuffer(int size, boolean isDirect) {
		ByteBuffer buffer;
		if (isDirect) {
			buffer = ByteBuffer.allocateDirect(size);
		} else {
			buffer = ByteBuffer.allocate(size);
		}
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		return buffer;
	}

	public static void deallocateBuffer(ByteBuffer buffer) {
		buffer.clear();
	}

	public static byte[] hexStringToByteArray(java.lang.String s) {
		return hexStringToByteArray(s, true);
	}

	public static byte[] hexStringToByteArray(java.lang.String s, boolean cleanSpaces) {
		if (cleanSpaces && s.indexOf(' ') != -1) {
			s = s.replaceAll(" ", "");
		}
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public ByteBuffer toBSON(ByteBuffer buffer) throws BSONParserException {
		if (root.getValueType() != ValueType.OBJECT) {
			throw new BSONParserException("Root type must be OBJECT");
		}
		buffer.rewind();
		return buffer;
	}

	@Override
	public java.lang.String toString() {
		return "BSON@" + Integer.toHexString(hashCode()) + ":" + getRoot().toString();
	}

	public static final class ObjectID extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 2458791087064626114L;

		public ObjectIdentifier objectID;

		public ObjectID(ObjectIdentifier objectID) {
			this.objectID = objectID;
		}

		public ObjectIdentifier getObjectID() {
			return objectID;
		}

		public void setObjectID(ObjectIdentifier objectID) {
			this.objectID = objectID;
		}

		@Override
		public ValueType internalType() {
			return ValueType.OBJECT_ID;
		}

		@Override
		public int nestedSize() {
			return ObjectIdentifier.LENGTH;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$oid\":\"%s\"}", objectID.toString());
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			return buffer.put(objectID.getContents());
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class JavaScript extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 8584810273835952483L;

		java.lang.String jsCode;

		public JavaScript(java.lang.String jsCode) {
			this.jsCode = jsCode;
		}

		@Override
		public ValueType internalType() {
			return ValueType.JAVA_SCRIPT_CODE;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$js\":\"%s\"}", jsCode);
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			return CODEC.encodeUTF8String(buffer, jsCode);
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * BSON Regular Expression Implementation.
	 *
	 * BSON ByteCode: <b>\x0B</b>.
	 */
	public static final class RegularExpression extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 6905337153787601861L;

		/**
		 * Regular Expression
		 */
		java.lang.String expression;

		/**
		 * Regular Expression Options.
		 */
		java.lang.String options;

		public RegularExpression(java.lang.String expression, java.lang.String options) {
			this.expression = expression;
			this.options = options;
		}

		@Override
		public ValueType internalType() {
			return ValueType.REGEX;
		}

		@Override
		public int nestedSize() {
			return 1 + expression.length() + 1 + ((options != null) ? options.length() : 0);
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$regex\":\"%s\", \"$opt\":\"%s\"}", expression, options);
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			CODEC.encodeUTF8String(buffer, expression);
			CODEC.encodeUTF8String(buffer, options);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * BSON JavaScript with Scope Implementation.
	 *
	 * BSON ByteCode: <b>\x0F</b>.
	 */
	public static final class JavaScriptWithScope extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 3715743229597512644L;
		/**
		 * JavaScript Code.
		 */
		java.lang.String jsCode;
		/**
		 * Scope Document.
		 */
		BSON.Object<?, ?> document;

		public JavaScriptWithScope(java.lang.String jsCode, BSON.Object<?, ?> document) {
			this.jsCode = jsCode;
			this.document = document;
		}

		@Override
		public ValueType internalType() {
			return ValueType.JAVA_SCRIPT_CODE_WS;
		}

		@Override
		public int nestedSize() {
			return 5 + jsCode.length() + document.nestedSize();
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$js\":\"%s\", \"$doc\":\"%s\"}", jsCode, document.toJSON());
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			CODEC.encodeCString(buffer, jsCode);
			document.toBSON(buffer);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class UTCDateTime extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -331695384446895828L;

		java.util.Date date;

		public UTCDateTime(long dateTime) {
			date = new java.util.Date(dateTime);
		}

		@Override
		public ValueType internalType() {
			return ValueType.UTC_DATE_TIME;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return (date != null) ? java.lang.String.format("\"%s\"", ISO8601_FORMAT.format(date)) : "null";
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			CODEC.encodeUTCDateTime(buffer, date.getTime());
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class Int32 extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -8623259974661974258L;
		int int32;

		public Int32(int int32) {
			this.int32 = int32;
		}

		@Override
		public ValueType internalType() {
			return ValueType.INT32;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return "" + int32;
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			CODEC.encodeInt32(buffer, int32);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class Int64 extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -150426236159560886L;

		long int64;

		public Int64(long int64) {
			this.int64 = int64;
		}

		@Override
		public ValueType internalType() {
			return ValueType.INT64;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$numberLong\":\"%s\"}", int64);
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			CODEC.encodeInt64(buffer, int64);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class Decimal extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -150426236159560887L;

		Decimal128 decimal128;

		public Decimal(Decimal128 decimal128) {
			this.decimal128 = decimal128;
		}

		@Override
		public ValueType internalType() {
			return ValueType.DECIMAL128;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$numberDecimal\":\"%s\"}", decimal128.toString());
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			CODEC.encodeDecimal128(buffer, this);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class Timestamp extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -7905721243903837708L;

		BSONTimestamp timestamp;

		public Timestamp(BSONTimestamp timestamp) {
			this.timestamp = timestamp;
		}

		@Override
		public ValueType internalType() {
			return ValueType.TIMESTAMP;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$timestamp\":{\"t\":%s,\"i\":%s}}", timestamp.getTime(),
					timestamp.getIncrement());
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			return CODEC.encodeTimestamp(buffer, timestamp);
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static class BinaryData extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -470686043986109846L;

		int size;
		byte type;
		ByteBuffer data;

		public BinaryData(int size, byte type, ByteBuffer data) {
			this.size = size;
			this.type = type;
			this.data = data;
		}

		@Override
		public ValueType internalType() {
			return ValueType.BINARY_DATA;
		}

		@Override
		public int nestedSize() {
			return size + 1;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$tp\":\"%s\",\"$s\":%s,\"$d\":[]}", type, size);
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			return CODEC.encodeBinaryData(buffer, this);
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class Undefined1 extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -7698699876171548324L;

		@Override
		public ValueType internalType() {
			return ValueType.UNDEFINED_1;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return "\"Undefined_1\"";
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			buffer.put(Constants.BSON_UNDEFINED_1);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class Undefined2 extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -144895138458224358L;

		java.lang.String value;

		public Undefined2(java.lang.String value) {
			this.value = value;
		}

		@Override
		public ValueType internalType() {
			return ValueType.UNDEFINED_2;
		}

		@Override
		public int nestedSize() {
			return 1;
		}

		@Override
		public java.lang.String toJSON() {
			return "\"Undefined_2\\" + value + "\"";
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			buffer.put(Constants.BSON_UNDEFINED_2);
			CODEC.encodeUTF8String(buffer, value);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class MinKey extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 3379267581398348546L;

		@Override
		public ValueType internalType() {
			return ValueType.MIN_KEY;
		}

		@Override
		public int nestedSize() {
			return 0;
		}

		@Override
		public java.lang.String toJSON() {
			return "\"MinKey\"";
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			buffer.put(Constants.BSON_MIN_KEY);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class MaxKey extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -1376756107799927028L;

		@Override
		public ValueType internalType() {
			return ValueType.MAX_KEY;
		}

		@Override
		public int nestedSize() {
			return 0;
		}

		@Override
		public java.lang.String toJSON() {
			return "\"MaxKey\"";
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			buffer.put(Constants.BSON_MAX_KEY);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}

	public static final class DBPointer extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 8209568600602210064L;

		public ObjectIdentifier objectID;

		public DBPointer(ObjectIdentifier objectID) {
			this.objectID = objectID;
		}

		public ObjectIdentifier getObjectID() {
			return objectID;
		}

		public void setObjectID(ObjectIdentifier objectID) {
			this.objectID = objectID;
		}

		@Override
		public ValueType internalType() {
			return ValueType.OBJECT_ID;
		}

		@Override
		public int nestedSize() {
			return ObjectIdentifier.LENGTH;
		}

		@Override
		public java.lang.String toJSON() {
			return java.lang.String.format("{\"$oid\":\"%s\"}", objectID.toString());
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			return buffer.put(objectID.getContents());
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			throw new UnsupportedOperationException();
		}
	}
}
