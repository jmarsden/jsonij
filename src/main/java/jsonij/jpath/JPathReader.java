/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.io.IOException;
import jsonij.ConstantUtility;
import jsonij.parser.BaseJSONReader;
import jsonij.parser.JSONReader;
import jsonij.parser.ParserException;

/**
 * JPathReader handles the JPath Reading including skipping whitespace.
 */
public class JPathReader extends BaseJSONReader implements JSONReader {

	String jPathString;
	int index;

	public JPathReader(String jpathString) {
		this.jPathString = jpathString;
		index = 0;
	}

	public String getJPath() {
		return jPathString;
	}

	@Override
	public int readNext() throws ParserException {
		if (index < jPathString.length()) {
			return jPathString.charAt(index++);
		} else {
			return -1;
		}
	}

	public void skipWhitepace() throws IOException, ParserException {
		while (ConstantUtility.isWhiteSpace(peek())) {
			read();
		}
	}

	public void skipWhitepace(StringBuilder appender) throws ParserException {
		while (ConstantUtility.isWhiteSpace(peek())) {
			appender.append((char) read());
		}
	}

	@Override
	public JSONReader getStringReader(Mode mode) {
		throw new UnsupportedOperationException();
	}
}
