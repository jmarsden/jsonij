/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.util.List;

import jsonij.Value;
import jsonij.ValueType;

public class KeyComponent extends Component {

	private String value;
	private int size;

	public KeyComponent(String value) {
		this.value = value;
		this.size = value.length();
	}
	
	public KeyComponent(String value, int size) {
		this.value = value;
		this.size = size;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
		this.size = value.length();
	}
	
	public int getSize() {
		return this.size;
	}

	@Override
	public String toString() {
		return String.format("KeyComponent [%s]", value);
	}

	@Override
	public List<Value> evaluate(List<Value> values, List<Value> results) {
		Value valueStore = null;
		for (Value currentValue : values) {
			String keyValue = getValue();
			if (currentValue.getValueType() == ValueType.OBJECT) {
				valueStore = currentValue.get(keyValue);
				if (valueStore != null) {
					results.add(valueStore);
				}
			} else if (currentValue.getValueType() == ValueType.ARRAY) {
				for (int j = 0; j < currentValue.size(); j++) {
					valueStore = currentValue.get(j).get(keyValue);
					if (valueStore != null) {
						results.add(valueStore);
					}
				}
			}
		}
		return results;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final KeyComponent other = (KeyComponent) obj;
		return this.value.equals(other.value);
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 53 * hash + (this.value != null ? this.value.hashCode() : 0);
		return hash;
	}
}
