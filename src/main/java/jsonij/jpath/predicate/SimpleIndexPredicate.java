/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath.predicate;

import jsonij.jpath.PredicateComponent;
import java.util.List;

import jsonij.Value;

/*
 * SimpleIndex Predicate Implementation. The most basic of the Predicates.
 * Results in a single specified index being retrieved during evaluation.
 *
 * @author J.W.Marsden.
 */
public class SimpleIndexPredicate extends PredicateComponent {

	public static final int LAST_INDEX;
	int index;

	static {
		LAST_INDEX = -1;
	}

	public SimpleIndexPredicate() {
		index = 0;
	}

	public SimpleIndexPredicate(int index) {
		this.index = index;
	}

	public boolean isLast() {
		return index == LAST_INDEX;
	}

	public void setLast() {
		this.index = LAST_INDEX;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public List<Value> evaluate(List<Value> values, List<Value> results) {
		if (isLast()) {
			int indexPredicate = -1;
			for (Value value : values) {
				if ((indexPredicate = value.size() - 1) >= 0) {
					results.add(value.get(indexPredicate));
				}
			}
		} else {
			for (Value value : values) {
				if (getIndex() >= 0 && getIndex() < value.size()) {
					results.add(value.get(getIndex()));
				} else if (getIndex() < 0 && (value.size() + getIndex()) >= 0) {
					results.add(value.get(value.size() + getIndex()));
				}
			}
		}
		return results;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SimpleIndexPredicate other = (SimpleIndexPredicate) obj;
		if (this.index != other.index) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 53 * hash + this.index;
		return hash;
	}
}
