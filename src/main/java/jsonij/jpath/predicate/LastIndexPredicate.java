/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath.predicate;

import jsonij.jpath.PredicateComponent;
import java.util.List;

import jsonij.Value;

/**
 * LastIndexPredicate Predicate Implementation. Results in querying the last
 * element of an array during evaluation.
 *
 * @author J.W.Marsden.
 */
public class LastIndexPredicate extends PredicateComponent {

	int offset;
	public static final LastIndexPredicate LAST;

	static {
		LAST = new LastIndexPredicate();
	}

	public LastIndexPredicate() {
		this(0);
	}

	public LastIndexPredicate(int offset) {
		this.offset = Math.abs(offset);
	}

	public boolean hasOffset() {
		return offset != 0;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final LastIndexPredicate other = (LastIndexPredicate) obj;
		if (this.offset != other.offset) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 71 * hash + this.offset;
		return hash;
	}

	@Override
	public List<Value> evaluate(List<Value> values, List<Value> results) {
		if (!hasOffset()) {
			int indexPredicate = -1;
			for (Value value : values) {
				if ((indexPredicate = value.size() - 1) >= 0) {
					results.add(value.get(indexPredicate));
				}
			}
		} else {
			int indexPredicate = -1;
			for (Value value : values) {
				if ((indexPredicate = value.size() - getOffset()) >= 0) {
					results.add(value.get(indexPredicate));
				}
			}
		}
		return results;
	}

}
