/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath.predicate;

import jsonij.jpath.PredicateComponent;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import jsonij.Value;

/**
 * JPath Union Predicate Implementation. This predicate allows a comma separated
 * zero based list of indexes for JPath evaluation. For example, [0,2] will
 * result in the first and third indexes of an array being evaluated in the
 * JPath. Indexes must be <code>0 &lt;= x &lt;= R</code>.
 */
public class UnionPredicate extends PredicateComponent {

	public final int CAPACITY_GROWTH_FACTOR = 10;
	int capacity;
	int current;
	int[] indexes;

	public UnionPredicate() {
		capacity = 10;
		current = 0;
		this.indexes = new int[capacity];
	}

	public UnionPredicate(int[] indexes) {
		this.capacity = Array.getLength(indexes);
		this.current = capacity;
		this.indexes = indexes;
	}

	public int[] getIndexes() {
		if (capacity == current) {
			return indexes;
		} else {
			int[] result = new int[current];
			System.arraycopy(indexes, 0, result, 0, current);
			return result;
		}
	}

	public void setIndexes(int[] indexes) {
		this.capacity = Array.getLength(indexes);
		this.current = capacity;
		this.indexes = indexes;
	}

	public boolean containsIndex(int index) {
		boolean result = false;
		for (int i = 0; i < current; i++) {
			if (indexes[i] == index) {
				result = true;
				break;
			}
		}
		return result;
	}

	public void addIndex(int index) {
		if (!containsIndex(index)) {
			if (current < capacity) {
				indexes[current] = index;
				current++;
			} else {
				int[] newIndexes = new int[capacity + CAPACITY_GROWTH_FACTOR];
				System.arraycopy(indexes, 0, newIndexes, 0, current);
				capacity = capacity + CAPACITY_GROWTH_FACTOR;
				newIndexes[current] = index;
				indexes = newIndexes;
				current++;
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UnionPredicate other = (UnionPredicate) obj;
		if (!Arrays.equals(this.getIndexes(), other.getIndexes())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 29 * hash + Arrays.hashCode(this.getIndexes());
		return hash;
	}

	@Override
	public List<Value> evaluate(List<Value> values, List<Value> results) {
		for (Value value : values) {
			int currentValueSize = value.size();
			for (int j = 0; j < Array.getLength(indexes); j++) {
				if (indexes[j] >= 0 && indexes[j] < currentValueSize) {
					results.add(value.get(indexes[j]));
				} else if (indexes[j] < 0 && (currentValueSize - indexes[j]) >= 0) {
					results.add(value.get(currentValueSize + indexes[j]));
				}
			}
		}
		return results;
	}
}
