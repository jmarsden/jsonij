/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath.predicate;

import java.util.ArrayList;
import java.util.List;

import jsonij.Value;
import jsonij.jpath.JPathParserException;
import jsonij.parser.JSONParser;
import jsonij.parser.ParserException;
import jsonij.parser.StringJSONReader;

public class FunctionArgument {

	public static enum ARGUMENT_TYPE {

		ATTRIBUTE, VALUE
	}

	ARGUMENT_TYPE type;

	public ARGUMENT_TYPE getType() {
		return type;
	}

	public void setType(ARGUMENT_TYPE type) {
		this.type = type;
	}

	public static class AttributeArgument extends FunctionArgument {

		String attributeName;

		public AttributeArgument(String attributeName) {
			this.attributeName = attributeName;
			this.type = ARGUMENT_TYPE.ATTRIBUTE;
		}

		public String getAttributeName() {
			return attributeName;
		}

		public void setAttributeName(String attributeName) {
			this.attributeName = attributeName;
		}

		@Override
		public String toString() {
			return String.format("@.%s", attributeName);
		}

	}

	public static class ValueArgument extends FunctionArgument {

		Value value;

		public ValueArgument(Value value) {
			this.value = value;
			this.type = ARGUMENT_TYPE.VALUE;
		}

		public Value getValue() {
			return value;
		}

		public void setValue(Value value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value.toJSON();
		}
	}

	public static FunctionArgument[] parseStringToArguments(String string) throws JPathParserException {
		FunctionArgument[] argumentArray = null;
		List<FunctionArgument> argumentList = new ArrayList<FunctionArgument>();
		int stringLength = string.length();
		int i = 0;
		while (i < stringLength - 1) {
			char c = string.charAt(i);
			while (i < stringLength - 1 && c == ' ') {
				c = string.charAt(++i);
			}
			if (c == '@') {
				if (i < stringLength - 1) {
					c = string.charAt(++i);
				}
				if (c == '.') {
					StringBuilder attributebuilder = new StringBuilder();
					if (i < stringLength - 1) {
						c = string.charAt(++i);
					}
					while (c != ',') {
						attributebuilder.append(c);
						if (i < stringLength - 1) {
							c = string.charAt(++i);
						} else {
							break;
						}
					}
					if (i < stringLength - 1) {
						c = string.charAt(++i);
					} else {
						break;
					}
					AttributeArgument argument = new AttributeArgument(attributebuilder.toString().trim());
					argumentList.add(argument);
				} else {
					throw new JPathParserException("functionPredicateAttributeParse", 0, 0, '.');
				}
			} else {
				try {
					StringJSONReader targetReader = new StringJSONReader(string, i);
					JSONParser valueParser = new JSONParser(targetReader);
					Value value = valueParser.parseValue();
					i = targetReader.getIndex();
					ValueArgument argument = new ValueArgument(value);
					argumentList.add(argument);
				} catch (ParserException ex) {
					throw new JPathParserException("functionPredicateValueParse", 0, 0, ex);
				}
			}
		}
		argumentArray = new FunctionArgument[argumentList.size()];
		argumentList.toArray(argumentArray);
		return argumentArray;
	}
}
