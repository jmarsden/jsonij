/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath.predicate;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jsonij.Value;
import jsonij.ValueType;
import jsonij.jpath.JPathRuntimeException;
import jsonij.jpath.predicate.FunctionArgument.ARGUMENT_TYPE;
import jsonij.jpath.predicate.FunctionArgument.AttributeArgument;
import jsonij.jpath.predicate.FunctionArgument.ValueArgument;

/**
 * @author jmarsden
 */
public class RegexFunction extends Function {

	public RegexFunction() {
		this.functionName = "regex";
		this.argumentTypeList = new ArrayList<Class<? extends FunctionArgument>>();
		this.argumentTypeList.add(AttributeArgument.class);
		this.argumentTypeList.add(ValueArgument.class);
	}

	@Override
	public Value evaluate(FunctionArgument[] args, Value value) {
		if (Array.getLength(args) != 2) {
			throw new JPathRuntimeException("InvalidArgCount");
		}

		if (args[0] == null || args[0].type != ARGUMENT_TYPE.ATTRIBUTE) {
			throw new JPathRuntimeException("InvalidArg", args[0]);
		}

		if (args[1] == null || args[1].type != ARGUMENT_TYPE.VALUE) {
			throw new JPathRuntimeException("InvalidArg", args[1]);
		}

		String attributeName = ((AttributeArgument) args[0]).getAttributeName();

		if (!value.has(attributeName)) {
			return null;
		}

		Value regexValue = ((ValueArgument) args[1]).getValue();

		if (regexValue.getValueType() != ValueType.STRING) {
			throw new JPathRuntimeException("InvalidArg", args[1]);
		}

		Value matchValue = value.get(attributeName);

		Pattern pattern = Pattern.compile(regexValue.getString());
		Matcher matcher = pattern.matcher(matchValue.getString());

		if (matcher.matches()) {
			return matchValue;
		} else {
			return null;
		}
	}

}
