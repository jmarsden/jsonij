/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath.predicate;

import java.util.List;

import jsonij.Value;
import jsonij.jpath.PredicateComponent;
import static jsonij.ValueType.ARRAY;
import static jsonij.ValueType.OBJECT;

/**
 * All predicate implementation.
 */
public class AllPredicate extends PredicateComponent {

	/**
	 * If the value is an array, then add all elements. If the value is an object,
	 * then add all attributes.
	 * 
	 * @param values  The current values.
	 * @param results The results.
	 * @return The new values.
	 */
	@Override
	public List<Value> evaluate(List<Value> values, List<Value> results) {
		for (Value value : values) {
			if (value.type() == ARRAY) {
				for (int i = 0; i < value.size(); i++) {
					results.add(value.get(i));
				}
			} else if (value.type() == OBJECT) {
				for (int i = 0; i < value.size(); i++) {
					results.add(value.get(i));
				}
			}
		}
		return results;
	}
}
