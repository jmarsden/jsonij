/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import jsonij.JSON;
import jsonij.Value;
import java.io.IOException;
import java.io.Reader;
import jsonij.parser.JSONParser;
import jsonij.parser.ParserException;

/**
 * JPath implementation. Inspired by XPath and
 * <a href="http://goessner.net/articles/JsonPath/">JsonPath</a>.
 *
 * @author J.W.Marsden
 * @param <C> Component Type
 */
public class JPath<C extends Component> extends JPathImp<C> {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -1908871021763306556L;

	public static final JPathParser JPATH_PARSER;

	static {
		JPATH_PARSER = new JPathParser();
	}

	public JPath() {
	}

	public static JPath<Component> parse(String path) throws IOException, ParserException {
		JPath<Component> jPath = JPATH_PARSER.parse(path);
		return jPath;
	}

	public static Value evaluate(java.lang.String document, java.lang.String jPath) throws ParserException, IOException {
		JSONParser jsonParser = new JSONParser();
		JSON jsonDocument = new JSON(jsonParser.parse(document));
		JPath<?> jPathInstance = JPATH_PARSER.parse(jPath);
		return jPathInstance.evaluate(jsonDocument);
	}

	public static Value evaluate(Reader documentReader, java.lang.String jPath) throws ParserException, IOException {
		JSONParser jsonParser = new JSONParser();
		JSON jsonDocument = new JSON(jsonParser.parse(documentReader));
		JPath<?> jPathInstance = JPATH_PARSER.parse(jPath);
		return jPathInstance.evaluate(jsonDocument);
	}

	public static Value evaluate(JSON jsonDocument, java.lang.String jPath) throws ParserException, IOException {
		JPath<?> jPathInstance = JPATH_PARSER.parse(jPath);
		return jPathInstance.evaluate(jsonDocument);
	}

	public static Value evaluate(Value value, java.lang.String jPath) throws ParserException, IOException {
		JPath<?> jPathInstance = JPATH_PARSER.parse(jPath);
		return jPathInstance.evaluate(value);
	}
}
