/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.util.Locale;

import jsonij.parser.ReaderException;

public class JPathParserException extends ReaderException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -4115413461244539112L;

	public static final String MESSAGE_BUNDLE = "JPathMessageBundle";

	/**
	 * Basic Constructor.
	 *
	 * @param key Exception key
	 */
	public JPathParserException(String key) {
		super(key, -1, -1, (Object[]) null);
	}

	/**
	 * Constructor Including Line Number and Position Number of Exception
	 *
	 * @param key      Exception Key
	 * @param line     Exception Line
	 * @param position Exception Position
	 * @param args     Additional Arguments for Exception
	 */
	public JPathParserException(String key, int line, int position, Object... args) {
		super(key, line, position, null, args);
	}

	/**
	 * Constructor Including Line Number, Position Number and Locale of Exception.
	 *
	 * @param key      Exception Key
	 * @param line     Exception Line
	 * @param position Exception Position
	 * @param locale   Valid Locale for the exception
	 * @param args     Additional Arguments for Exception
	 */
	public JPathParserException(String key, int line, int position, Locale locale, Object... args) {
		super(key, line, position, locale, args);
	}

	@Override
	public String getBundleName() {
		return MESSAGE_BUNDLE;
	}
}
