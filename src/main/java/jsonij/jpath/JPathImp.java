/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.lang.reflect.Array;
import java.util.ArrayList;

import jsonij.JSON;
import jsonij.Value;

/**
 * JPath implementation. Inspired by XPath and
 * <a href="http://goessner.net/articles/JsonPath/">JsonPath</a>.
 *
 * @param <C> Component Type
 */
public class JPathImp<C extends Component> extends ArrayList<C> {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 3165927117307054584L;

	boolean recordEvaluateTime;

	long lastEvaluateTime;

	public JPathImp() {
		recordEvaluateTime = false;
		lastEvaluateTime = -1;
	}

	public Value evaluate(JSON json) throws JPathRuntimeException {
		Value value = json.getRoot();
		return evaluate(value);
	}

	public Value[] evaluateAll(JSON json) throws JPathRuntimeException {
		Value value = json.getRoot();
		return evaluateAll(value);
	}

	public Value evaluate(Value value) throws JPathRuntimeException {
		Value[] returnValues = evaluateAll(value);
		if (Array.getLength(returnValues) > 0) {
			return returnValues[0];
		} else {
			return null;
		}
	}

	public Value[] evaluateAll(Value value) throws JPathRuntimeException {
		long startTime = 0, finishTime = -1;
		if (recordEvaluateTime) {
			startTime = System.nanoTime();
		}
		ArrayList<Value> currentValues = new ArrayList<Value>();
		ArrayList<Value> nextValues = null;
		currentValues.add(value);

		// int count = 0;

		for (int i = 0; i < size(); i++) {
			Component c = get(i);
			nextValues = new ArrayList<Value>();
			if (c.getClass() == SearchComponent.class) {
				throw new JPathRuntimeException("notSupported", SearchComponent.class);
			}
			c.evaluate(currentValues, nextValues);
			currentValues = nextValues;
			// count++;
		}

		if (recordEvaluateTime) {
			finishTime = System.nanoTime();
			lastEvaluateTime = finishTime - startTime;
		}

		int size = currentValues.size();
		Value[] returnValues = new Value[size];
		for (int i = 0; i < size; i++) {
			returnValues[i] = currentValues.get(i);
		}
		return returnValues;
	}

	public boolean isRecordEvaluateTime() {
		return recordEvaluateTime;
	}

	public void setRecordEvaluateTime(boolean recordEvaluateTime) {
		this.recordEvaluateTime = recordEvaluateTime;
	}

	public long getLastEvaluateTime() {
		return lastEvaluateTime / 1000L;
	}
}
