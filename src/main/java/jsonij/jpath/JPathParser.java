/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import jsonij.jpath.predicate.AllPredicate;
import jsonij.jpath.predicate.UnionPredicate;
import jsonij.jpath.predicate.SimpleIndexPredicate;
import jsonij.jpath.predicate.LastIndexPredicate;
import jsonij.jpath.predicate.StartEndStepPredicate;
import jsonij.jpath.predicate.ExpressionPredicate;
import jsonij.ConstantUtility;
import jsonij.Value;
import jsonij.jpath.predicate.ExpressionPredicate.ExpressionPredicateCombineOperator;
import jsonij.jpath.predicate.ExpressionPredicate.ExpressionPredicateOperator;
import jsonij.jpath.predicate.ExpressionPredicate.FunctionExpressionPredicateCondition;
import jsonij.jpath.predicate.ExpressionPredicate.OperatorExpressionPredicateCondition;
import jsonij.jpath.predicate.FunctionArgument;

import static jsonij.Constants.ALL_CHAR;
import static jsonij.Constants.AND;
import static jsonij.Constants.COLON_CHAR;
import static jsonij.Constants.COMMA_CHAR;
import static jsonij.Constants.CURRENT_ELEMENT_CHAR;
import static jsonij.Constants.DOLLARS_CHAR;
import static jsonij.Constants.EQUAL;
import static jsonij.Constants.EXPRESSION_CHAR;
import static jsonij.Constants.FILTER_CHAR;
import static jsonij.Constants.GREATER;
import static jsonij.Constants.LAST_CHAR;
import static jsonij.Constants.LAST_PREDICATE;
import static jsonij.Constants.LEFT_PARENTHESIS;
import static jsonij.Constants.LEFT_SQUARE_BRACKET;
import static jsonij.Constants.LESS;
import static jsonij.Constants.MINUS;
import static jsonij.Constants.OR;
import static jsonij.Constants.PERIOD_CHAR;
import static jsonij.Constants.REVERSE_SOLIDUS;
import static jsonij.Constants.RIGHT_PARENTHESIS;
import static jsonij.Constants.RIGHT_SQUARE_BRACKET;
import static jsonij.Constants.SINGLE_QUOTE_CHAR;
import static jsonij.Constants.SOLIDUS_CHAR;
import jsonij.parser.JSONParser;
import jsonij.parser.ParserException;

public class JPathParser {

	public enum JPathMode {
		SOLIDUS, PERIOD, PATH
	}

	public JPath<Component> parse(String jPath) throws ParserException {
		JPathReader target = new JPathReader(jPath);
		return parse(target);
	}

	public JPath<Component> parse(JPathReader target) throws ParserException {
		JPath<Component> path = new JPath<Component>();
		JPathMode mode = null;
		if (mode == null) {
			if (target.peek() == SOLIDUS_CHAR) {
				mode = JPathMode.SOLIDUS;
			} else if (target.peek() == DOLLARS_CHAR) {
				mode = JPathMode.PERIOD;
			}
		}
		while (target.peek() != -1) {
			if (mode == JPathMode.SOLIDUS && target.peek() == SOLIDUS_CHAR) {
				target.read();
				if (target.peek() == SOLIDUS_CHAR) {
					target.read();
					path.add(new SearchComponent());
				}
			} else if (mode == JPathMode.PERIOD && target.peek() == PERIOD_CHAR) {
				target.read();
				
				if (target.peek() == PERIOD_CHAR) {
					target.read();
					path.add(new SearchComponent());
				} 
			} else if (mode == JPathMode.PERIOD && target.peek() == DOLLARS_CHAR) {
				target.read();
				if (target.peek() == PERIOD_CHAR) {
					target.read();
					if (target.peek() == PERIOD_CHAR) {
						target.read();
						path.add(new SearchComponent());
					}
				} else if(target.peek() == LEFT_SQUARE_BRACKET) {
					mode = JPathMode.PATH;
				}
			} else if (mode == JPathMode.PATH && target.peek() == LEFT_SQUARE_BRACKET) {
				target.read();
				if (target.peek() == SINGLE_QUOTE_CHAR) {
					target.read();
					KeyComponent key = readKey(target, JPathMode.PATH);
					if (key != null) {
						path.add(key);
					} else {
						throw new JPathParserException("invalidKey", target.getLineNumber(),
								target.getPositionNumber(), (char) target.peek());
					}
					if (target.peek() == SINGLE_QUOTE_CHAR) {
						target.read();
						if (target.peek() == RIGHT_SQUARE_BRACKET) {
							target.read();
						} else {
							throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
									target.getPositionNumber(), (char) RIGHT_SQUARE_BRACKET, (char) target.peek());
						}
					} else {
						throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
								target.getPositionNumber(), (char) SINGLE_QUOTE_CHAR, (char) target.peek());
					}
				} else {
					throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
							target.getPositionNumber(), (char) SINGLE_QUOTE_CHAR, (char) target.peek());
				}
			} else if (target.peek() == LEFT_SQUARE_BRACKET) {
				PredicateComponent predicate = readPredicate(target, mode);
				if (predicate != null) {
					path.add(predicate);
				}
			} else if (target.peek() == ALL_CHAR) {
				target.read();
				path.add(new AllComponent());
			} else {
				KeyComponent key = readKey(target, mode);
				if (key != null) {
					path.add(key);
				}
			}
		}
		return path;
	}

	public KeyComponent readKey(JPathReader target, JPathMode mode) throws ParserException {
		StringBuilder keyBuilder = new StringBuilder();
		int keySize = 0;
		while (target.peek() != -1) {
			if (mode == JPathMode.SOLIDUS && target.peek() == SOLIDUS_CHAR) {
				break;
			} else if (mode == JPathMode.PERIOD && target.peek() == PERIOD_CHAR) {
				break;
			} else if (target.peek() == LEFT_SQUARE_BRACKET) {
				break;
			} else if (mode == JPathMode.PATH && target.peek() == SINGLE_QUOTE_CHAR) {
				break;
			} else if (target.peek() == REVERSE_SOLIDUS) {
				target.read();
				keyBuilder.append((char) target.read());
				keySize++;
			} else {
				keyBuilder.append((char) target.read());
				keySize++;
			}
		}
		return new KeyComponent(keyBuilder.toString(), keySize);
	}

	public PredicateComponent readPredicate(JPathReader target, JPathMode mode) throws ParserException {
		if (target.peek() == -1) {
			return null;
		}
		if (target.peek() == LEFT_SQUARE_BRACKET) {
			target.read();
		} else {
			throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) '[', (char) target.peek());
		}
		StringBuilder predicateComponentBuilder;
		if (target.peek() == ALL_CHAR) {
			target.read();
			if (target.peek() == RIGHT_SQUARE_BRACKET) {
				target.read();
			}
			return new AllPredicate();
		} else if (ConstantUtility.isNumeric(target.peek())) {
			/**
			 * Most likely case, first character is a digit and this is a SimpleIndex.
			 *
			 */
			predicateComponentBuilder = new StringBuilder();
			predicateComponentBuilder.append((char) target.read());
			while (ConstantUtility.isNumeric(target.peek())) {
				predicateComponentBuilder.append((char) target.read());
			}
			if (target.peek() == RIGHT_SQUARE_BRACKET) {
				target.read();
				return new SimpleIndexPredicate(Integer.parseInt(predicateComponentBuilder.toString()));
			} else if (target.peek() == COMMA_CHAR) {
				// Union Predicate
				UnionPredicate unionPredicate = new UnionPredicate();
				int index = -1;
				while (ConstantUtility.isNumeric(target.peek()) || target.peek() == COMMA_CHAR) {
					if (target.peek() == COMMA_CHAR) {
						index = Integer.parseInt(predicateComponentBuilder.toString());
						unionPredicate.addIndex(index);
						target.read();
						predicateComponentBuilder = new StringBuilder();
					} else if (ConstantUtility.isNumeric(target.peek())) {
						predicateComponentBuilder.append((char) target.read());
					}
				}
				if (target.peek() == RIGHT_SQUARE_BRACKET) {
					index = Integer.parseInt(predicateComponentBuilder.toString());
					unionPredicate.addIndex(index);
					target.read();
					return unionPredicate;
				} else {
					throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
							target.getPositionNumber(), (char) ']', (char) target.peek());
				}
			} else if (target.peek() == COLON_CHAR) {
				int start = Integer.parseInt(predicateComponentBuilder.toString());
				return readStartEndStepPredicate(target, predicateComponentBuilder, start);
			} else {
				throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
						target.getPositionNumber(), (char) ']', (char) target.peek());
			}
		} else if (target.peek() == LAST_CHAR) {
			/**
			 * Check for $
			 *
			 */
			target.read();
			if (target.peek() == RIGHT_SQUARE_BRACKET) {
				target.read();
				return LastIndexPredicate.LAST;
			} else if (target.peek() == MINUS) {
				target.read();
				if (ConstantUtility.isDigit(target.peek())) {
					predicateComponentBuilder = new StringBuilder();
					predicateComponentBuilder.append((char) target.read());
					while (ConstantUtility.isDigit(target.peek())) {
						predicateComponentBuilder.append((char) target.read());
					}
					if (target.peek() == RIGHT_SQUARE_BRACKET) {
						target.read();
						return new LastIndexPredicate(Integer.parseInt(predicateComponentBuilder.toString()));
					} else {
						throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
								target.getPositionNumber(), (char) ']', (char) target.peek());
					}
				} else {
					throw new UnsupportedOperationException("Not a digit in LastIndex.");
				}
			} else {
				throw new JPathParserException("invalidValue", target.getLineNumber(), target.getPositionNumber(),
						(char) ']', (char) target.peek());
			}
		} else if (target.peek() == LAST_PREDICATE.charAt(0)) {
			/**
			 * Check for last() predicate string
			 *
			 */
			for (int i = 0; i < LAST_PREDICATE.length(); i++) {
				if (target.peek() == LAST_PREDICATE.charAt(i)) {
					target.read();
				} else {
					throw new JPathParserException("invalidValue", target.getLineNumber(), target.getPositionNumber(),
							LAST_PREDICATE, (char) target.peek());
				}
			}
			if (target.peek() == RIGHT_SQUARE_BRACKET) {
				target.read();
				return LastIndexPredicate.LAST;
			} else if (target.peek() == MINUS) {
				target.read();
				if (ConstantUtility.isDigit(target.peek())) {
					predicateComponentBuilder = new StringBuilder();
					predicateComponentBuilder.append((char) target.read());
					while (ConstantUtility.isDigit(target.peek())) {
						predicateComponentBuilder.append((char) target.read());
					}
					if (target.peek() == RIGHT_SQUARE_BRACKET) {
						target.read();
						return new LastIndexPredicate(Integer.parseInt(predicateComponentBuilder.toString()));
					} else {
						throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
								target.getPositionNumber(), (char) ']', (char) target.peek());
					}
				} else {
					throw new UnsupportedOperationException("Not a digit in LastIndex.");
				}
			} else {
				throw new JPathParserException("invalidPathExpecting1", target.getLineNumber(),
						target.getPositionNumber(), (char) ']', (char) target.peek());
			}
		} else if (target.peek() == COLON_CHAR) {
			// Start:End:Step
			int start = 0;
			predicateComponentBuilder = new StringBuilder();
			return readStartEndStepPredicate(target, predicateComponentBuilder, start);
		} else if (target.peek() == EXPRESSION_CHAR) {
			return readExpressionPredicate(target, mode);
		} else if (target.peek() == FILTER_CHAR) {
			return readFilterPredicate(target, mode);
		} 
		throw new UnsupportedOperationException("Invalid Predicate.");
	}

	public StartEndStepPredicate readStartEndStepPredicate(JPathReader target, StringBuilder predicateComponentBuilder,
			int start) throws ParserException {
		// Start End Step Predicate
		int end = -1;
		int step = 1;

		StartEndStepPredicate startEndStepPredicate = new StartEndStepPredicate();

		if (target.peek() == RIGHT_SQUARE_BRACKET) {
			int num = Integer.parseInt(predicateComponentBuilder.toString());
			target.read();
			return startEndStepPredicate;
		} else {
			throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),
					target.getPositionNumber(), (char) ']', (char) target.peek());
		}
	}

	public ExpressionPredicate readExpressionPredicate(JPathReader target, JPathMode mode) throws ParserException {
		ExpressionPredicate expressionPredicate = new ExpressionPredicate();
		StringBuilder predicateComponentBuilder;
		if (target.peek() == LEFT_PARENTHESIS) {
			target.read();
			StringBuilder expressionStringBuilder = new StringBuilder();
			char c;
			while (target.peek() != RIGHT_PARENTHESIS) {
				if (target.peek() == -1) {
					throw new JPathParserException("invalidPathFoundEndExpecting1", target.getLineNumber(),
							target.getPositionNumber(), (char) ')', (char) target.peek());
				}
				target.skipWhitepace(expressionStringBuilder);
				ExpressionPredicateCombineOperator op = null;
				if (target.peek() == AND) {
					while (target.peek() == AND) {
						expressionStringBuilder.append((char) target.read());
					}
					op = ExpressionPredicateCombineOperator.AND;
				} else if (target.peek() == OR) {
					while (target.peek() == OR) {
						expressionStringBuilder.append((char) target.read());
					}
					op = ExpressionPredicateCombineOperator.OR;
				}
				target.skipWhitepace(expressionStringBuilder);
				if (target.peek() == CURRENT_ELEMENT_CHAR) {
					expressionStringBuilder.append((char) target.read());
					if (target.peek() == PERIOD_CHAR) {
						expressionStringBuilder.append((char) target.read());
						target.skipWhitepace(expressionStringBuilder);
						String attribute = null;
						String value = null;
						predicateComponentBuilder = new StringBuilder();
						while (target.peek() != RIGHT_PARENTHESIS && target.peek() != LESS && target.peek() != GREATER && target.peek() != MINUS
								&& target.peek() != EQUAL) {
							c = (char) target.read();
							expressionStringBuilder.append(c);
							predicateComponentBuilder.append(c);
						}
						attribute = predicateComponentBuilder.toString().trim();
						ExpressionPredicateOperator expressionPredicateOperator = null;
						if (target.peek() == RIGHT_PARENTHESIS) {
							expressionStringBuilder.append((char) target.read());
							expressionPredicateOperator = ExpressionPredicateOperator.NOT_NULL;
						} else if (target.peek() == EQUAL) {
							expressionStringBuilder.append((char) target.read());
							expressionPredicateOperator = ExpressionPredicateOperator.EQUAL;
						} else if (target.peek() == LESS) {
							expressionStringBuilder.append((char) target.read());
							if (target.peek() == EQUAL) {
								expressionStringBuilder.append((char) target.read());
								expressionPredicateOperator = ExpressionPredicateOperator.LESS_EQUAL;
							} else {
								expressionPredicateOperator = ExpressionPredicateOperator.LESS;
							}
						} else if (target.peek() == GREATER) {
							expressionStringBuilder.append((char) target.read());
							if (target.peek() == EQUAL) {
								expressionStringBuilder.append((char) target.read());
								expressionPredicateOperator = ExpressionPredicateOperator.GREATER_EQUAL;
							} else {
								expressionPredicateOperator = ExpressionPredicateOperator.GREATER;
							}
						} else if (target.peek() == MINUS) {
							expressionPredicateOperator = ExpressionPredicateOperator.MINUS;
						}
						target.skipWhitepace(expressionStringBuilder);
						predicateComponentBuilder = new StringBuilder();
						while (target.peek() != RIGHT_PARENTHESIS && target.peek() != AND && target.peek() != OR) {
							if (target.peek() == -1) {
								throw new JPathParserException("invalidPathFoundEndExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) ')', (char) target.peek());
							}
							c = (char) target.read();
							expressionStringBuilder.append(c);
							predicateComponentBuilder.append(c);
						}
						value = predicateComponentBuilder.toString().trim();
						JSONParser jsonParser = new JSONParser(value);
						Value jsonValue = jsonParser.parseValue();
						target.skipWhitepace(expressionStringBuilder);
						OperatorExpressionPredicateCondition predicateCondition = new OperatorExpressionPredicateCondition(attribute, expressionPredicateOperator, jsonValue);
						if (op != null) {
							predicateCondition.setCombine(op);
							op = null;
						}
						expressionPredicate.conditions().add(predicateCondition);
					}
				} else if (jPathFunctionParseCheck(target.peek())) {
					// Read Function Name
					StringBuilder functionNameBuilder = new StringBuilder();
					while (target.peek() != -1 && target.peek() != '(') {
						c = (char) target.read();
						expressionStringBuilder.append(c);
						functionNameBuilder.append(c);
					}
					c = (char) target.read();
					expressionStringBuilder.append(c);
					target.skipWhitepace(expressionStringBuilder);

					// Read Function Arguments
					StringBuilder argumentsBuilder = new StringBuilder();

					while (target.peek() != -1 && target.peek() != ')') {
						c = (char) target.read();
						expressionStringBuilder.append(c);
						argumentsBuilder.append(c);
					}
					c = (char) target.read();
					expressionStringBuilder.append(c);

					FunctionArgument[] argumentArray = FunctionArgument
							.parseStringToArguments(argumentsBuilder.toString().trim());
					FunctionExpressionPredicateCondition predicateCondition = new FunctionExpressionPredicateCondition(
							functionNameBuilder.toString(), argumentArray);

					expressionPredicate.conditions().add(predicateCondition);

				} else {
					throw new JPathParserException("expression1", 0, target.getPositionNumber(),
							expressionStringBuilder.toString());
				}
			}
			// Read Left Parenthesis
			target.read();

			expressionPredicate.setExpression(expressionStringBuilder.toString());

			if (target.peek() == RIGHT_SQUARE_BRACKET) {
				expressionStringBuilder.append((char) target.read());
			}

			return expressionPredicate;
		} else {
			throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) '(', (char) target.peek());
		}
	}

	public ExpressionPredicate readFilterPredicate(JPathReader target, JPathMode mode) throws ParserException {
		StringBuilder predicateComponentBuilder;
		if (target.peek() == FILTER_CHAR) {
			target.read();
			if (target.peek() == LEFT_PARENTHESIS) {
				ExpressionPredicate expression = readExpressionPredicate(target, mode);
				// expression.setFilter(true);
				return expression;
			} else {
				throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(),target.getPositionNumber(), (char) '(', (char) target.peek());
			}
		} else {
			throw new JPathParserException("invalidStringExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) '?', (char) target.peek());
		}
	}

	public boolean jPathFunctionParseCheck(int i) {
		if (i == 'r') {
			return true;
		}
		return false;
	}
}
