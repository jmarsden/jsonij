/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Internationised Parser Exception.
 */
public class JPathRuntimeException extends RuntimeException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -4761587256432880263L;

	public static final String MESSAGE_BUNDLE = "JPathMessageBundle";

	/**
	 * Exception Key
	 */
	protected String key;

	protected String message;
	/**
	 * Exception Locale
	 */
	protected Locale locale;

	public JPathRuntimeException(String key, Object... args) {
		this(key, null, args);
	}

	public JPathRuntimeException(String key, Locale locale, Object... args) {
		this.key = key;
		this.locale = ((locale == null) ? Locale.ENGLISH : locale);
		if (this.locale != null) {
			String messageFormat = ResourceBundle.getBundle(getBundleName(), this.locale).getString(this.key);
			this.message = String.format(messageFormat, args);
		} else {
			this.message = String.format("Undefined Exception %s %s", key, locale);
		}
	}

	public String getBundleName() {
		return MESSAGE_BUNDLE;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
