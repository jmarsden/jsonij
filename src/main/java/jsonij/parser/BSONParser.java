/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2017 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import jsonij.BSON;
import jsonij.BSONCodec;
import jsonij.Value;
import static jsonij.Constants.BSON_ARRAY;
import static jsonij.Constants.BSON_BOOLEAN;
import static jsonij.Constants.BSON_DOCUMENT;
import static jsonij.Constants.BSON_DOUBLE;
import static jsonij.Constants.BSON_INT32;
import static jsonij.Constants.BSON_INT64;
import static jsonij.Constants.BSON_JS_CODE;
import static jsonij.Constants.BSON_NULL;
import static jsonij.Constants.BSON_OBJECT_ID;
import static jsonij.Constants.BSON_STRING;
import static jsonij.Constants.BSON_TERMINATION;
import static jsonij.Constants.BSON_TIMESTAMP;
import static jsonij.Constants.BSON_UTC_DATE_TIME;
import static jsonij.Constants.BSON_JS_CODE_WS;
import static jsonij.Constants.BSON_REGEX;
import static jsonij.Constants.BSON_DB_POINTER;
import static jsonij.Constants.BSON_BINARY_DATA;
import static jsonij.Constants.BSON_MAX_KEY;
import static jsonij.Constants.BSON_MIN_KEY;
import static jsonij.Constants.BSON_UNDEFINED_1;
import static jsonij.Constants.BSON_UNDEFINED_2;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BSONParser implements Parser {

	private final BSONCodec bsonCodec;
	private InputStream inputStream;
	private DataInputStream dataInputStream;
	private int index;
	private final byte[] buffer;

	public BSONParser(InputStream inputStream) {
		bsonCodec = BSON.CODEC;
		this.inputStream = inputStream;
		this.index = 0;
		this.dataInputStream = new DataInputStream(inputStream);
		this.buffer = new byte[8];
	}

	@Override
	public boolean canParse() throws ParserException {

		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int currentDocument() {
		return index;
	}

	public void resetDocument(InputStream inputStream) throws IOException {
		this.dataInputStream.close();
		this.inputStream = inputStream;
		this.index = 0;
		this.dataInputStream = new DataInputStream(inputStream);
	}

	private int readLittleInt() throws IOException {
		dataInputStream.readFully(buffer, 0, 4);
		return (buffer[3]) << 24 | (buffer[2] & 0xff) << 16 | (buffer[1] & 0xff) << 8 | (buffer[0] & 0xff);
	}

	/**
	 * Parses an input stream of BSON and returns the root Value.
	 *
	 * @return The root value of the read BSON
	 * @throws ParserException BSON Parser Exception
	 */
	@Override
	public BSON.Object<java.lang.String, Value> parse() throws ParserException {
		try {
			int byteCount = readLittleInt();
			ByteBuffer byteBuffer = BSON.allocateBuffer(byteCount);
			byteBuffer.putInt(byteCount);
			for (int i = 4; i < byteCount; i++) {
				byteBuffer.put(dataInputStream.readByte());
			}
			byteBuffer.flip();
			byteBuffer.position(4);
			BSON.Object<String, Value> returnObject = new BSON.Object<String, Value>();
			String elementKey;
			Value elementValue;
			byte element = byteBuffer.get();
			while (element != BSON_TERMINATION) {
				elementKey = parseBSONElement(byteBuffer);
				elementValue = parseValue(element, byteBuffer);
				if (elementKey == null || elementValue == null) {
					// TODO: throw an exception
				}
				returnObject.put(elementKey, elementValue);
				element = byteBuffer.get();
			}
			index++;
			return returnObject;
		} catch (IOException ex) {
			throw new BSONParserException(ex);
		}
	}

	public BSON.Object<java.lang.String, Value> parse(int n) throws ParserException {
		if (n < index) {
			throw new BSONParserException("Attempt to read a BSONParser Backwards.");
		}
		int skipCount = n - index - 1;
		for (int i = 0; i < skipCount; i++) {
			try {
				int byteCount = readLittleInt();
				dataInputStream.skip(byteCount - 4);
				index++;
			} catch (IOException ex) {
				throw new BSONParserException(ex);
			}
		}
		return parse();
	}

	private Value parseValue(byte element, ByteBuffer byteBuffer) throws IOException {
		Value elementValue;
		switch (element) {
		case BSON_BOOLEAN:
			elementValue = parseBSONBoolean(byteBuffer);
			break;
		case BSON_OBJECT_ID:
			elementValue = parseBSONObjectID(byteBuffer);
			break;
		case BSON_DOUBLE:
			elementValue = parseBSONDouble(byteBuffer);
			break;
		case BSON_STRING:
			elementValue = parseBSONString(byteBuffer);
			break;
		case BSON_NULL:
			elementValue = BSON.NULL;
			break;
		case BSON_ARRAY:
			elementValue = parseBSONArray(byteBuffer);
			break;
		case BSON_DOCUMENT:
			elementValue = parseBSONDocument(byteBuffer);
			break;
		case BSON_TIMESTAMP:
			elementValue = parseBSONTimestamp(byteBuffer);
			break;
		case BSON_INT32:
			elementValue = parseBSONInt32(byteBuffer);
			break;
		case BSON_INT64:
			elementValue = parseBSONInt64(byteBuffer);
			break;
		case BSON_UTC_DATE_TIME:
			elementValue = parseBSONUTCDateTime(byteBuffer);
			break;
		case BSON_JS_CODE:
			elementValue = parseBSONJS(byteBuffer);
			break;
		case BSON_JS_CODE_WS:
			elementValue = parseBSONJSWithScope(byteBuffer);
			break;
		case BSON_REGEX:
			elementValue = parseBSONRegex(byteBuffer);
			break;
		case BSON_BINARY_DATA:
			elementValue = parseBSONBinaryData(byteBuffer);
			break;
		case BSON_DB_POINTER:
			elementValue = parseBSONDBPointer(byteBuffer);
			break;
		case BSON_MAX_KEY:
			elementValue = parseBSONMaxKey(byteBuffer);
			break;
		case BSON_MIN_KEY:
			elementValue = parseBSONMinKey(byteBuffer);
			break;
		case BSON_UNDEFINED_1:
			elementValue = parseBSONUndefined1(byteBuffer);
			break;
		case BSON_UNDEFINED_2:
			elementValue = parseBSONUndefined2(byteBuffer);
			break;
		default:
			System.out.println("Unknown Element: " + Integer.toHexString(element));
			elementValue = BSON.NULL;
		}
		return elementValue;
	}

	/**
	 * BSON Element CString Parse.
	 *
	 * @param byteBuffer Buffer to parse
	 * @return Element value as String.
	 */
	private String parseBSONElement(ByteBuffer byteBuffer) {
		return bsonCodec.decodeCString(byteBuffer);
	}

	/**
	 * Parse a boolean value.
	 *
	 * @param byteBuffer Buffer to parse
	 * @return BSON Boolean Value.
	 */
	private BSON.Boolean parseBSONBoolean(ByteBuffer byteBuffer) {
		return bsonCodec.decodeBSONBoolean(byteBuffer) ? BSON.TRUE : BSON.FALSE;
	}

	/**
	 * Parse an ObjectID.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON ObjectID Value.
	 * @throws IOException Java IO Exception.
	 */
	private BSON.ObjectID parseBSONObjectID(ByteBuffer byteBuffer) throws IOException {
		byte[] objectIDBytes = new byte[12];
		byteBuffer.get(objectIDBytes);
		return new BSON.ObjectID(bsonCodec.decodeObjectID(objectIDBytes));
	}

	/**
	 * Parse a BSON Double value as a Numeric.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON Numeric Value.
	 */
	private BSON.Numeric parseBSONDouble(ByteBuffer byteBuffer) {
		return new BSON.Numeric(bsonCodec.decodeDouble(byteBuffer));
	}

	/**
	 * Parse a BSON UTF8 value as a String.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON String Value.
	 */
	private BSON.String parseBSONString(ByteBuffer byteBuffer) {
		return new BSON.String(bsonCodec.decodeUTF8String(byteBuffer));
	}

	/**
	 * Parse a BSON Array.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON Array Value.
	 * @throws IOException Java IO Exception.
	 */
	private Value parseBSONArray(ByteBuffer byteBuffer) throws IOException {
		int byteCount = byteBuffer.getInt();

		String elementKey;
		Value elementValue;

		BSON.Array<Value> array = new BSON.Array<Value>();

		byte element = byteBuffer.get();

		while (element != BSON_TERMINATION) {
			elementKey = parseBSONElement(byteBuffer);
			elementValue = parseValue(element, byteBuffer);
			array.add(elementValue);
			element = byteBuffer.get();
		}
		return array;
	}

	/**
	 * Parse a BSON Object.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON Object Value.
	 * @throws IOException Java IO Exception.
	 */
	private BSON.Object<?, ?> parseBSONDocument(ByteBuffer byteBuffer) throws IOException {
		int byteSize = byteBuffer.getInt();

		BSON.Object<String, Value> returnObject = new BSON.Object<String, Value>();
		String elementKey;
		Value elementValue;
		byte element = byteBuffer.get();
		while (element != BSON_TERMINATION) {
			elementKey = parseBSONElement(byteBuffer);
			elementValue = parseValue(element, byteBuffer);
			if (elementKey == null || elementValue == null) {
				// TODO: throw an exception
			}
			returnObject.put(elementKey, elementValue);
			element = byteBuffer.get();
		}
		return returnObject;
	}

	/**
	 * Parse a BSON Timestamp.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON Timestamp Value.
	 */
	private BSON.Timestamp parseBSONTimestamp(ByteBuffer byteBuffer) {
		return new BSON.Timestamp(bsonCodec.decodeTimestamp(byteBuffer));
	}

	/**
	 * Parse a BSON Int32.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON Int32 Value.
	 */
	private BSON.Int32 parseBSONInt32(ByteBuffer byteBuffer) {
		return new BSON.Int32(bsonCodec.decodeInt32(byteBuffer));
	}

	/**
	 * Parse a BSON Int64.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON Int64 Value.
	 */
	private BSON.Int64 parseBSONInt64(ByteBuffer byteBuffer) {
		return new BSON.Int64(bsonCodec.decodeInt64(byteBuffer));
	}

	/**
	 * Parse a BSON UTCDateTime.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON UTCDateTime Value.
	 */
	private BSON.UTCDateTime parseBSONUTCDateTime(ByteBuffer byteBuffer) {
		return new BSON.UTCDateTime(bsonCodec.decodeUTCDateTime(byteBuffer));
	}

	/**
	 * Parse BSON JavaScript.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON JavaScript Value.
	 */
	private BSON.JavaScript parseBSONJS(ByteBuffer byteBuffer) {
		return new BSON.JavaScript(bsonCodec.decodeUTF8String(byteBuffer));
	}

	/**
	 * Parse BSON JavaScript with Scope.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON JavaScriptWithScope Value.
	 * @throws IOException Java IO Exception.
	 */
	private BSON.JavaScriptWithScope parseBSONJSWithScope(ByteBuffer byteBuffer) throws IOException {
		return new BSON.JavaScriptWithScope(bsonCodec.decodeUTF8String(byteBuffer), parseBSONDocument(byteBuffer));
	}

	/**
	 * Parse BSON Regular Expression.
	 *
	 * @param byteBuffer Buffer to parse.
	 * @return BSON RegularExpression Value.
	 */
	private Value parseBSONRegex(ByteBuffer byteBuffer) {
		return new BSON.RegularExpression(bsonCodec.decodeCString(byteBuffer), bsonCodec.decodeCString(byteBuffer));
	}

	private Value parseBSONBinaryData(ByteBuffer byteBuffer) {
		int size = bsonCodec.decodeInt32(byteBuffer);
		byte type = byteBuffer.get();
		ByteBuffer payload = byteBuffer.slice();
		return new BSON.BinaryData(size, type, payload);
	}

	private Value parseBSONDBPointer(ByteBuffer byteBuffer) {
		byte[] objectIDBytes = new byte[12];
		byteBuffer.get(objectIDBytes);
		return new BSON.ObjectID(bsonCodec.decodeObjectID(objectIDBytes));
	}

	private Value parseBSONMaxKey(ByteBuffer byteBuffer) {
		return BSON.MAX_KEY;
	}

	private Value parseBSONMinKey(ByteBuffer byteBuffer) {
		return BSON.MIN_KEY;
	}

	private Value parseBSONUndefined1(ByteBuffer byteBuffer) {
		return BSON.UNDEFINED_1;
	}

	private Value parseBSONUndefined2(ByteBuffer byteBuffer) {
		String value = bsonCodec.decodeUTF8String(byteBuffer);
		return new BSON.Undefined2(value);
	}

}
