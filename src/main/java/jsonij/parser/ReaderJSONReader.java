/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import jsonij.ConstantUtility;
import java.io.IOException;
import java.io.Reader;

/**
 * java.io.Reader implementation of a JSONReader.
 *
 * @author J.W.Marsden
 */
public class ReaderJSONReader extends BaseJSONReader implements JSONReader {

	protected Reader reader;
	ReaderJSONStringReader stringReader;
	private int peekRead;

	public ReaderJSONReader(Reader reader) {
		if (reader == null) {
			throw new NullPointerException("Null Reader Exception.");
		}
		this.reader = reader;
		peekRead = -1;
	}

	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	/**
	 * Reads from the reader.
	 *
	 * @return The read byte if found otherwise -1 if the end of the stream is
	 *         reached.
	 * @throws jsonij.parser.ParserException Parser Exception.
	 */
	@Override
	public int readNext() throws ParserException {
		try {
			int r = -1;
			while (peekRead != -1 || (reader.ready() && (r = reader.read()) != -1)) {
				if (peekRead != -1) {
					r = peekRead;
					peekRead = -1;
				}
				position.movePosition();
				if (ConstantUtility.isComment(r)) {
					int peekR;
					if (reader.ready() && (peekR = reader.read()) != -1) {
						if (ConstantUtility.isComment(peekR)) {
							do {
								r = reader.read();
								position.movePosition();
							} while (r != -1 && !ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r));
						} else {
							peekRead = peekR;
							break;
						}
					}
				}
				if (ConstantUtility.isReturn(r)) {
					handleNewLine();
					r = reader.read();
					if (ConstantUtility.isNewLine(r)) {
						r = reader.read();
					}
				}
				if (ConstantUtility.isNewLine(r)) {
					handleNewLine();
					r = reader.read();
				}
				if (ConstantUtility.isComment(r)) {
					peekRead = r;
					continue;
				}
				if (!ConstantUtility.isWhiteSpace(r) && !ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r)) {
					break;
				}
			}
			return r;
		} catch (IOException ex) {
			throw new JSONParserException("readNext", getLineNumber(), getPositionNumber(), ex);
		}
	}

	@Override
	public JSONReader getStringReader(Mode mode) {
		if (stringReader == null) {
			stringReader = new ReaderJSONStringReader();
		} else if (stringReader.isActive()) {
			// TODO: Do we want an exception here?
		}
		stringReader.setMode(mode);
		stringReader.setActive(true);
		return stringReader;
	}

	protected class ReaderJSONStringReader implements JSONReader {

		protected Mode mode;
		protected boolean active;

		public ReaderJSONStringReader() {
			this.mode = Mode.QUOTED_READING;
			active = true;
		}

		public Mode getMode() {
			return mode;
		}

		public void setMode(Mode mode) {
			this.mode = mode;
		}

		/**
		 * @return the active
		 */
		public boolean isActive() {
			return active;
		}

		/**
		 * @param active the active to set
		 */
		protected void setActive(boolean active) {
			this.active = active;
		}

		@Override
		public int peek() throws ParserException {
			if (!hasPeeked) {
				if (!active) {
					return -1;
				}
				peekValue = readNext();
				hasPeeked = true;
			}
			return peekValue;
		}

		@Override
		public int read() throws ParserException {
			if (hasPeeked) {
				if (!active) {
					return -1;
				}
				hasPeeked = false;
				return peekValue;
			}
			return readNext();
		}

		@Override
		public void close() {
			active = false;
		}

		protected int readNext() throws ParserException {
			int r = -1;
			try {
				while (peekRead != -1 || reader.ready() && (r = reader.read()) != -1) {
					if (peekRead != -1) {
						r = peekRead;
						peekRead = -1;
					}
					position.movePosition();
					if (ConstantUtility.isComment(r)) {
						int peekR;
						if (reader.ready() && (peekR = reader.read()) != -1) {
							if (ConstantUtility.isComment(peekR)) {
								do {
									r = reader.read();
									position.movePosition();
								} while (r != -1 && !ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r));
							} else {
								peekRead = peekR;
								break;
							}
						}
					}
					if (ConstantUtility.isReturn(r)) {
						handleNewLine();
						r = reader.read();
						if (ConstantUtility.isNewLine(r)) {
							r = reader.read();
						}
					}
					if (ConstantUtility.isNewLine(r)) {
						handleNewLine();
						r = reader.read();
					}
					if (ConstantUtility.isComment(r)) {
						peekRead = r;
						continue;
					}
					if (!ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r)) {
						break;
					}
				}
			} catch (IOException ex) {
				throw new JSONParserException("readNext", getLineNumber(), getPositionNumber(), ex);
			}
			return r;
		}

		@Override
		public Position getPosition() {
			return ReaderJSONReader.this.getPosition();
		}

		@Override
		public boolean isHasPeeked() {
			return ReaderJSONReader.this.isHasPeeked();
		}

		@Override
		public boolean hasPeeked() {
			return ReaderJSONReader.this.hasPeeked();
		}

		@Override
		public void setHasPeeked(boolean hasPeeked) {
			ReaderJSONReader.this.setHasPeeked(hasPeeked);
		}

		@Override
		public int getLineNumber() {
			return ReaderJSONReader.this.getLineNumber();
		}

		@Override
		public int getPositionNumber() {
			return ReaderJSONReader.this.getPositionNumber();
		}

		@Override
		public JSONReader getStringReader(Mode mode) {
			return this;
		}
	}
}
