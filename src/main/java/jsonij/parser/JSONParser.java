/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import jsonij.JSON;
import jsonij.Value;
import jsonij.ConstantUtility;
import static jsonij.Constants.*;
import java.io.Reader;
import jsonij.parser.JSONReader.Mode;

public class JSONParser implements Parser {

	public enum Dialect {
		STRICT_RFC_4627, FLEXIBLE_RFC_4627
	}

	private Dialect dialect;

	private int currentDocument;
	private JSONReader target;
	private StringBuilder numericStringBuilder;

	public JSONParser() {
		dialect = Dialect.FLEXIBLE_RFC_4627;
		currentDocument = 0;
		target = null;
		numericStringBuilder = null;
	}

	public JSONParser(java.lang.String inputString) {
		this();
		target = new StringJSONReader(inputString);
	}

	public JSONParser(StringJSONReader target) {
		this();
		this.target = target;
	}

	public JSONParser(InputStream inputStream) {
		this(new BufferedReader(new InputStreamReader(inputStream)));
	}

	public JSONParser(Reader inputReader) {
		this();
		target = new ReaderJSONReader(inputReader);
	}

	public Dialect getDialect() {
		return dialect;
	}

	public void setDialect(Dialect dialect) {
		this.dialect = dialect;
	}

	@Override
	public boolean canParse() throws ParserException {
		return target != null && target.peek() != -1;
	}

	@Override
	public int currentDocument() {
		return currentDocument;
	}

	@Override
	public Value parse() throws ParserException {
		int r = target.peek();
		if (r == -1) {
			return null;
		}
		Value value;
		switch (r) {
		case OPEN_OBJECT:
			value = parseObject();
			break;
		case OPEN_ARRAY:
			value = parseArray();
			break;
		default:
			value = parseValue();
			break;
		}
		currentDocument++;
		return value;
	}

	@Override
	public Value parse(int i) throws ParserException {
		if (i < currentDocument) {
			throw new JSONParserException("Attempt to read a JSONParser Backwards.");
		}
		int skipCount = i - currentDocument - 1;
		for (int j = 0; j < skipCount; j++) {
			parse();
		}
		return parse();
	}

	public final Value parse(java.lang.String targetString) throws ParserException {
		currentDocument = 0;
		target = new StringJSONReader(targetString);
		return parse();
	}

	public final Value parse(Reader targetReader) throws ParserException {
		currentDocument = 0;
		target = new ReaderJSONReader(targetReader);
		return parse();
	}

	public final Value parseValue() throws ParserException {
		Value value = null;
		int p = target.peek();
		if (dialect == Dialect.STRICT_RFC_4627 && p == DOUBLE_QUOTE) {
			value = parseString();
		} else if (dialect == Dialect.FLEXIBLE_RFC_4627 && (p == DOUBLE_QUOTE || p == SINGLE_QUOTE)) {
			value = parseFlexibleString();
		} else if (p == OPEN_OBJECT) {
			value = parseObject();
		} else if (p == OPEN_ARRAY) {
			value = parseArray();
		} else if (ConstantUtility.isNumeric(p)) {
			value = parseNumeric();
		} else if (p == TRUE_STR.charAt(0)) {
			value = parseTrue();
		} else if (p == FALSE_STR.charAt(0)) {
			value = parseFalse();
		} else if (p == NULL_STR.charAt(0)) {
			value = parseNull();
		} else {
			throw new JSONParserException("invalidUnexpected", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
		}
		return value;
	}

	/**
	 * Parse a JSON Value from the target.
	 *
	 * @param terminators Termination Characters
	 * @return Parsed JSON Value
	 * @throws ParserException Parser Exception
	 */
	public final Value parseValue(int... terminators) throws ParserException {
		Value value = null;
		int p = target.peek();
		if (dialect == Dialect.STRICT_RFC_4627 && p == DOUBLE_QUOTE) {
			value = parseString();
		} else if (dialect == Dialect.FLEXIBLE_RFC_4627 && (p == DOUBLE_QUOTE || p == SINGLE_QUOTE)) {
			value = parseFlexibleString();
		} else if (p == OPEN_OBJECT) {
			value = parseObject();
		} else if (p == OPEN_ARRAY) {
			value = parseArray();
		} else if (ConstantUtility.isNumeric(p)) {
			value = parseNumeric();
		} else if (p == TRUE_STR.charAt(0)) {
			value = parseTrue();
		} else if (p == FALSE_STR.charAt(0)) {
			value = parseFalse();
		} else if (p == NULL_STR.charAt(0)) {
			value = parseNull();
		} else if (terminators != null) {
			value = parseFlexibleString(terminators);
		} else {
			throw new JSONParserException("invalidUnexpected", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
		}
		return value;
	}

	/**
	 * Parses a JSON Object Value from the reader.
	 *
	 * @return The Object Value
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.Object<JSON.String, Value> parseObject() throws ParserException {
		if (target.peek() != OPEN_OBJECT) {
			throw new JSONParserException("invalidObjectExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) OPEN_OBJECT, (char) target.peek());
		}
		target.read();
		JSON.Object<JSON.String, Value> value = new JSON.Object<JSON.String, Value>();
		if (target.peek() != CLOSE_OBJECT) {
			JSON.String attributeName;
			if (dialect != Dialect.STRICT_RFC_4627) {
				attributeName = (JSON.String) parseFlexibleString(NAME_SEPARATOR);
			} else {
				attributeName = (JSON.String) parseString();
			}
			if (target.peek() == NAME_SEPARATOR) {
				target.read();
			} else {
				throw new JSONParserException("invalidObjectExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) NAME_SEPARATOR, (char) target.peek());
			}
			Value attributeValue;
			if (dialect != Dialect.STRICT_RFC_4627) {
				attributeValue = parseValue(OBJECT_KEY_VALUE_TERMINALS);
			} else {
				attributeValue = parseValue();
			}
			value.put(attributeName, attributeValue);
			while (target.peek() == VALUE_SEPARATOR) {
				target.read();
				if (dialect != Dialect.STRICT_RFC_4627) {
					attributeName = (JSON.String) parseFlexibleString(NAME_SEPARATOR);
				} else {
					attributeName = (JSON.String) parseString();
				}
				if (value.containsKey(attributeName)) {
					throw new JSONParserException("invalidKeyAlreadyUsed", target.getLineNumber(), target.getPositionNumber(), attributeName);
				}
				if (target.peek() == NAME_SEPARATOR) {
					target.read();
				} else {
					throw new JSONParserException("invalidObjectExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) NAME_SEPARATOR, (char) target.peek());
				}
				if (dialect != Dialect.STRICT_RFC_4627) {
					attributeValue = parseValue(ARRAY_KEY_VALUE_TERMINALS);
				} else {
					attributeValue = parseValue();
				}

				value.put(attributeName, attributeValue);
			}
		}
		if (target.peek() == CLOSE_OBJECT) {
			target.read();
		} else {
			throw new JSONParserException("invalidArrayExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) CLOSE_OBJECT, (char) target.peek());
		}
		return value;
	}

	/**
	 * Parses a JSON Array Value from the reader.
	 *
	 * @return The Array Value
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.Array<Value> parseArray() throws ParserException {
		if (target.peek() != OPEN_ARRAY) {
			throw new JSONParserException("invalidArrayExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) OPEN_ARRAY, (char) target.peek());
		}
		target.read();
		JSON.Array<Value> value = new JSON.Array<Value>();
		if (target.peek() != CLOSE_ARRAY) {
			Value arrayValue;
			if (dialect != Dialect.STRICT_RFC_4627) {
				arrayValue = parseValue(ARRAY_KEY_VALUE_TERMINALS);
			} else {
				arrayValue = parseValue();
			}
			value.add(arrayValue);
			while (target.peek() == VALUE_SEPARATOR) {
				target.read();
				arrayValue = parseValue();
				value.add(arrayValue);
			}
		}
		if (target.peek() == CLOSE_ARRAY) {
			target.read();
		} else {
			throw new JSONParserException("invalidArrayExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) CLOSE_ARRAY, (char) target.peek());
		}
		return value;
	}

	/**
	 * Parses a JSON String Value from the reader.
	 *
	 * @return The String Value
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.String parseString() throws ParserException {
		JSON.String value;
		if (target.peek() != DOUBLE_QUOTE) {
			throw new JSONParserException("invalidStringExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) DOUBLE_QUOTE, (char) target.peek());
		}
		StringBuilder valueStringBuilder = new StringBuilder();
		JSONReader targetString = target.getStringReader(Mode.QUOTED_READING);
		targetString.read();
		int p;
		while (true) {
			p = targetString.peek();
			if (p == DOUBLE_QUOTE) {
				break;
			} else if (p == REVERSE_SOLIDUS) {
				targetString.read();
				p = targetString.peek();
				switch (p) {
				case DOUBLE_QUOTE:
					valueStringBuilder.append((char) QUOTATION_MARK);
					targetString.read();
					break;
				case REVERSE_SOLIDUS:
					valueStringBuilder.append((char) ESCAPE);
					targetString.read();
					break;
				case SOLIDUS_CHAR:
					valueStringBuilder.append((char) SOLIDUS_CHAR);
					targetString.read();
					break;
				case BACKSPACE_CHAR:
					valueStringBuilder.append((char) BACKSPACE);
					targetString.read();
					break;
				case FORM_FEED_CHAR:
					valueStringBuilder.append((char) FORM_FEED);
					targetString.read();
					break;
				case NEW_LINE_CHAR:
					valueStringBuilder.append((char) NEW_LINE);
					targetString.read();
					break;
				case CARRIAGE_RETURN_CHAR:
					valueStringBuilder.append((char) CARRIAGE_RETURN);
					targetString.read();
					break;
				case TAB_CHAR:
					valueStringBuilder.append((char) TAB);
					targetString.read();
					break;
				case HEX_CHAR:
					targetString.read();
					StringBuilder unicodeStringBuilder = new StringBuilder();
					for (int i = 0; i < 4; i++) {
						if (ConstantUtility.isHexDigit(targetString.peek())) {
							unicodeStringBuilder.append((char) targetString.read());
						} else {
							throw new JSONParserException("invalidStringHex", target.getLineNumber(), target.getPositionNumber(), targetString.peek());
						}
					}
					int unicodeInt = Integer.parseInt(unicodeStringBuilder.toString().toUpperCase(), 16);
					if (Character.isHighSurrogate((char) unicodeInt)) {
						String highSurrogateString = unicodeStringBuilder.toString();
						int highSurrogate = unicodeInt;
						unicodeStringBuilder = new StringBuilder();
						if (targetString.peek() == REVERSE_SOLIDUS) {
							targetString.read();
						} else {
							throw new JSONParserException("invalidStringMissingSurrogate", target.getLineNumber(), target.getPositionNumber(), REVERSE_SOLIDUS,
									targetString.peek());
						}
						if (targetString.peek() == HEX_CHAR) {
							targetString.read();
						} else {
							throw new JSONParserException("invalidStringMissingSurrogate", target.getLineNumber(), target.getPositionNumber(), HEX_CHAR, targetString.peek());
						}
						for (int i = 0; i < 4; i++) {
							if (ConstantUtility.isHexDigit(targetString.peek())) {
								unicodeStringBuilder.append((char) targetString.read());
							} else {
								throw new JSONParserException("invalidStringHex", target.getLineNumber(), target.getPositionNumber(), targetString.peek());
							}
						}
						String lowSurrogateString = unicodeStringBuilder.toString();
						int lowSurrogate = Integer.parseInt(lowSurrogateString.toUpperCase(), 16);
						if (Character.isSurrogatePair((char) highSurrogate, (char) lowSurrogate)) {
							char[] c = Character.toChars(Character.toCodePoint((char) highSurrogate, (char) lowSurrogate));
							valueStringBuilder.append(new String(c));
						} else {
							throw new JSONParserException("invalidStringSurrogates", target.getLineNumber(), target.getPositionNumber(), highSurrogateString, lowSurrogateString);
						}
					} else {
						if (ConstantUtility.isValidStringChar(unicodeInt)) {
							valueStringBuilder.append((char) unicodeInt);
						} else {
							throw new JSONParserException("invalidStringValue", target.getLineNumber(), target.getPositionNumber(), unicodeInt, unicodeStringBuilder.toString());
						}
					}
					break;
				default:
					throw new JSONParserException("invalidStringEscape", target.getLineNumber(), target.getPositionNumber(), targetString.peek());
				}
			} else {
				if (ConstantUtility.isValidStringChar(p)) {
					valueStringBuilder.append((char) targetString.read());
				} else {
					throw new JSONParserException("invalidStringValue", target.getLineNumber(), target.getPositionNumber(), targetString.peek(), (char) targetString.peek());
				}
			}
		}
		if (targetString.peek() != DOUBLE_QUOTE) {
			throw new JSONParserException("invalidStringExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) DOUBLE_QUOTE, (char) targetString.peek());
		} else {
			targetString.read();
		}
		targetString.close();
		value = new JSON.String(valueStringBuilder.toString());
		return value;
	}

	/**
	 * Parses a JSON String Value (with flexible rules) from the reader.
	 *
	 * @param terminals Termination Characters
	 * @return Parsed JSON String
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.String parseFlexibleString(int... terminals) throws ParserException {
		JSON.String value;
		StringBuilder valueStringBuilder = new StringBuilder();

		int quotation = -1;
		JSONReader targetString;
		if (target.peek() == DOUBLE_QUOTE || target.peek() == SINGLE_QUOTE) {
			quotation = target.read();
			targetString = target.getStringReader(Mode.QUOTED_READING);
		} else {
			targetString = target.getStringReader(Mode.UN_QUOTED_READING);
		}

		int p;
		while (true) {
			p = targetString.peek();
			if ((quotation != -1 && p == quotation) || terminalFound(p, terminals)) {
				break;
			} else if (p == REVERSE_SOLIDUS) {
				targetString.read();
				p = targetString.peek();
				switch (p) {
				case DOUBLE_QUOTE:
					valueStringBuilder.append((char) QUOTATION_MARK);
					targetString.read();
					break;
				case REVERSE_SOLIDUS:
					valueStringBuilder.append((char) ESCAPE);
					targetString.read();
					break;
				case SOLIDUS_CHAR:
					valueStringBuilder.append((char) SOLIDUS_CHAR);
					targetString.read();
					break;
				case BACKSPACE_CHAR:
					valueStringBuilder.append((char) BACKSPACE);
					targetString.read();
					break;
				case FORM_FEED_CHAR:
					valueStringBuilder.append((char) FORM_FEED);
					targetString.read();
					break;
				case NEW_LINE_CHAR:
					valueStringBuilder.append((char) NEW_LINE);
					targetString.read();
					break;
				case CARRIAGE_RETURN_CHAR:
					valueStringBuilder.append((char) CARRIAGE_RETURN);
					targetString.read();
					break;
				case TAB_CHAR:
					valueStringBuilder.append((char) TAB);
					targetString.read();
					break;
				case HEX_CHAR:
					targetString.read();
					StringBuilder unicodeStringBuilder = new StringBuilder();
					for (int i = 0; i < 4; i++) {
						if (ConstantUtility.isHexDigit(targetString.peek())) {
							unicodeStringBuilder.append((char) targetString.read());
						} else {
							throw new JSONParserException("invalidStringHex", target.getLineNumber(), target.getPositionNumber(), targetString.peek());
						}
					}
					int unicodeInt = Integer.parseInt(unicodeStringBuilder.toString().toUpperCase(), 16);
					if (Character.isHighSurrogate((char) unicodeInt)) {
						String highSurrogateString = unicodeStringBuilder.toString();
						int highSurrogate = unicodeInt;
						unicodeStringBuilder = new StringBuilder();
						if (targetString.peek() == REVERSE_SOLIDUS) {
							targetString.read();
						} else {
							throw new JSONParserException("invalidStringMissingSurrogate", target.getLineNumber(), target.getPositionNumber(), REVERSE_SOLIDUS,
									targetString.peek());
						}
						if (targetString.peek() == HEX_CHAR) {
							targetString.read();
						} else {
							throw new JSONParserException("invalidStringMissingSurrogate", target.getLineNumber(), target.getPositionNumber(), HEX_CHAR, targetString.peek());
						}
						for (int i = 0; i < 4; i++) {
							if (ConstantUtility.isHexDigit(targetString.peek())) {
								unicodeStringBuilder.append((char) targetString.read());
							} else {
								throw new JSONParserException("invalidStringHex", target.getLineNumber(), target.getPositionNumber(), targetString.peek());
							}
						}
						String lowSurrogateString = unicodeStringBuilder.toString();
						int lowSurrogate = Integer.parseInt(lowSurrogateString.toUpperCase(), 16);
						if (Character.isSurrogatePair((char) highSurrogate, (char) lowSurrogate)) {
							char[] c = Character.toChars(Character.toCodePoint((char) highSurrogate, (char) lowSurrogate));
							valueStringBuilder.append(new String(c));
						} else {
							throw new JSONParserException("invalidStringSurrogates", target.getLineNumber(), target.getPositionNumber(), highSurrogateString, lowSurrogateString);
						}
					} else {
						if (ConstantUtility.isValidStringChar(unicodeInt)) {
							valueStringBuilder.append((char) unicodeInt);
						} else {
							throw new JSONParserException("invalidStringValue", target.getLineNumber(), target.getPositionNumber(), unicodeInt, unicodeStringBuilder.toString());
						}
					}
					break;
				default:
					throw new JSONParserException("invalidStringEscape", target.getLineNumber(), target.getPositionNumber(), targetString.peek());
				}
			} else {
				if (ConstantUtility.isValidStringChar(p) || ConstantUtility.isWhiteSpace(p) || (dialect == Dialect.FLEXIBLE_RFC_4627 && quotation != -1 && quotation != p)) {
					valueStringBuilder.append((char) targetString.read());
				} else {
					throw new JSONParserException("invalidStringValue", target.getLineNumber(), target.getPositionNumber(), targetString.peek(), (char) targetString.peek());
				}
			}
		}
		if (quotation != -1 && target.peek() == quotation) {
			targetString.read();
			value = new JSON.String(valueStringBuilder.toString());
		} else {
			value = new JSON.String(valueStringBuilder.toString().trim());
		}
		return value;
	}

	private boolean terminalFound(int p, int[] terminals) {
		if (terminals != null && terminals.length > 0) {
			for (int i = 0; i < terminals.length; i++) {
				if (p == terminals[i]) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Parses a JSON Numeric Value from the reader.
	 *
	 * @return The Value of the numeric
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.Numeric parseNumeric() throws ParserException {
		JSON.Numeric value;
		numericStringBuilder = new StringBuilder();
		boolean minusFlag = false;
		boolean decimalFlag = false;
		boolean exponetFlag = false;
		int s = 0;
		if (target.peek() == MINUS) {
			minusFlag = true;
			target.read();
			numericStringBuilder.append((char) MINUS);
			if (!(target.peek() >= DIGITS[0] && target.peek() <= DIGITS[9])) {
				throw new JSONParserException("invalidNumericExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
			}
		}
		if (target.peek() == DIGITS[0]) {
			numericStringBuilder.append((char) target.read());
			s++;
		} else if (target.peek() >= DIGITS[1] && target.peek() <= DIGITS[9]) {
			numericStringBuilder.append((char) target.read());
			s++;
			while (ConstantUtility.isDigit(target.peek())) {
				numericStringBuilder.append((char) target.read());
				s++;
			}
		} else {
			throw new JSONParserException("invalidNumericExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
		}
		if (target.peek() == DECIMAL_POINT) {
			target.read();
			decimalFlag = true;
			numericStringBuilder.append((char) DECIMAL_POINT);
			if (!(target.peek() >= DIGITS[0] && target.peek() <= DIGITS[9])) {
				throw new JSONParserException("invalidNumericExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
			}
			while (ConstantUtility.isDigit(target.peek())) {
				numericStringBuilder.append((char) target.read());
				s++;
			}
		}
		if (target.peek() == EXPS[0] || target.peek() == EXPS[1]) {
			target.read();
			exponetFlag = true;
			numericStringBuilder.append((char) EXPS[1]);
			switch (target.peek()) {
			case MINUS:
				target.read();
				numericStringBuilder.append((char) MINUS);
				break;
			case PLUS:
				target.read();
				numericStringBuilder.append((char) PLUS);
				break;
			default:
				numericStringBuilder.append((char) PLUS);
				break;
			}
			if (!(target.peek() >= DIGITS[0] && target.peek() <= DIGITS[9])) {
				throw new JSONParserException("invalidNumericExpecting1", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
			}
			while (ConstantUtility.isDigit(target.peek())) {
				numericStringBuilder.append((char) target.read());
			}
		}
		String numericString = numericStringBuilder.toString();
		if (!decimalFlag && !exponetFlag) {
			if (s < 10) {
				value = new JSON.Numeric(Integer.parseInt(numericString));
			} else if (s < 18) {
				value = new JSON.Numeric(Long.parseLong(numericString));
			} else {
				value = new JSON.Numeric(numericString);
			}
		} else {
			if (s < 10) {
				value = new JSON.Numeric(Float.parseFloat(numericString));
			} else if (s < 16) {
				value = new JSON.Numeric(Double.parseDouble(numericString));
			} else {
				value = new JSON.Numeric(numericString);
			}
		}
		return value;
	}

	/**
	 * Parses a JSON True Value from the reader.
	 *
	 * @return The False Value
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.True parseTrue() throws ParserException {
		for (int i = 0; i < TRUE_STR.length(); i++) {
			if (target.peek() == TRUE_STR.charAt(i)) {
				target.read();
			} else {
				throw new JSONParserException("invalidValue", target.getLineNumber(), target.getPositionNumber(), TRUE_STR, (char) target.peek());
			}
		}
		return JSON.TRUE;
	}

	/**
	 * Parses a JSON False Value from the reader.
	 *
	 * @return The False Value
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.False parseFalse() throws ParserException {
		for (int i = 0; i < FALSE_STR.length(); i++) {
			if (target.peek() == FALSE_STR.charAt(i)) {
				target.read();
			} else {
				throw new JSONParserException("invalidValue", target.getLineNumber(), target.getPositionNumber(), FALSE_STR, (char) target.peek());
			}
		}
		return JSON.FALSE;
	}

	/**
	 * Parses a JSON Null Value from the reader.
	 *
	 * @return The Null Value
	 * @throws ParserException JSON Parser Exception
	 */
	public final JSON.Null parseNull() throws ParserException {
		for (int i = 0; i < NULL_STR.length(); i++) {
			if (target.peek() == NULL_STR.charAt(i)) {
				target.read();
			} else {
				throw new JSONParserException("invalidValue", target.getLineNumber(), target.getPositionNumber(), NULL_STR, (char) target.peek());
			}
		}
		return JSON.NULL;
	}
}
