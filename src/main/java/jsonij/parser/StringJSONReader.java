/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import jsonij.ConstantUtility;

/**
 * java.lang.String implementation of a JSONReader.
 *
 * @author J.W.Marsden
 */
public class StringJSONReader extends BaseJSONReader implements JSONReader {

	protected String jsonString;
	protected int index;
	protected int length;
	final protected StringJSONStringReader stringReader;
	private int peekRead;

	public StringJSONReader(String jsonString) {
		if (jsonString == null) {
			throw new NullPointerException("Null String Exception.");
		}
		this.jsonString = jsonString;
		length = jsonString.length();
		stringReader = new StringJSONStringReader();
		index = 0;
		peekRead = -1;
	}

	public StringJSONReader(String jsonString, int index) {
		if (jsonString == null) {
			throw new NullPointerException("Null String Exception.");
		}
		this.jsonString = jsonString;
		length = jsonString.length();
		stringReader = new StringJSONStringReader();
		this.index = index;
		position.setPostionNumber(index);
		peekRead = -1;
	}

	public void setJSONString(String jsonString) {
		if (jsonString == null) {
			throw new NullPointerException("Null String Exception.");
		}
		this.jsonString = jsonString;
		length = jsonString.length();
		index = 0;
		peekRead = -1;
	}

	public void setJSONString(String jsonString, int index) {
		if (jsonString == null) {
			throw new NullPointerException("Null String Exception.");
		}
		this.jsonString = jsonString;
		length = jsonString.length();
		this.index = index;
		position.setPostionNumber(index);
		peekRead = -1;
	}

	public String getJSONString() {
		return jsonString;
	}

	@Override
	public int readNext() throws ParserException {
		int r = -1;
		while (peekRead != -1 || index < length) {
			if (peekRead != -1) {
				r = peekRead;
				peekRead = -1;
			} else {
				r = jsonString.charAt(index++);
				position.movePosition();
			}
			if (ConstantUtility.isComment(r)) {
				if (index < length) {
					int pr = jsonString.charAt(index++);
					if (ConstantUtility.isComment(pr)) {
						do {
							r = jsonString.charAt(index++);
							position.movePosition();
						} while (index < length && !ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r));
						if (index >= length) {
							r = -1;
						}
					} else {
						peekRead = pr;
						break;
					}
				}
			}
			if (ConstantUtility.isReturn(r)) {
				handleNewLine();
				if (index < length) {
					r = jsonString.charAt(index++);
					if (ConstantUtility.isNewLine(r)) {
						if (index < length) {
							r = jsonString.charAt(index++);
						}
					}
				} else {
					break;
				}
			}
			if (ConstantUtility.isNewLine(r)) {
				handleNewLine();
				if (index < length) {
					r = jsonString.charAt(index++);
				}
			}
			if (ConstantUtility.isComment(r)) {
				peekRead = r;
				continue;
			}
			if (!ConstantUtility.isWhiteSpace(r) && !ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r)) {
				break;
			}
		}
		if (ConstantUtility.isReturn(r) || ConstantUtility.isNewLine(r) || ConstantUtility.isWhiteSpace(r)) {
			r = -1;
		}
		return r;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public JSONReader getStringReader(Mode mode) {
		stringReader.setActive(true);
		stringReader.setMode(mode);
		return stringReader;
	}

	protected class StringJSONStringReader implements JSONReader {

		protected Mode mode;
		public boolean active;

		public StringJSONStringReader() {
			mode = Mode.QUOTED_READING;
			active = true;
		}

		public Mode getMode() {
			return mode;
		}

		public void setMode(Mode mode) {
			this.mode = mode;
		}

		/**
		 * @return the active
		 */
		public boolean isActive() {
			return active;
		}

		/**
		 * @param active the active to set
		 */
		protected void setActive(boolean active) {
			this.active = active;
		}

		@Override
		public int peek() throws ParserException {
			if (!hasPeeked) {
				if (!active) {
					return -1;
				}
				peekValue = readNext();
				hasPeeked = true;
			}
			return peekValue;
		}

		@Override
		public int read() throws ParserException {
			if (!active) {
				return -1;
			}
			if (hasPeeked) {
				hasPeeked = false;
				return peekValue;
			}
			return readNext();
		}

		@Override
		public void close() {
			active = false;
		}

		protected int readNext() throws ParserException {
			int r = -1;
			if (index < length) {
				while (peekRead != -1 || index < length) {
					if (peekRead != -1) {
						r = peekRead;
						peekRead = -1;
					} else {
						r = jsonString.charAt(index++);
						position.movePosition();
					}
					if (mode == Mode.QUOTED_READING) {
						break;
					} else {
						if (ConstantUtility.isComment(r)) {
							if (index < length) {
								int pr = jsonString.charAt(index++);
								if (ConstantUtility.isComment(pr)) {
									do {
										r = jsonString.charAt(index++);
										position.movePosition();
									} while (index < length && !ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r));
									if (index >= length) {
										r = -1;
									}
								} else {
									peekRead = pr;
									break;
								}
							}
						}
						if (ConstantUtility.isReturn(r)) {
							handleNewLine();
							if (index < length) {
								r = jsonString.charAt(index++);
								if (ConstantUtility.isNewLine(r)) {
									if (index < length) {
										r = jsonString.charAt(index++);
									}
								}
							} else {
								break;
							}
						}
						if (ConstantUtility.isNewLine(r)) {
							handleNewLine();
							if (index < length) {
								r = jsonString.charAt(index++);
							}
						}
						if (ConstantUtility.isComment(r)) {
							peekRead = r;
							continue;
						}
						if (!ConstantUtility.isReturn(r) && !ConstantUtility.isNewLine(r)) {
							break;
						}
					}
				}
				if (ConstantUtility.isReturn(r) || ConstantUtility.isNewLine(r)) {
					r = -1;
				}
			} else {
				r = -1;
			}
			return r;
		}

		@Override
		public Position getPosition() {
			return StringJSONReader.this.getPosition();
		}

		@Override
		public boolean isHasPeeked() {
			return StringJSONReader.this.isHasPeeked();
		}

		@Override
		public boolean hasPeeked() {
			return StringJSONReader.this.hasPeeked();
		}

		@Override
		public void setHasPeeked(boolean hasPeeked) {
			StringJSONReader.this.setHasPeeked(hasPeeked);
		}

		@Override
		public int getLineNumber() {
			return StringJSONReader.this.getLineNumber();
		}

		@Override
		public int getPositionNumber() {
			return StringJSONReader.this.getPositionNumber();
		}

		@Override
		public JSONReader getStringReader(Mode mode) {
			return this;
		}
	}
}
