/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

public class Position {

	protected int lineNumber;
	protected int postionNumber;

	public Position() {
		this.lineNumber = 1;
		this.postionNumber = 0;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public int setLineNumber(int lineNumber) {
		return this.lineNumber = lineNumber;
	}

	public int newLine() {
		postionNumber = 0;
		return lineNumber++;
	}

	public int getPostionNumber() {
		return postionNumber;
	}

	public int setPostionNumber(int postionNumber) {
		return this.postionNumber = postionNumber;
	}

	final public int movePosition() {
		return postionNumber++;
	}

	@Override
	public String toString() {
		return String.format("(%s,%s)", getLineNumber(), getPostionNumber());
	}
}
