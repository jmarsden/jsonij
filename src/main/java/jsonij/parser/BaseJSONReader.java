/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import java.io.Reader;

public abstract class BaseJSONReader implements JSONReader {

	protected int peekValue;
	protected boolean hasPeeked;
	protected Position position;

	public BaseJSONReader() {
		this(null);
	}

	public BaseJSONReader(Reader reader) {
		this.peekValue = -1;
		this.hasPeeked = false;
		this.position = new Position();
	}

	/**
	 * @return the position
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 * @return The reader position
	 */
	protected Position setPosition(Position position) {
		return this.position = position;
	}

	/**
	 * Only here for POJO reasons.
	 *
	 * @return hasPeeked()
	 */
	public boolean isHasPeeked() {
		return hasPeeked();
	}

	public boolean hasPeeked() {
		return hasPeeked;
	}

	public void setHasPeeked(boolean hasPeeked) {
		this.hasPeeked = hasPeeked;
	}

	public int getLineNumber() {
		return getPosition().getLineNumber();
	}

	public int getPositionNumber() {
		return getPosition().getPostionNumber();
	}

	public int peek() throws ParserException {
		if (!hasPeeked) {
			peekValue = readNext();
			hasPeeked = true;
		}
		return peekValue;
	}

	public int read() throws ParserException {
		if (hasPeeked) {
			hasPeeked = false;
			return peekValue;
		}
		return readNext();
	}

	/**
	 * Reads from the reader.
	 *
	 * @return The read byte if found otherwise -1 if the end of the stream is
	 *         reached.
	 * @throws jsonij.parser.ParserException General Parser Exception
	 */
	protected abstract int readNext() throws ParserException;

	public void close() {

	}

	protected void handleNewLine() throws ParserException {
		getPosition().newLine();
	}

	@Override
	public String toString() {
		String state;
		try {
			state = String.format("Next Char %s", (char) peek());
		} catch (ParserException e) {
			state = String.format("Unknown State: %s", e.toString());
		}
		return String.format("Reader %s: %s", getPosition(), state);
	}

	public abstract JSONReader getStringReader(Mode mode);
}
