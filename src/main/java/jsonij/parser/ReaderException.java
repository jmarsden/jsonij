/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

public abstract class ReaderException extends ParserException {
	protected int line;

	protected int position;

	protected String message;

	/**
	 * Exception Key
	 */
	protected String key;
	/**
	 * Exception Locale
	 */
	protected Locale locale;

	/**
	 * Basic Constructor.
	 *
	 * @param key  Exception key
	 * @param args Additional Arguments for Exception
	 */
	public ReaderException(String key, Object... args) {
		this(key, -1, -1, null, args);
	}

	/**
	 * Constructor Including Line Number and Position Number of Exception
	 *
	 * @param key      Exception Key
	 * @param line     Exception Line
	 * @param position Exception Position
	 * @param args     Additional Arguments for Exception
	 */
	public ReaderException(String key, int line, int position, Object... args) {
		this(key, line, position, null, args);
	}

	/**
	 * Constructor Including Line Number, Position Number and Locale of Exception.
	 *
	 * @param key      Exception Key
	 * @param line     Exception Line
	 * @param position Exception Position
	 * @param locale   Valid Locale for the exception
	 * @param args     Additional Arguments for Exception
	 */
	public ReaderException(String key, int line, int position, Locale locale, Object... args) {
		this.line = line;
		this.position = position;
		this.key = key;

		this.locale = ((locale == null) ? Locale.ENGLISH : locale);
		if (this.locale != null) {
			try {
				String messageFormat = ResourceBundle.getBundle(getBundleName()).getString(this.key);
				this.message = String.format(messageFormat, args);
			} catch (Exception ex) {
				StringBuilder argumentStringBuilder = new StringBuilder();
				Object argValue;
				int argCount;
				if ((argCount = Array.getLength(args)) > 0) {
					for (int i = 0; i < argCount - 1; i++) {
						argValue = args[i];
						if (argValue != null) {
							argumentStringBuilder.append(Arrays.toString(args)).append(',');
						} else {
							argumentStringBuilder.append("null").append(',');
						}
					}
					argValue = args[argCount - 1];
					if (argValue != null) {
						argumentStringBuilder.append(argValue.toString());
					} else {
						argumentStringBuilder.append("null");
					}
				}
				String messageFormat = "Message Format Not Found (%s#%s[%s]): %s";
				this.message = String.format(messageFormat, getBundleName(), this.key, argumentStringBuilder.toString(), ex);
			}
		} else {
			this.message = String.format("Undefined Exception %s %s", key, locale);
		}
	}

	public abstract String getBundleName();

	@Override
	public String getMessage() {
		String output = "Parsing Exception";
		if (line != -1 || position != -1) {
			output = String.format("%s (%s,%s): %s", output, line, position, message);
		} else {
			output = String.format("%s: %s", output, message);
		}
		return output;
	}
}
