/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.nio.ByteBuffer;
import static jsonij.Constants.*;

public class UBJSONCodec {

	// Null
	public ByteBuffer encodeUBJSONNull(ByteBuffer byteBuffer) {
		byteBuffer.put(UBJSON_NULL);
		return byteBuffer;
	}

	public byte decodeUBJSONNull(ByteBuffer byteBuffer) {
		byteBuffer.get();
		return UBJSON_NULL;
	}
	
	// Boolean
	public ByteBuffer encodeUBJSONBoolean(ByteBuffer byteBuffer, boolean value) {
		if (value) {
			byteBuffer.put(UBJSON_TRUE);
		} else {
			byteBuffer.put(UBJSON_FALSE);
		}
		return byteBuffer;
	}

	public boolean decodeUBJSONBoolean(ByteBuffer byteBuffer) {
		byte b = byteBuffer.get();
		if (b == UBJSON_FALSE) {
			return false;
		} else if (b == UBJSON_TRUE) {
			return true;
		}
		return false;
	}

	void encodeString(ByteBuffer buffer, String value) {
		throw new UnsupportedOperationException("Not supported yet."); 
	}
}
