/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import static jsonij.Constants.BSON_FALSE;
import static jsonij.Constants.BSON_TERMINATION;
import static jsonij.Constants.BSON_TRUE;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import jsonij.BSON.BinaryData;
import jsonij.BSON.Decimal;

/**
 * Main BSON Encode and Decode methods for BSON Little Endian Buffers.
 * 
 * This class is not thread-safe.
 */
public class BSONCodec {

	public static Charset CHARSET;
	public final byte[] BYTE_ARRAY_4 = new byte[4];
	public final byte[] BYTE_ARRAY_8 = new byte[8];

	static {
		CHARSET = Charset.forName("UTF-8");
	}

	public boolean validateBufferOrder(ByteBuffer byteBuffer) {
		return byteBuffer.order() == ByteOrder.LITTLE_ENDIAN;
	}

	// Double
	public double decodeDouble(ByteBuffer byteBuffer) {
		double d = byteBuffer.getDouble();
		return d;
	}

	public ByteBuffer encodeDouble(ByteBuffer byteBuffer, double value) {
		byteBuffer.putDouble(value);
		return byteBuffer;
	}

	// Object Id
	public ByteBuffer encodeObjectID(ByteBuffer byteBuffer, ObjectIdentifier objectID) {
		byteBuffer.put(objectID.getContents());
		return byteBuffer;
	}

	public ObjectIdentifier decodeObjectID(byte[] byteBuffer) {
		return new ObjectIdentifier(byteBuffer);
	}

	// Boolean
	public ByteBuffer encodeBSONBoolean(ByteBuffer byteBuffer, boolean value) {
		if (value) {
			byteBuffer.put(BSON_TRUE);
		} else {
			byteBuffer.put(BSON_FALSE);
		}
		return byteBuffer;
	}

	public boolean decodeBSONBoolean(ByteBuffer byteBuffer) {
		byte b = byteBuffer.get();
		if (b == BSON_FALSE) {
			return false;
		} else if (b == BSON_TRUE) {
			return true;
		}
		return false;
	}

	// UTC datetime
	public long decodeUTCDateTime(ByteBuffer byteBuffer) {
		byte[] byteArray = BYTE_ARRAY_8;
		byteBuffer.get(byteArray);
		long dateValue = (long) (byteArray[7]) << 56 | (long) (byteArray[6] & 0xff) << 48
				| (long) (byteArray[5] & 0xff) << 40 | (long) (byteArray[4] & 0xff) << 32
				| (long) (byteArray[3] & 0xff) << 24 | (long) (byteArray[2] & 0xff) << 16
				| (long) (byteArray[1] & 0xff) << 8 | (long) (byteArray[0] & 0xff);

		return dateValue;
	}

	public ByteBuffer encodeUTCDateTime(ByteBuffer byteBuffer, long time) {
		byte[] byteArray = BYTE_ARRAY_8;
		byteArray[7] = (byte) (time >> 56);
		byteArray[6] = (byte) (time >> 48);
		byteArray[5] = (byte) (time >> 40);
		byteArray[4] = (byte) (time >> 32);
		byteArray[3] = (byte) (time >> 24);
		byteArray[2] = (byte) (time >> 16);
		byteArray[1] = (byte) (time >> 8);
		byteArray[0] = (byte) (time);
		byteBuffer.put(byteArray);
		return byteBuffer;
	}

	// 32-bit Int
	public final int decodeInt32(ByteBuffer byteBuffer) {
		byte[] byteArray = BYTE_ARRAY_4;
		byteBuffer.get(byteArray);
		int int32 = (byteArray[3]) << 24 | (byteArray[2] & 0xff) << 16 | (byteArray[1] & 0xff) << 8
				| (byteArray[0] & 0xff);
		return int32;
	}

	public final ByteBuffer encodeInt32(ByteBuffer byteBuffer, int value) {
		byte[] byteArray = BYTE_ARRAY_4;
		byteArray[3] = (byte) (value >> 24);
		byteArray[2] = (byte) (value >> 16);
		byteArray[1] = (byte) (value >> 8);
		byteArray[0] = (byte) (value);
		byteBuffer.put(byteArray);
		return byteBuffer;
	}

	// BSONTimestamp
	public BSONTimestamp decodeTimestamp(ByteBuffer byteBuffer) {
		byte[] byteArray = BYTE_ARRAY_4;
		byteBuffer.get(byteArray);
		int inc = (byteArray[3]) << 24 | (byteArray[2] & 0xff) << 16 | (byteArray[1] & 0xff) << 8
				| (byteArray[0] & 0xff);
		byteBuffer.get(byteArray);
		int time = (byteArray[3]) << 24 | (byteArray[2] & 0xff) << 16 | (byteArray[1] & 0xff) << 8
				| (byteArray[0] & 0xff);

		return new BSONTimestamp(time, inc);
	}

	public ByteBuffer encodeTimestamp(ByteBuffer byteBuffer, BSONTimestamp timestamp) {
		byte[] byteArray = BYTE_ARRAY_4;
		int inc = timestamp.getIncrement();
		byteArray[3] = (byte) (inc >> 24);
		byteArray[2] = (byte) (inc >> 16);
		byteArray[1] = (byte) (inc >> 8);
		byteArray[0] = (byte) (inc);
		byteBuffer.put(byteArray);
		int time = timestamp.getTime();
		byteArray[3] = (byte) (time >> 24);
		byteArray[2] = (byte) (time >> 16);
		byteArray[1] = (byte) (time >> 8);
		byteArray[0] = (byte) (time);
		byteBuffer.put(byteArray);
		return byteBuffer;
	}

	// 64-bit Int
	public long decodeInt64(ByteBuffer byteBuffer) {
		byte[] byteArray = BYTE_ARRAY_8;
		byteBuffer.get(byteArray);
		long int64 = (long) (byteArray[7]) << 56 | (long) (byteArray[6] & 0xff) << 48
				| (long) (byteArray[5] & 0xff) << 40 | (long) (byteArray[4] & 0xff) << 32
				| (long) (byteArray[3] & 0xff) << 24 | (long) (byteArray[2] & 0xff) << 16
				| (long) (byteArray[1] & 0xff) << 8 | (long) (byteArray[0] & 0xff);

		return int64;
	}

	public ByteBuffer encodeInt64(ByteBuffer byteBuffer, long int64) {
		byte[] byteArray = BYTE_ARRAY_8;
		byteArray[7] = (byte) (int64 >> 56);
		byteArray[6] = (byte) (int64 >> 48);
		byteArray[5] = (byte) (int64 >> 40);
		byteArray[4] = (byte) (int64 >> 32);
		byteArray[3] = (byte) (int64 >> 24);
		byteArray[2] = (byte) (int64 >> 16);
		byteArray[1] = (byte) (int64 >> 8);
		byteArray[0] = (byte) (int64);
		byteBuffer.put(byteArray);
		return byteBuffer;
	}

	// String
	public ByteBuffer encodeUTF8String(ByteBuffer byteBuffer, String value) {
		byteBuffer.putInt(value.length() + 1);
		byteBuffer.put(value.getBytes(CHARSET));
		byteBuffer.put(BSON_TERMINATION);
		return byteBuffer;
	}

	public String decodeUTF8String(ByteBuffer byteBuffer) {
		int size = byteBuffer.getInt();
		byte[] stringBytes = new byte[size - 1];
		byteBuffer.get(stringBytes, 0, size - 1);
		String stringValue = new String(stringBytes, BSONCodec.CHARSET);
		byteBuffer.get();
		return stringValue;
	}

	// CString
	public ByteBuffer encodeCString(ByteBuffer byteBuffer, String value) {
		if (value == null || value.equals("")) {
			byteBuffer.put(BSON_TERMINATION);
		} else {
			byteBuffer.put(value.getBytes(CHARSET));
			byteBuffer.put(BSON_TERMINATION);
		}
		return byteBuffer;
	}

	public String decodeCString(ByteBuffer byteBuffer) {
		byteBuffer.mark();
		// int p = byteBuffer.position();
		int c = 0;
		@SuppressWarnings("unused")
		byte b;
		while ((b = byteBuffer.get()) != BSON_TERMINATION) {
			c++;
		}
		byte[] elementBytes = new byte[c];
		byteBuffer.reset();
		byteBuffer.get(elementBytes, 0, c);
		byteBuffer.get();
		String element = new String(elementBytes, BSONCodec.CHARSET);
		return element;
	}

	// Decimal
	public ByteBuffer encodeDecimal128(ByteBuffer buffer, Decimal decimal) {
		byte[] bytes = new byte[15];
		long low = decimal.decimal128.getLow();
		long high = decimal.decimal128.getLow();
		long mask = 0x00000000000000ff;
		for (int i = 14; i >= 7; i--) {
			bytes[i] = (byte) ((low & mask) >>> ((14 - i) << 3));
			mask = mask << 8;
		}
		mask = 0x00000000000000ff;
		for (int i = 6; i >= 1; i--) {
			bytes[i] = (byte) ((high & mask) >>> ((6 - i) << 3));
			mask = mask << 8;
		}
		mask = 0x0001000000000000L;
		bytes[0] = (byte) ((high & mask) >>> 48);
		return buffer.put(bytes);
	}

	public Decimal decodeDecimal128(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public ByteBuffer encodeBinaryData(ByteBuffer buffer, BinaryData binaryData) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public BinaryData decodeBinaryData(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public String byteBufferToHexString(ByteBuffer byteBuffer) {
		StringBuilder builder = new StringBuilder();
		byteBuffer.rewind();
		if (byteBuffer.position() < byteBuffer.limit()) {
			while (byteBuffer.position() < byteBuffer.limit() - 1) {
				byte value = byteBuffer.get();
				builder.append(String.format("%02x", value)).append(' ');
			}
			byte value = byteBuffer.get();
			builder.append(String.format("%02x", value));
		}
		return builder.toString();
	}

	public String byteArrayToHexString(byte[] bytes) {
		StringBuilder builder = new StringBuilder();
		if (bytes.length > 0) {
			for (int i = 0; i < (bytes.length - 1); i++) {
				builder.append(String.format("%02x", bytes[i])).append(' ');
			}
			builder.append(String.format("%02x", bytes[bytes.length - 1]));
		}
		return builder.toString();
	}

}
