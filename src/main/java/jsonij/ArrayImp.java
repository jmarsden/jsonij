/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayImp<E extends Value> extends Value implements java.util.List<E> {

	/**
	 * Generated Serial UID
	 */
	private static final long serialVersionUID = -8216125868251081773L;
	/**
	 * Array Contents Holder
	 */
	protected List<E> arrayValue;

	public ArrayImp() {
		arrayValue = new ArrayList<E>();
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#internalType()
	 */
	@Override
	public ValueType internalType() {
		return ValueType.ARRAY;
	}

	/*
	 * (non-Javadoc) @see java.util.List#add(java.lang.Object)
	 */
	@Override
	public boolean add(E e) {
		return arrayValue.add(e);
	}

	/*
	 * (non-Javadoc) @see java.util.List#add(int, java.lang.Object)
	 */
	@Override
	public void add(int index, E element) {
		arrayValue.add(index, element);
	}

	/*
	 * (non-Javadoc) @see java.util.List#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends E> c) {
		return arrayValue.addAll(c);
	}

	/*
	 * (non-Javadoc) @see java.util.List#addAll(int, java.util.Collection)
	 */
	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return arrayValue.addAll(index, c);
	}

	/*
	 * (non-Javadoc) @see java.util.List#clear()
	 */
	@Override
	public void clear() {
		arrayValue.clear();
	}

	/*
	 * (non-Javadoc) @see java.util.List#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		return arrayValue.contains(o);
	}

	/*
	 * (non-Javadoc) @see java.util.List#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		return arrayValue.containsAll(c);
	}

	/*
	 * (non-Javadoc) @see java.util.List#get(int)
	 */
	@Override
	public E get(int index) {
		return arrayValue.get(index);
	}

	/*
	 * (non-Javadoc) @see java.util.List#indexOf(java.lang.Object)
	 */
	@Override
	public int indexOf(Object o) {
		return arrayValue.indexOf(o);
	}

	/*
	 * (non-Javadoc) @see java.util.List#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return arrayValue.isEmpty();
	}

	/*
	 * (non-Javadoc) @see java.util.List#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return arrayValue.iterator();
	}

	/*
	 * (non-Javadoc) @see java.util.List#lastIndexOf(java.lang.Object)
	 */
	@Override
	public int lastIndexOf(Object o) {
		return arrayValue.lastIndexOf(o);
	}

	/*
	 * (non-Javadoc) @see java.util.List#listIterator()
	 */
	@Override
	public ListIterator<E> listIterator() {
		return arrayValue.listIterator();
	}

	/*
	 * (non-Javadoc) @see java.util.List#listIterator(int)
	 */
	@Override
	public ListIterator<E> listIterator(int index) {
		return arrayValue.listIterator();
	}

	/*
	 * (non-Javadoc) @see java.util.List#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		return arrayValue.remove(o);
	}

	/*
	 * (non-Javadoc) @see java.util.List#remove(int)
	 */
	@Override
	public E remove(int index) {
		return arrayValue.get(index);
	}

	/*
	 * (non-Javadoc) @see java.util.List#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		return arrayValue.removeAll(c);
	}

	/*
	 * (non-Javadoc) @see java.util.List#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		return arrayValue.retainAll(c);
	}

	/*
	 * (non-Javadoc) @see java.util.List#set(int, java.lang.Object)
	 */
	@Override
	public E set(int index, E element) {
		return arrayValue.set(index, element);
	}

	/*
	 * (non-Javadoc) @see java.util.List#size()
	 */
	@Override
	public int size() {
		return arrayValue.size();
	}

	/*
	 * (non-Javadoc) @see java.util.List#subList(int, int)
	 */
	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		return arrayValue.subList(fromIndex, toIndex);
	}

	/*
	 * (non-Javadoc) @see java.util.List#toArray()
	 */
	@Override
	public Object[] toArray() {
		return arrayValue.toArray();
	}

	/*
	 * (non-Javadoc) @see java.util.List#toArray(T[])
	 */
	@SuppressWarnings("hiding")
	@Override
	public <E> E[] toArray(E[] a) {
		return arrayValue.toArray(a);
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#nestedSize()
	 */
	@Override
	public int nestedSize() {
		int c = 0;
		for (E e : this) {
			c += e.nestedSize();
		}
		return size() + c;
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#toJSON()
	 */
	@Override
	public String toJSON() {
		Iterator<E> valueIterator = iterator();
		if (valueIterator.hasNext()) {
			StringBuilder jsonStringBuilder = new StringBuilder();
			jsonStringBuilder.append((char) Constants.OPEN_ARRAY);
			Value value = valueIterator.next();
			if (value != null) {
				jsonStringBuilder.append(value.toJSON());
			} else {
				jsonStringBuilder.append(JSON.NULL.toJSON());
			}
			while (valueIterator.hasNext()) {
				value = valueIterator.next();
				if (value != null) {
					jsonStringBuilder.append((char) Constants.VALUE_SEPARATOR).append(value.toJSON());
				} else {
					jsonStringBuilder.append((char) Constants.VALUE_SEPARATOR).append(JSON.NULL.toJSON());
				}
			}
			jsonStringBuilder.append((char) Constants.CLOSE_ARRAY);
			return jsonStringBuilder.toString();
		} else {
			return "[]";
		}
	}

	@Override
	public ByteBuffer toBSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
