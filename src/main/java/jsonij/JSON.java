/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.nio.ByteBuffer;

import static jsonij.Constants.*;

/**
 * JSON document class.
 *
 * This class defines the representation of each of the JSON types and provides
 * methods to parse and create JSON instances from various sources. All JSON
 * types extend Value. The following table documents which class to use when
 * representing each of the JSON types.
 *
 * <table summary="JSON Types">
 * <tr>
 * <td style="border:1px solid black;"><strong>JSON Type</strong></td>
 * <td style="border:1px solid black;"><strong>Class</strong></td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">Object</td>
 * <td style="border:1px solid black;">JSON.Object&lt;String,Value&gt;</td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">Array</td>
 * <td style="border:1px solid black;">JSON.Array&lt;Value&gt;</td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">String</td>
 * <td style="border:1px solid black;">JSON.String</td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">Numeric</td>
 * <td style="border:1px solid black;">JSON.Numeric</td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">True</td>
 * <td style="border:1px solid black;">JSON.TRUE</td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">False</td>
 * <td style="border:1px solid black;">JSON.FALSE</td>
 * </tr>
 * <tr>
 * <td style="border:1px solid black;">Null</td>
 * <td style="border:1px solid black;">JSON.NULL</td>
 * </tr>
 * </table>
 *
 * All types are defined in this class.
 *
 * @author J.W.Marsden
 * @version 1.0.0
 */
public class JSON {

	/**
	 * Root Value for the JSON Document.
	 */
	protected Value root;
	/**
	 * Static Instance holding the JSON True instance. Reference this instead of
	 * constructing new JSON.True instances.
	 */
	public static final True TRUE;
	/**
	 * Static Instance holding the JSON False instance. Reference this instead of
	 * constructing new JSON.False instances.
	 */
	public static final False FALSE;
	/**
	 * Static Instance holding the JSON Null instance. Reference this instead of
	 * constructing new JSON.Null instances.
	 */
	public static final Null NULL;

	static {
		TRUE = new JSON.True();
		FALSE = new JSON.False();
		NULL = new JSON.Null();
	}

	/**
	 * Default JSON constructor. Requires the JSON root Value.
	 *
	 * @param root The root JSON value
	 */
	public JSON(Value root) {
		if (root == null) {
			throw new NullPointerException("JSON Root Cannot be Null.");
		}
		if (root.internalType() != ValueType.ARRAY && root.internalType() != ValueType.OBJECT) {
			throw new RuntimeException("JSON can only be constructed from Arrays and Objects.");
		}
		this.root = root;
	}

	/**
	 * Accessor for the root value for this JSON Document.
	 *
	 * @return The root Value.
	 */
	public Value getRoot() {
		return root;
	}

	/**
	 * Package protected mutator for this JSON document.
	 *
	 * @param root The root to set for the JSON document.
	 */
	void setRoot(Value root) {
		if (root == null) {
			throw new NullPointerException("JSON Root Cannot be Null.");
		}
		if (root.internalType() != ValueType.ARRAY && root.internalType() != ValueType.OBJECT) {
			throw new RuntimeException("JSON can only be constructed from Arrays and Objects.");
		}
		this.root = root;
	}

	/**
	 * Size inspector for the root JSON Value. If the root is an object or an array
	 * this will return the dimension.
	 *
	 * @return The size of the root JSON Value.
	 * @see jsonij.Value#size()
	 */
	public int size() {
		return getRoot().size();
	}

	/**
	 * Null check for the root JSON Value.
	 *
	 * @return Boolean if the root value is JSON.Null.
	 * @see jsonij.Value#isNull()
	 */
	public boolean isNull() {
		return getRoot().isNull();
	}

	/**
	 * Boolean accessor for the root JSON Value.
	 *
	 * @return Boolean Value for the root JSON Value.
	 * @see jsonij.Value#getBoolean()
	 */
	public boolean getBoolean() {
		return getRoot().getBoolean();
	}

	/**
	 * Integer accessor for the root JSON Value.
	 *
	 * @return int Value for the root JSON Value.
	 * @see jsonij.Value#getInt()
	 */
	public int getInt() {
		return getRoot().getInt();
	}

	/**
	 * Double accessor for the root JSON Value.
	 *
	 * @return double Value for the root JSON Value.
	 * @see jsonij.Value#getDouble()
	 */
	public double getDouble() {
		return getRoot().getDouble();
	}

	/**
	 * String accessor for the root JSON Value.
	 *
	 * @return java.lang.String Value for the root JSON Value.
	 * @see jsonij.Value#getString()
	 */
	public java.lang.String getString() {
		return getRoot().getString();
	}

	/**
	 * Value accessor by index for the root JSON Value.
	 *
	 * @param i The index to access.
	 * @return Value Value for the root JSON Value.
	 * @see jsonij.Value#get(int i)
	 */
	public Value get(int i) {
		return getRoot().get(i);
	}

	/**
	 * Value accessor by String for the root JSON Value. This method only functions
	 * if the root value is a JSON Object.
	 *
	 * @param key The key to access.
	 * @return Value Value for the root JSON Value.
	 */
	public Value get(java.lang.String key) {
		return getRoot().get(key);
	}

	/**
	 * Converts JSON Document into a valid JSON String.
	 *
	 * @return The JSON String.
	 */
	public java.lang.String toJSON() {
		return getRoot().toJSON();
	}

	@Override
	public java.lang.String toString() {
		return "JSON@" + Integer.toHexString(hashCode()) + ":" + getRoot().toString();
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (o == null) {
			return false;
		}
		if (!getClass().equals(o.getClass())) {
			return false;
		}
		return getRoot().equals(((JSON) o).getRoot());
	}

	@Override
	public int hashCode() {
		if (root == null) {
			return 0;
		} else {
			return root.hashCode();
		}
	}

	/**
	 * JSON Object. Wrapper for ObjectImp.
	 *
	 * @param <K> The key internalType (must extend JSON.String).
	 * @param <V> The element (must extend Value).
	 */
	public static final class Object<K extends CharSequence, V extends Value> extends ObjectImp<K, V> {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -6693177054713276130L;

		@SuppressWarnings("unchecked")
		public void put(K k, java.lang.String s) {
			put(k, (V) new JSON.String(s));
		}

		@SuppressWarnings("unchecked")
		public void put(K k, byte n) {
			put(k, (V) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void put(K k, short n) {
			put(k, (V) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void put(K k, int n) {
			put(k, (V) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void put(K k, double n) {
			put(k, (V) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void put(K k, float n) {
			put(k, (V) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void put(K k, boolean b) {
			if (b) {
				put(k, (V) JSON.TRUE);
			} else {
				put(k, (V) JSON.FALSE);
			}
		}

		@SuppressWarnings("unchecked")
		public void putNull(K k) {
			put(k, (V) JSON.NULL);
		}

	}

	/**
	 * JSON Array. Wrapper for ArrayImp.
	 *
	 * @param <E> The element that the Array will hold (must extend Value).
	 */
	public static final class Array<E extends Value> extends ArrayImp<E> {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -4044562650881178384L;

		@SuppressWarnings("unchecked")
		public void add(java.lang.String s) {
			add((E) new JSON.String(s));
		}

		@SuppressWarnings("unchecked")
		public void add(byte n) {
			add((E) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void add(short n) {
			add((E) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void add(int n) {
			add((E) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void add(double n) {
			add((E) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void add(float n) {
			add((E) new JSON.Numeric(n));
		}

		@SuppressWarnings("unchecked")
		public void add(boolean b) {
			if (b) {
				add((E) JSON.TRUE);
			} else {
				add((E) JSON.FALSE);
			}
		}

		@SuppressWarnings("unchecked")
		public void addNull() {
			add((E) JSON.NULL);
		}
	}

	/**
	 * JSON Numeric. Wrapper for NumericImp.
	 */
	public static final class Numeric extends NumericImp {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -5098879252150523422L;

		/**
		 * Default Constructor.
		 *
		 * @param value The java.lang.Number Value.
		 */
		public Numeric(int value) {
			super(value);
		}

		public Numeric(long value) {
			super(value);
		}

		public Numeric(float value) {
			super(value);
		}

		public Numeric(double value) {
			super(value);
		}

		public Numeric(Number numberValue) {
			super(numberValue);
		}

		public Numeric(java.lang.String numberString) {
			super(numberString);
		}
	}

	/**
	 * JSON String. Wrapper for StringImp.
	 */
	public static final class String extends StringImp {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -2499952452764709487L;

		/**
		 * Default Constructor.
		 *
		 * @param s The java.lang.String value.
		 */
		public String(java.lang.String s) {
			super(s);
		}

		public static String getValue(java.lang.String string) {
			return new JSON.String(string);
		}
	}

	/**
	 * JSON Boolean. Common parent to True and False.
	 */
	public static abstract class Boolean extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 2008468998971968312L;

		public static Boolean getValue(boolean bool) {
			if (bool) {
				return JSON.TRUE;
			} else {
				return JSON.FALSE;
			}
		}
	}

	/**
	 * JSON True Implementation.
	 */
	public static final class True extends Boolean {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = 9058009765879934009L;
		/**
		 * The String for True.
		 */
		public static final java.lang.String VALUE;

		static {
			VALUE = "true";
		}

		/**
		 * Default Constructor. Use JSON.TRUE to get an instance of this Object.
		 */
		private True() {
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#internalType()
		 */
		@Override
		public ValueType internalType() {
			return ValueType.TRUE;
		}

		/*
		 * (non-Javadoc) @see openecho.json.JSON.Boolean#getBoolean()
		 */
		@Override
		public boolean getBoolean() {
			return true;
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#nestedSize()
		 */
		@Override
		public int nestedSize() {
			return 1;
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#toJSON()
		 */
		@Override
		public java.lang.String toJSON() {
			return VALUE;
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			buffer.put(BSON_TRUE);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			buffer.put(UBJSON_TRUE);
			return buffer;
		}

		/*
		 * (non-Javadoc) @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			int hash = 5 + toJSON().hashCode();
			return hash;
		}

		/*
		 * (non-Javadoc) @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(java.lang.Object obj) {
			if (obj instanceof JSON.True) {
				return true;
			}
			return false;
		}
	}

	/**
	 * JSON False Implementation.
	 */
	public static final class False extends Boolean {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -8602585595611232921L;
		/**
		 * The String for False.
		 */
		public static final java.lang.String VALUE;

		static {
			VALUE = "false";
		}

		/**
		 * Default Constructor. Use JSON.FALSE to get an instance of this Object.
		 */
		private False() {
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#internalType()
		 */
		@Override
		public ValueType internalType() {
			return ValueType.FALSE;
		}

		/*
		 * (non-Javadoc) @see openecho.json.JSON.Boolean#getBoolean()
		 */
		@Override
		public boolean getBoolean() {
			return false;
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#nestedSize()
		 */
		@Override
		public int nestedSize() {
			return 1;
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#toJSON()
		 */
		@Override
		public java.lang.String toJSON() {
			return VALUE;
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			buffer.put(BSON_FALSE);
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			buffer.put(UBJSON_FALSE);
			return buffer;
		}

		/*
		 * (non-Javadoc) @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			int hash = 3 + toJSON().hashCode();
			return hash;
		}

		/*
		 * (non-Javadoc) @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(java.lang.Object obj) {
			if (obj instanceof JSON.False) {
				return true;
			}
			return false;
		}
	}

	/**
	 * JSON Null Implementation.
	 */
	public static final class Null extends Value {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -2323485713422970959L;
		/**
		 * The String for null.
		 */
		public static final java.lang.String VALUE;

		static {
			VALUE = "null";
		}

		/**
		 * Default Constructor. Use JSON.NULL to get an instance of this Object.
		 */
		private Null() {
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#internalType()
		 */
		@Override
		public ValueType internalType() {
			return ValueType.NULL;
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#nestedSize()
		 */
		@Override
		public int nestedSize() {
			return 0;
		}

		/*
		 * (non-Javadoc) @see openecho.json.Value#toJSON()
		 */
		@Override
		public java.lang.String toJSON() {
			return VALUE;
		}

		@Override
		public ByteBuffer toBSON(ByteBuffer buffer) {
			return buffer;
		}

		@Override
		public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
			buffer.put(UBJSON_NULL);
			return buffer;
		}

		/*
		 * (non-Javadoc) @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			int hash = 7 + toJSON().hashCode();
			return hash;
		}

		/*
		 * (non-Javadoc) @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(java.lang.Object obj) {
			return obj instanceof JSON.Null;
		}
	}
}
