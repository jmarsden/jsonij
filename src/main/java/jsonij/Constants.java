/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

/**
 * JSON/BSON Constant Definition.
 *
 * @author J.W.Marsden
 */
public interface Constants {

	/**
	 * File Constants
	 */
	public static final int BACKSPACE = '\b';
	public static final int TAB = '\t';
	public static final int FORM_FEED = '\f';
	public static final int NEW_LINE = '\n';
	public static final int CARRIAGE_RETURN = '\r';
	public static final int SPACE = ' ';
	public static final int ESCAPE = '\\';
	public static final int QUOTATION_MARK = '\"';
	public static final int COMMENT = '-';
	/**
	 * JSON Constants
	 */
	public static final int OPEN_ARRAY = '[';
	public static final int OPEN_OBJECT = '{';
	public static final int CLOSE_ARRAY = ']';
	public static final int CLOSE_OBJECT = '}';
	public static final int VALUE_SEPARATOR = ',';
	public static final int NAME_SEPARATOR = ':';
	public static final int[] ARRAY_KEY_VALUE_TERMINALS = new int[] { VALUE_SEPARATOR, CLOSE_ARRAY };
	public static final int[] OBJECT_KEY_VALUE_TERMINALS = new int[] { VALUE_SEPARATOR, CLOSE_OBJECT };
	/**
	 * JSON Numeric Constants
	 */
	public static final char[] DIGITS = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	public static final int DECIMAL_POINT = '.';
	public static final int MINUS = '-';
	public static final int PLUS = '+';
	public static final char[] EXPS = new char[] { 'e', 'E' };
	/**
	 * JSON String Constants
	 */
	public static final int SINGLE_QUOTE = '\'';
	public static final int DOUBLE_QUOTE = '"';
	public static final int REVERSE_SOLIDUS = '\\';
	public static final int SOLIDUS_CHAR = '/';
	public static final int BACKSPACE_CHAR = 'b';
	public static final int FORM_FEED_CHAR = 'f';
	public static final int NEW_LINE_CHAR = 'n';
	public static final int CARRIAGE_RETURN_CHAR = 'r';
	public static final int TAB_CHAR = 't';
	public static final int HEX_CHAR = 'u';
	public static final char[] HEXDIGITS = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
			'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F' };
	/**
	 * Other JSON Types
	 */
	public static final String TRUE_STR = "true";
	public static final String FALSE_STR = "false";
	public static final String NULL_STR = "null";
	/**
	 * JPath Constants
	 */
	public static final int DOLLARS_CHAR = '$';
	public static final int LEFT_SQUARE_BRACKET = '[';
	public static final int RIGHT_SQUARE_BRACKET = ']';
	public static final int LEFT_PARENTHESIS = '(';
	public static final int RIGHT_PARENTHESIS = ')';
	public static final int ALL_CHAR = '*';
	public static final int LAST_CHAR = '$';
	public static final int SINGLE_QUOTE_CHAR = '\'';
	public static final int EXPRESSION_CHAR = '(';
	public static final int FILTER_CHAR = '?';
	public static final int PERIOD_CHAR = '.';
	public static final int CURRENT_ELEMENT_CHAR = '@';
	public static final int COMMA_CHAR = ',';
	public static final int COLON_CHAR = ':';
	public static final int LESS = '<';
	public static final int GREATER = '>';
	public static final int EQUAL = '=';
	public static final int AND = '&';
	public static final int OR = '|';
	public static final String LAST_PREDICATE = "last()";
	public static final String POSITION_PREDICATE = "position()";
	/**
	 * BSON Constants
	 */
	public static final byte BSON_TERMINATION = 0x00;
	public static final byte BSON_DOUBLE = 0x01;
	public static final byte BSON_STRING = 0x02;
	public static final byte BSON_DOCUMENT = 0x03;
	public static final byte BSON_ARRAY = 0x04;
	public static final byte BSON_BINARY_DATA = 0x05;
	public static final byte BSON_UNDEFINED_1 = 0x06;
	public static final byte BSON_OBJECT_ID = 0x07;
	public static final byte BSON_BOOLEAN = 0x08;
	public static final byte BSON_UTC_DATE_TIME = 0x09;
	public static final byte BSON_NULL = 0x0A;
	public static final byte BSON_REGEX = 0x0B;
	public static final byte BSON_DB_POINTER = 0x0C;
	public static final byte BSON_JS_CODE = 0x0D;
	public static final byte BSON_UNDEFINED_2 = 0x0E;
	public static final byte BSON_JS_CODE_WS = 0x0F;
	public static final byte BSON_INT32 = 0x10;
	public static final byte BSON_TIMESTAMP = 0x11;
	public static final byte BSON_INT64 = 0x12;
	public static final byte BSON_DECIMAL128 = 0x13;
	public static final byte BSON_MIN_KEY = (byte) 0xFF;
	public static final byte BSON_MAX_KEY = 0x7F;
	public static final byte BSON_FALSE = 0x0;
	public static final byte BSON_TRUE = 0x1;
	public static final byte TYPE_CHECK = 0x55;
	public static final byte BSON_BINARY_GENERIC = 0x00;
	public static final byte BSON_BINARY_FUNCTION = 0x01;
	public static final byte BSON_BINARY_BINARY = 0x02;
	public static final byte BSON_BINARY_UID_OLD = 0x03;
	public static final byte BSON_BINARY_UID = 0x04;
	public static final byte BSON_BINARY_USER_DEFINED_BEGIN = (byte) 0x80;
	public static final byte BSON_BINARY_USER_DEFINED_END = (byte) 0xFF;
	public static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
	/**
	 * UBJSON Constants
	 */
	public static final byte UBJSON_NULL = 'Z';
	public static final byte UBJSON_NO_OP = 'N';
	public static final byte UBJSON_TRUE = 'T';
	public static final byte UBJSON_FALSE = 'F';
	public static final byte UBJSON_INT8 = 'i';
	public static final byte UBJSON_UINT8 = 'U';
	public static final byte UBJSON_INT16 = 'I';
	public static final byte UBJSON_INT32 = 'l';
	public static final byte UBJSON_INT64 = 'L';
	public static final byte UBJSON_FLOAT32 = 'd';
	public static final byte UBJSON_FLOAT64 = 'D';
	public static final byte UBJSON_HIGH_PRECISION_NUMBER = 'H';
	public static final byte UBJSON_CHAR = 'C';
	public static final byte UBJSON_STRING = 'S';
	public static final byte UBJSON_OPEN_ARRAY = '[';
	public static final byte UBJSON_OPEN_OBJECT = '{';
	public static final byte UBJSON_CLOSE_ARRAY = ']';
	public static final byte UBJSON_CLOSE_OBJECT = '}';
}
