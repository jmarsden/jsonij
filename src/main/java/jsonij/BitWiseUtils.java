/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

public class BitWiseUtils {

	public final static short BYTE_BIT_COUNT = 8;
	public final static short SHORT_BIT_COUNT = 16;
	public final static short INT_BIT_COUNT = 32;
	public final static short LONG_BIT_COUNT = 64;
	public final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static final String convertToBinaryString(byte value) {
		StringBuilder outputBuilder = new StringBuilder();
		for (int i = 7; i >= 0; i--) {
			outputBuilder.append(value >> i & 0x1);
		}
		return outputBuilder.toString();
	}

	public static final String convertToBinaryString(short value) {
		StringBuilder outputBuilder = new StringBuilder();
		for (int i = 15; i >= 0; i--) {
			outputBuilder.append(value >> i & 0x1);
		}
		return outputBuilder.toString();
	}

	public static final String convertToBinaryString(int value) {
		StringBuilder outputBuilder = new StringBuilder();
		for (int i = 31; i >= 0; i--) {
			outputBuilder.append(value >> i & 0x1);
		}
		return outputBuilder.toString();
	}

	public static final String convertToHexString(byte value) {
		return "" + HEX_ARRAY[value >>> 4] + HEX_ARRAY[value & 0x0F];
	}

	public static final String convertToHexString(short value) {
		return "" + HEX_ARRAY[value >>> 12] + HEX_ARRAY[value >>> 8 & 0x0F] + HEX_ARRAY[value >>> 4 & 0x0F]
				+ HEX_ARRAY[value & 0x0F];
	}

	public static final String convertToHexString(int value) {
		return "" + HEX_ARRAY[value >>> 28] + HEX_ARRAY[value >>> 24 & 0x0F] + HEX_ARRAY[value >>> 20 & 0x0F]
				+ HEX_ARRAY[value >>> 16 & 0x0F] + HEX_ARRAY[value >>> 12 & 0x0F] + HEX_ARRAY[value >>> 8 & 0x0F]
				+ HEX_ARRAY[value >>> 4 & 0x0F] + HEX_ARRAY[value & 0x0F];
	}

	public static final boolean extractBooleanFromBits(byte value, int offset) throws BitWiseException {
		if (offset > BYTE_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		return (value & (0x1 << (BYTE_BIT_COUNT - offset - 1))) != 0;
	}

	public static final boolean extractBooleanFromBits(short value, int offset) throws BitWiseException {
		if (offset > SHORT_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		return (value & (0x1 << (SHORT_BIT_COUNT - offset - 1))) != 0;
	}

	public static final byte extractByteFromBits(byte value, int offset, int bitCount) throws BitWiseException {
		if (offset + bitCount > BYTE_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		byte recycleType = 0;
		int rotate = BYTE_BIT_COUNT - offset - bitCount;
		for (int i = offset; i < offset + bitCount; i++) {
			recycleType |= ((value & (0x1 << (BYTE_BIT_COUNT - i - 1))) >>> rotate);
		}
		return recycleType;
	}

	public static final byte extractShortFromBits(short value, int offset, int bitCount) throws BitWiseException {
		if (offset + bitCount > SHORT_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		byte recycleType = 0;
		int rotate = SHORT_BIT_COUNT - offset - bitCount;
		for (int i = offset; i < offset + bitCount; i++) {
			recycleType |= ((value & (0x1 << (SHORT_BIT_COUNT - i - 1))) >>> rotate);
		}
		return recycleType;
	}

	public static final byte extractByteFromBits(short value, int offset, int bitCount) throws BitWiseException {
		if (offset + bitCount > SHORT_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		byte recycleType = 0;
		int rotate = SHORT_BIT_COUNT - offset - bitCount;
		for (int i = offset; i < offset + bitCount; i++) {
			recycleType |= ((value & (0x1 << (SHORT_BIT_COUNT - i - 1))) >>> rotate);
		}
		return recycleType;
	}

	public static final byte extractByteFromBits(int value, int offset, int bitCount) throws BitWiseException {
		if (offset + bitCount > INT_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		byte recycleType = 0;
		int rotate = INT_BIT_COUNT - offset - bitCount;
		for (int i = offset; i < offset + bitCount; i++) {
			recycleType |= ((value & (0x1 << (INT_BIT_COUNT - i - 1))) >>> rotate);
		}
		return recycleType;
	}

	public static final byte extractByteFromBits(long value, int offset, int bitCount) throws BitWiseException {
		if (offset + bitCount > LONG_BIT_COUNT) {
			throw new BitWiseException("overflow");
		}
		byte recycleType = 0;
		int rotate = LONG_BIT_COUNT - offset - bitCount;
		for (int i = offset; i < offset + bitCount; i++) {
			recycleType |= ((value & (0x1 << (LONG_BIT_COUNT - i - 1))) >>> rotate);
		}
		return recycleType;
	}
}
