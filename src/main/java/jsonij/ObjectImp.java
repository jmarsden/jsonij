/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import static jsonij.Constants.CLOSE_OBJECT;
import static jsonij.Constants.NAME_SEPARATOR;
import static jsonij.Constants.OPEN_OBJECT;
import static jsonij.Constants.QUOTATION_MARK;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ObjectImp<CS extends CharSequence, V extends Value> extends Value implements java.util.Map<CS, V> {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 7601285884315492844L;
	/**
	 * Holds the Mapping Values for the Object
	 */
	public Map<CS, V> mapValue;

	/**
	 * Default Constructor
	 */
	public ObjectImp() {
		mapValue = new LinkedHashMap<CS, V>();
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#internalType()
	 */
	@Override
	public ValueType internalType() {
		return ValueType.OBJECT;
	}

	/*
	 * (non-Javadoc) @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		mapValue.clear();
	}

	/*
	 * (non-Javadoc) @see java.util.Map#containsKey(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean containsKey(Object key) {
		return mapValue.containsKey((CS) key);
	}

	/*
	 * (non-Javadoc) @see java.util.Map#containsValue(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean containsValue(Object value) {
		return mapValue.containsValue((V) value);
	}

	/*
	 * (non-Javadoc) @see java.util.Map#entrySet()
	 */
	@Override
	public Set<java.util.Map.Entry<CS, V>> entrySet() {
		return mapValue.entrySet();
	}

	/*
	 * (non-Javadoc) @see java.util.List#get(java.lang.Integer)
	 */
	@Override
	public V get(int i) {
		Set<CS> keySet = mapValue.keySet();
		if (i < keySet.size()) {
			Iterator<CS> iterator = keySet.iterator();
			for (int j = 0; j < i - 1; j++) {
				iterator.next();
			}
			return mapValue.get((CS) iterator.next());
		}
		return null;
	}

	/*
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		return mapValue.get((CS) key.toString());
	}

	@SuppressWarnings("unchecked")
	public V safeGet(Object key) {
		return mapValue.get((CS) key.toString());
	}

	/*
	 * (non-Javadoc) @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return mapValue.isEmpty();
	}

	/*
	 * (non-Javadoc) @see java.util.Map#keySet()
	 */
	@Override
	public Set<CS> keySet() {
		return mapValue.keySet();
	}

	/*
	 * (non-Javadoc) @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V put(CS key, V value) {
		CS keySequence = (CS) key.toString();
		return mapValue.put(keySequence, value);
	}

	/*
	 * (non-Javadoc) @see java.util.Map#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends CS, ? extends V> m) {
		mapValue.putAll(m);
	}

	/*
	 * (non-Javadoc) @see java.util.Map#remove(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V remove(Object key) {
		CS keySequence = (CS) key.toString();
		return mapValue.remove(keySequence);
	}

	/*
	 * (non-Javadoc) @see java.util.Map#size()
	 */
	@Override
	public int size() {
		return mapValue.size();
	}

	/*
	 * (non-Javadoc) @see java.util.Map#values()
	 */
	@Override
	public Collection<V> values() {
		return mapValue.values();
	}

	@Override
	public int nestedSize() {
		int c = 0;
		for (V v : values()) {
			c += v.nestedSize();
		}
		return size() + c;
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#toJSON()
	 */
	@Override
	public String toJSON() {
		CS k;
		Value v;
		Iterator<CS> keyIterator = mapValue.keySet().iterator();
		if (keyIterator.hasNext()) {
			StringBuilder json = new StringBuilder();
			json.append((char) OPEN_OBJECT);
			k = (CS) keyIterator.next();
			v = get(k);
			json.append((char) QUOTATION_MARK).append(k.toString()).append((char) QUOTATION_MARK)
					.append((char) NAME_SEPARATOR).append(v.toJSON());
			while (keyIterator.hasNext()) {
				k = (CS) keyIterator.next();
				v = get(k);
				json.append((char) ',').append((char) QUOTATION_MARK).append(k.toString()).append((char) QUOTATION_MARK)
						.append((char) NAME_SEPARATOR).append(v.toJSON());
			}
			json.append((char) CLOSE_OBJECT);
			return json.toString();
		} else {
			return "{}";
		}
	}

	@Override
	public ByteBuffer toBSON(ByteBuffer buffer) {
		int startPosition = buffer.position();
		buffer.putInt(0);

		CS k;
		Value v;
		Iterator<CS> keyIterator = mapValue.keySet().iterator();

		if (keyIterator.hasNext()) {

			while (keyIterator.hasNext()) {

				k = (CS) keyIterator.next();
				v = get(k);

				ValueType type = v.getValueType();
				buffer.put(type.getBSONType());
				BSON.CODEC.encodeCString(buffer, k.toString());
				v.toBSON(buffer);
			}
		}
		buffer.put(jsonij.Constants.BSON_TERMINATION);

		int finishPosition = buffer.position();

		int size = finishPosition - startPosition;

		buffer.putInt(startPosition, size);
		buffer.position(finishPosition);
		buffer.flip();
		return buffer;
	}

	@Override
	public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
