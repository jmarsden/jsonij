/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.io.Serializable;
import java.nio.ByteBuffer;

public class DoubleInternalNumber extends Number implements InternalNumber, Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 7240423424673550573L;

	double value;

	public DoubleInternalNumber(double value) {
		this.value = value;
	}

	@Override
	public byte byteValue() {
		return new Double(value).byteValue();
	}

	@Override
	public double doubleValue() {
		return value;
	}

	@Override
	public float floatValue() {
		return new Double(value).floatValue();
	}

	@Override
	public int intValue() {
		return new Double(value).intValue();
	}

	@Override
	public long longValue() {
		return new Double(value).longValue();
	}

	@Override
	public short shortValue() {
		return new Double(value).shortValue();
	}

	@Override
	public Number getNumber() {
		return value;
	}

	@Override
	public int nestedSize() {
		return 0;
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#toJSON()
	 */
	@Override
	public String toJSON() {
		if (value % 1 != 0 || value >= Long.MAX_VALUE || value <= Long.MIN_VALUE) {
			return "" + value;
		} else {
			return "" + (long) value;
		}
	}

	@Override
	public ByteBuffer toBSON(ByteBuffer buffer) {
		return BSON.CODEC.encodeDouble(buffer, value);
	}

	@Override
	public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
