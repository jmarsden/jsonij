/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;

public class NumericImp extends Value implements Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 7016727603513295556L;

	public final int LONG = 1;
	public final int FLOAT = 2;
	public final int DOUBLE = 3;
	public final int STRING = 4;
	public final int NUMERIC = 5;

	private final int type;
	private long longValue;
	private float floatValue;
	private double doubleValue;

	private InternalNumber internalNumberData;
	private String value;

	public NumericImp(int value) {
		longValue = value;
		type = LONG;
	}

	public NumericImp(long value) {
		longValue = value;
		type = LONG;
	}

	public NumericImp(float value) {
		floatValue = value;
		type = FLOAT;
	}

	public NumericImp(double value) {
		doubleValue = value;
		type = DOUBLE;
	}

	public NumericImp(String value) {
		this.value = value;
		internalNumberData = null;
		type = STRING;
	}

	public NumericImp(Number numberValue) {
		if (numberValue == null) {
			internalNumberData = new LongInternalNumber(0);
		}
		internalNumberData = new NumberInternalNumber(numberValue);
		type = NUMERIC;
	}

	/*
	 * (non-Javadoc) @see jsonij.Value#internalType()
	 */
	@Override
	public ValueType internalType() {
		return ValueType.NUMERIC;
	}

	public byte byteValue() {
		if (type == LONG) {
			return (byte) longValue;
		}
		if (type == FLOAT) {
			return (byte) floatValue;
		}
		if (type == DOUBLE) {
			return (byte) doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber().byteValue();
	}

	public double doubleValue() {
		if (type == LONG) {
			return (double) longValue;
		}
		if (type == FLOAT) {
			return (double) floatValue;
		}
		if (type == DOUBLE) {
			return doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber().doubleValue();
	}

	public float floatValue() {
		if (type == LONG) {
			return (float) longValue;
		}
		if (type == FLOAT) {
			return floatValue;
		}
		if (type == DOUBLE) {
			return (float) doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber().floatValue();
	}

	public int intValue() {
		if (type == LONG) {
			return (int) longValue;
		}
		if (type == FLOAT) {
			return (int) floatValue;
		}
		if (type == DOUBLE) {
			return (int) doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber().intValue();
	}

	public long longValue() {
		if (type == LONG) {
			return longValue;
		}
		if (type == FLOAT) {
			return (long) floatValue;
		}
		if (type == DOUBLE) {
			return (long) doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber().longValue();
	}

	public short shortValue() {
		if (type == LONG) {
			return (short) longValue;
		}
		if (type == FLOAT) {
			return (short) floatValue;
		}
		if (type == DOUBLE) {
			return (short) doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber().shortValue();
	}

	public void parseInternalValue() {
		int s = value.length();
		boolean decimalFlag = value.contains(".");
		boolean exponetFlag = value.contains("e") || value.contains("E");
		if (!decimalFlag && !exponetFlag) {
			if (s < 10) {
				internalNumberData = new LongInternalNumber(Integer.parseInt(value));
			} else if (s < 18) {
				internalNumberData = new LongInternalNumber(Long.parseLong(value));
			} else {
				internalNumberData = new NumberInternalNumber(new BigInteger(value));
			}
		} else {
			if (s < 10) {
				internalNumberData = new DoubleInternalNumber(Float.parseFloat(value));
			} else if (s < 16) {
				internalNumberData = new DoubleInternalNumber(Float.parseFloat(value));
			} else {
				internalNumberData = new NumberInternalNumber(new BigDecimal(value));
			}
		}
	}

	@Override
	public Number getNumber() {
		if (type == LONG) {
			return longValue;
		}
		if (type == FLOAT) {
			return floatValue;
		}
		if (type == DOUBLE) {
			return doubleValue;
		}
		if (type == STRING && internalNumberData == null) {
			parseInternalValue();
		}
		return internalNumberData.getNumber();
	}

	@Override
	public int nestedSize() {
		return 0;
	}

	/*
	 * (non-Javadoc) @see json.Value#toJSON()
	 */
	@Override
	public String toJSON() {
		if (type == LONG) {
			return "" + longValue;
		}
		if (type == FLOAT) {
			return "" + floatValue;
		}
		if (type == DOUBLE) {
			return "" + doubleValue;
		}
		return (internalNumberData != null) ? internalNumberData.toJSON() : value;
	}

	@Override
	public ByteBuffer toBSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final NumericImp other = (NumericImp) obj;
		return this.compareTo(other) == 0;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 11 * hash + (type == LONG ? (int) this.longValue : 0);
		hash = 11 * hash + (type == FLOAT ? (int) this.floatValue : 0);
		hash = 11 * hash + (type == DOUBLE ? (int) this.doubleValue : 0);
		hash = 23 * hash + (this.internalNumberData != null ? this.internalNumberData.hashCode() : 0);
		hash = 31 * hash + (this.value != null ? this.value.hashCode() : 0);
		return hash;
	}
}
