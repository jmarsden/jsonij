/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import jsonij.math.random.MersenneTwisterFast;
import java.lang.management.ManagementFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Basic implementation of ObjectIdentifier
 *
 * @author J.W.Marsden
 */
public class ObjectIdentifier {

	public final static char[] HEX_ARRAY = "0123456789abcdef".toCharArray();
	public final static DateFormat ISO8601_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
	public final static byte LENGTH = 24;
	public static long processID;
	public static MersenneTwisterFast mtf;
	public byte[] contents;

	static {
		processID = getPID();
		mtf = new MersenneTwisterFast();
	}

	public ObjectIdentifier(String byteString) {
		if (byteString.length() != 24) {
			throw new RuntimeException("Bytes must be length of 24");
		}
		byte[] bytes = ObjectIdentifier.hexToBytes(byteString);
		this.contents = bytes;
	}

	public ObjectIdentifier(byte[] bytes) {
		this.contents = bytes;
	}

	public byte[] getContents() {
		return contents;
	}

	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	public long getTimestamp() {
		long timestamp = ((contents[0] & 0xFF) << 24) | ((contents[1] & 0xFF) << 16) | ((contents[2] & 0xFF) << 8)
				| (contents[3] & 0xFF);
		return timestamp * 1000L;
	}

	public String getTimestampString() {
		return ISO8601_FORMAT.format(new Date(getTimestamp()));
	}

	@Override
	public String toString() {
		return bytesToHex(contents);
	}

	public static ObjectIdentifier getObjectID() {
		byte[] bytes = new byte[12];
		int time = (int) (System.currentTimeMillis() / 1000L);
		bytes[0] = (byte) ((time >> 24) & 0xFF);
		bytes[1] = (byte) ((time >> 16) & 0xFF);
		bytes[2] = (byte) ((time >> 8) & 0xFF);
		bytes[3] = (byte) (time & 0xFF);
		int uuid = (int) UUID.randomUUID().getMostSignificantBits();
		bytes[4] = (byte) ((uuid >> 16) & 0xFF);
		bytes[5] = (byte) ((uuid >> 8) & 0xFF);
		bytes[6] = (byte) (uuid & 0xFF);
		short pid = (short) processID;
		bytes[7] = (byte) ((pid >> 8) & 0xFF);
		bytes[8] = (byte) (pid & 0xFF);
		bytes[9] = mtf.nextByte();
		bytes[10] = mtf.nextByte();
		bytes[11] = mtf.nextByte();
		return new ObjectIdentifier(bytes);
	}

	private static long getPID() {
		try {
			String processName = ManagementFactory.getRuntimeMXBean().getName();
			return Long.parseLong(processName.split("@")[0]);
		} catch (NumberFormatException nfe) {
			return mtf.nextLong();
		}
	}

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static byte[] hexToBytes(String byteString) {
		int len = byteString.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(byteString.charAt(i), 16) << 4)
					+ Character.digit(byteString.charAt(i + 1), 16));
		}
		return data;
	}
}
