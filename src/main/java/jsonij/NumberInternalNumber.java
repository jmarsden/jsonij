/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import java.io.Serializable;
import java.nio.ByteBuffer;

public class NumberInternalNumber extends Number implements InternalNumber, Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 8305925440476168109L;

	protected Number value;

	public NumberInternalNumber(Number value) {
		if (value == null) {
			this.value = 0;
		} else {
			this.value = value;
		}
	}

	@Override
	public byte byteValue() {
		return value.byteValue();
	}

	@Override
	public double doubleValue() {
		return value.doubleValue();
	}

	@Override
	public float floatValue() {
		return value.floatValue();
	}

	@Override
	public int intValue() {
		return value.intValue();
	}

	@Override
	public long longValue() {
		return value.longValue();
	}

	@Override
	public short shortValue() {
		return value.shortValue();
	}

	@Override
	public Number getNumber() {
		return value;
	}

	@Override
	public int nestedSize() {
		return 0;
	}

	@Override
	public String toJSON() {
		return value.toString();
	}

	@Override
	public ByteBuffer toBSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ByteBuffer toUniversalBinaryJSON(ByteBuffer buffer) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final NumberInternalNumber other = (NumberInternalNumber) obj;
		return !(this.value != other.value && (this.value == null || !this.value.equals(other.value)));
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 11 * hash + (this.value != null ? this.value.hashCode() : 0);
		return hash;
	}
}
