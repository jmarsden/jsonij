/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 J.W.Marsden
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.referencing;

import jsonij.io.LittleEndianRandomAccessFile;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author John
 */
public class BTreeNode {

    public final byte T;

    public byte[] contents;

    public BTreeNode(byte T) {
        this.T = T;
    }

    /**
     * Copy to the contents of Record
     *
     * @param contents The contents to copy.
     */
    public void setRecord(byte[] contents) {
        System.arraycopy(contents, 0, this.contents, 0, contents.length);
    }

    public byte getInfo() {
        return -1;
    }

    public int getData(byte position) {
        return -1;
    }

    public int getKey(byte[] position) {
        return -1;
    }

    

}
