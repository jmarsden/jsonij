/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 J.W.Marsden
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.referencing;

import jsonij.BitWiseException;

/**
 * Record Reference Interface. 
 *
 * @author J.W.Marsden
 */
public interface RecordReference {

	public boolean isActive() throws BitWiseException;

	public byte getType() throws BitWiseException;

	/**
	 * Copies the 12 data bytes to the destination array.
	 * 
	 * @param destination the destination array for the data.
	 * @return The destination array populated.
	 * @throws BitWiseException BitWise Exception.
	 */
	public byte[] getData(byte[] destination) throws BitWiseException;

	public int getShortData() throws BitWiseException;

	public int getIntData() throws BitWiseException;

	public long getLongData() throws BitWiseException;

	public double getDoubleData() throws BitWiseException;

	public String getUTF8Data() throws BitWiseException;

	public long getCategory() throws BitWiseException;

	public int getExtent() throws BitWiseException;

	public int getOffset() throws BitWiseException;
}