/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 J.W.Marsden
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.referencing;

/**
 *
 * @author John
 */
public interface DBConstants {
    
    public byte RECORD_TYPE_IDENTIFIER = 0x0;
    public byte RECORD_TYPE_BYTE = 0x1;
    public byte RECORD_TYPE_SHORT = 0x2;
    public byte RECORD_TYPE_INT = 0x3;
    public byte RECORD_TYPE_LONG = 0x4;
    public byte RECORD_TYPE_DOUBLE = 0x5;
    public byte RECORD_TYPE_SMALL_UTF8 = 0x6;
    public byte RECORD_TYPE_REFRENCE_UTF8 = 0x6;
    public byte RECORD_TYPE_TIMESTAMP = 0x7;
    public byte RECORD_TYPE_DATE = 0x8;
}
