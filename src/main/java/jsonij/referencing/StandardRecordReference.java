/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 J.W.Marsden
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.referencing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import jsonij.BitWiseException;
import jsonij.BitWiseUtils;

/**
 * Standard Record Reference Implementation. This record has the layout;
 *
 * <pre>
 *
 * | 2B   | 12B  | 4B       | 2B     | 2B     |
 * | Head | Data | Category | Extent | Offset |
 * </pre>
 *
 * Head is 2 byte of header data. The first bit identifies this record as active
 * or recyclable. The next 4 bits define the payload type. The next 3 bits are
 * reserved.
 *
 * Data is 12 bytes of payload information. For some types this will be the
 * direct data. For other types this will be the key to look up the actual
 * payload.
 *
 * Category is 4 bytes of category references for the record.
 *
 * Extent is a reference to the Extent identifier for the record.
 *
 * Offset is the offset within the Extent.
 *
 * @author J.W.Marsden.
 */
public class StandardRecordReference implements RecordReference {

	public static final short BYTE_COUNT;
	public static final short HEAD_SIZE;
	public static final short DATA_SIZE;
	public static final short CATEGORY_SIZE;
	public static final short EXTEND_SIZE;
	public static final short OFFSET_SIZE;

	public static final short HEAD_OFFSET;
	public static final short DATA_OFFSET;
	public static final short CATEGORY_OFFSET;
	public static final short EXTEND_OFFSET;
	public static final short OFFSET_OFFSET;

	public ByteBuffer contents;

	static {
		BYTE_COUNT = 22;
		HEAD_SIZE = 2;
		DATA_SIZE = 12;
		CATEGORY_SIZE = 4;
		EXTEND_SIZE = 2;
		OFFSET_SIZE = 2;

		HEAD_OFFSET = 0;
		DATA_OFFSET = HEAD_SIZE;
		CATEGORY_OFFSET = (short) (HEAD_SIZE + DATA_SIZE);
		EXTEND_OFFSET = (short) (HEAD_SIZE + DATA_SIZE + CATEGORY_SIZE);
		OFFSET_OFFSET = (short) (HEAD_SIZE + DATA_SIZE + CATEGORY_SIZE + EXTEND_SIZE);
	}

	public StandardRecordReference() {
		contents = ByteBuffer.allocate(BYTE_COUNT);
		contents.order(ByteOrder.LITTLE_ENDIAN);
	}

	/**
	 * Copy to the contents of Record
	 *
	 * @param contents The contents to copy.
	 */
	public void setRecord(byte[] contents) {
		this.contents.rewind();
		this.contents.put(contents);
		this.contents.flip();
	}

	public short getHead() {
		return contents.getShort(0);
	}

	public void setHead(short head) {
		contents.putShort(0, head);
	}

	public boolean isActive() throws BitWiseException {
		short head = contents.getShort(0);
		return BitWiseUtils.extractBooleanFromBits(head, 0);
	}

	public byte getType() throws BitWiseException {
		short head = contents.getShort(0);
		return BitWiseUtils.extractByteFromBits(head, 1, 4);
	}

	public byte[] getData(byte[] destination) {
		contents.get(destination, DATA_OFFSET, DATA_SIZE);
		return destination;
	}

	public void setData(byte[] data) {
		contents.put(data, DATA_OFFSET, DATA_SIZE);
	}

	public long getCategory() {
		return contents.getShort(CATEGORY_OFFSET);
	}

	public void setCategory(long category) {
		contents.putLong(CATEGORY_OFFSET, category);
	}

	public int getExtent() {
		return contents.getShort(EXTEND_OFFSET);
	}

	public void setExtent(short extent) {
		contents.putShort(EXTEND_OFFSET, extent);
	}

	public int getOffset() {
		return contents.getShort(OFFSET_OFFSET);
	}

	public void setOffset(short offset) {
		contents.putShort(OFFSET_OFFSET, offset);
	}

	public int getShortData() {
		return contents.getShort(DATA_OFFSET);
	}

	public int getIntData() {
		return contents.getInt(DATA_OFFSET);
	}

	public long getLongData() {
		return contents.getLong(DATA_OFFSET);
	}

	public double getDoubleData() {
		return contents.getDouble(DATA_OFFSET);
	}

	public String getUTF8Data() {
		contents.position(DATA_OFFSET);
		contents.mark();
		int counter = 0;
		while (contents.get() != 0x0 && contents.position() < 15) {
			counter++;
		}
		byte[] chars = new byte[counter];
		contents.get(chars, 0, counter);
		return new String(chars);
	}

	/**
	 * Write the record to a ByteBuffer. Assumes the buffer is long enough to
	 * handle the record and its reset.
	 *
	 * @param buffer Destination buffer.
	 * @return Written buffer.
	 */
	public ByteBuffer writeToBuffer(ByteBuffer buffer) {
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.put(contents);
		buffer.flip();
		return buffer;
	}
}
