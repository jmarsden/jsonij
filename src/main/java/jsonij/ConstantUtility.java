/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import static jsonij.Constants.CARRIAGE_RETURN;
import static jsonij.Constants.COMMENT;
import static jsonij.Constants.DECIMAL_POINT;
import static jsonij.Constants.DIGITS;
import static jsonij.Constants.EXPS;
import static jsonij.Constants.HEXDIGITS;
import static jsonij.Constants.MINUS;
import static jsonij.Constants.NEW_LINE;
import static jsonij.Constants.PLUS;
import static jsonij.Constants.SPACE;
import static jsonij.Constants.TAB;

public class ConstantUtility {

	public static boolean isDigit(int r) {
		return r >= DIGITS[0] && r <= DIGITS[9];
	}

	public static boolean isDigit(char c) {
		return isDigit((int) c);
	}

	public static boolean isNumeric(int r) {
		return r == PLUS || r == MINUS || r == DECIMAL_POINT || r == EXPS[0] || r == EXPS[1] || isDigit(r);
	}

	public static boolean isNumeric(char c) {
		return isNumeric((int) c);
	}

	public static boolean isHexDigit(int r) {
		return (r >= HEXDIGITS[0] && r <= HEXDIGITS[9]) || (r >= HEXDIGITS[10] && r <= HEXDIGITS[15])
				|| (r >= HEXDIGITS[16] && r <= HEXDIGITS[21]);
	}

	public static boolean isHexDigit(char c) {
		return isHexDigit((int) c);
	}

	public static boolean isWhiteSpace(int r) {
		return r == SPACE || r == TAB;
	}

	public static boolean isWhiteSpace(char c) {
		return isWhiteSpace((int) c);
	}

	public static boolean isNewLine(int r) {
		return r == NEW_LINE;
	}

	public static boolean isNewLine(char c) {
		return isNewLine((int) c);
	}

	public static boolean isReturn(int r) {
		return r == CARRIAGE_RETURN;
	}

	public static boolean isReturn(char c) {
		return isReturn((int) c);
	}

	public static boolean isValidStringChar(int r) {
		return // ( r >= 93 && r <= 1114111 ) || ( r >= 35 && r <= 91 ) || ( r == 32 ) || ( r
				// == 33 );
		r >= 32 && r != 34 && r != 92;
	}

	public static boolean isComment(int r) {
		return r == COMMENT;
	}
}
