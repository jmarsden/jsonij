/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij;

import static jsonij.Constants.*;

public enum ValueType {

	/**
	 * OBJECT used to denote JSON Object Types.
	 */
	OBJECT(BSON_DOCUMENT),
	/**
	 * ARRAY used to denote JSON Array Types.
	 */
	ARRAY(BSON_ARRAY),
	/**
	 * STRING used to denote JSON String Types.
	 */
	STRING(BSON_STRING),
	/**
	 * NUMERIC used to denote JSON Numeric Types.
	 */
	NUMERIC(TYPE_CHECK),
	/**
	 * TRUE used to denote JSON True Types.
	 */
	TRUE(BSON_TRUE),
	/**
	 * False used to denote JSON False Types.
	 */
	FALSE(BSON_FALSE),
	/**
	 * Null used to denote JSON Null Types.
	 */
	NULL(BSON_NULL),
	/**
	 * Object ID used to denote BSON ObjectId Types.
	 */
	OBJECT_ID(BSON_OBJECT_ID),
	/**
	 * JavaScript Code used to denote BSON JS Types.
	 */
	JAVA_SCRIPT_CODE(BSON_JS_CODE),
	/**
	 * JavaScript WS Code used to denote BSON JS WSTypes.
	 */
	JAVA_SCRIPT_CODE_WS(BSON_JS_CODE_WS),
	/**
	 * UTC Date Time used to denote BSON UTC Date Time Types.
	 */
	UTC_DATE_TIME(BSON_UTC_DATE_TIME),
	/**
	 * Int32 used to denote BSON 32 bit Integer.
	 */
	INT32(BSON_INT32),
	/**
	 * Int64 used to denote BSON 64 bit Integer.
	 */
	INT64(BSON_INT64),
	/**
	 * DECIMAL128 used to denote BSON 128 bit Decimal.
	 */
	DECIMAL128(BSON_DECIMAL128),
	/**
	 * Timestamp used to denote BSON Timestamp Object.
	 */
	TIMESTAMP(BSON_TIMESTAMP), 
	BINARY_DATA(BSON_BINARY_DATA), 
	UNDEFINED_1(BSON_UNDEFINED_1),
	UNDEFINED_2(BSON_UNDEFINED_2), 
	REGEX(BSON_REGEX), 
	DB_POINTER(BSON_DB_POINTER), 
	MIN_KEY(BSON_MIN_KEY),
	MAX_KEY(BSON_MAX_KEY);

	byte BSON_TYPE;

	ValueType(byte type) {
		BSON_TYPE = type;
	}

	public byte getBSONType() {
		return BSON_TYPE;
	}
}
