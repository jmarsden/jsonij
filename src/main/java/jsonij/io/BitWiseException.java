/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io;

public class BitWiseException extends Exception {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1911805772388438212L;

	public BitWiseException() {
	}

	public BitWiseException(String message) {
		super(message);
	}

	public BitWiseException(String message, Throwable cause) {
		super(message, cause);
	}

	public BitWiseException(Throwable cause) {
		super(cause);
	}
}
