/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class BufferUtils {

	public static final ByteBuffer createBuffer(int capacity) {
		ByteBuffer buffer = ByteBuffer.allocateDirect((int) capacity);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		return buffer;
	}

	public static final String bufferToString(ByteBuffer buffer, int rowCount, boolean showValue) {
		buffer.rewind();
		StringBuilder byteLineBuilder = new StringBuilder();
		StringBuilder characterLineBuilder = new StringBuilder();
		int line = 0;
		for (int i = 0; i <= buffer.limit() - rowCount; i = i + rowCount) {
			byteLineBuilder.append(FormatUtils.leftPadString("" + (line * rowCount), 4)).append(" | ");

			characterLineBuilder.setLength(0);
			for (int j = 0; j < rowCount; j++) {
				byte c = buffer.get();
				byteLineBuilder.append(BitWiseUtils.convertToHexString(c)).append(' ');
				characterLineBuilder.append((c > 32 && c < 126) ? ((char) c) : " ").append(' ');
			}
			if (showValue) {
				byteLineBuilder.append(" |  ").append(characterLineBuilder.toString()).append(" |").append('\n');
			} else {
				byteLineBuilder.append('\n');
			}
			line++;
		}
		characterLineBuilder.setLength(0);
		if (buffer.limit() % rowCount != 0) {
			byteLineBuilder.append(FormatUtils.leftPadString("" + (line * rowCount), 4)).append(" | ");

			for (int i = 0; i < buffer.limit() % rowCount; i++) {
				byte c = buffer.get();
				byteLineBuilder.append(BitWiseUtils.convertToHexString(c)).append(' ');
				characterLineBuilder.append((char) c).append(' ');
			}
			if (showValue) {
				byteLineBuilder.append(" |  ").append(characterLineBuilder.toString()).append(" |").append('\n');
			} else {
				byteLineBuilder.append('\n');
			}
		}
		return byteLineBuilder.toString();
	}

}
