/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2018 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Basic wrapper for RandomAccessFile to switch it to Little Endian.
 * 
 * @author J.W.Marsden
 */
public final class LittleEndianRandomAccessFile implements DataInput, DataOutput {

	protected byte[] buffer;
	protected RandomAccessFile randomAccessFile;

	public LittleEndianRandomAccessFile(File file, String mode) throws FileNotFoundException {
		randomAccessFile = new RandomAccessFile(file, mode);
		buffer = new byte[8];
	}

	public LittleEndianRandomAccessFile(String file, String mode) throws FileNotFoundException {
		randomAccessFile = new RandomAccessFile(file, mode);
		buffer = new byte[8];
	}

	public final void close() throws IOException {
		randomAccessFile.close();
	}

	public final FileDescriptor getFD() throws IOException {
		return randomAccessFile.getFD();
	}

	public final long getFilePointer() throws IOException {
		return randomAccessFile.getFilePointer();
	}

	public final long length() throws IOException {
		return randomAccessFile.length();
	}

	public final int read() throws IOException {
		return randomAccessFile.read();
	}

	public final int read(byte ba[]) throws IOException {
		return randomAccessFile.read(ba);
	}

	public final int read(byte ba[], int off, int len) throws IOException {
		return randomAccessFile.read(ba, off, len);
	}

	@Override
	public final boolean readBoolean() throws IOException {
		return randomAccessFile.readBoolean();
	}

	@Override
	public final byte readByte() throws IOException {
		return randomAccessFile.readByte();
	}

	@Override
	public final char readChar() throws IOException {
		randomAccessFile.readFully(buffer, 0, 2);
		return (char) ((buffer[1] & 0xff) << 8 | (buffer[0] & 0xff));
	}

	@Override
	public final double readDouble() throws IOException {
		return Double.longBitsToDouble(readLong());
	}

	@Override
	public final float readFloat() throws IOException {
		return Float.intBitsToFloat(readInt());
	}

	@Override
	public final void readFully(byte b[]) throws IOException {
		randomAccessFile.readFully(b, 0, b.length);
	}

	@Override
	public final void readFully(byte b[], int off, int len) throws IOException {
		randomAccessFile.readFully(b, off, len);
	}

	@Override
	public final int readInt() throws IOException {
		randomAccessFile.readFully(buffer, 0, 4);
		return (buffer[3]) << 24 | (buffer[2] & 0xff) << 16 | (buffer[1] & 0xff) << 8 | (buffer[0] & 0xff);
	}

	@Override
	public final String readLine() throws IOException {
		return randomAccessFile.readLine();
	}

	@Override
	public final long readLong() throws IOException {
		randomAccessFile.readFully(buffer, 0, 8);
		return (long) (buffer[7]) << 56 | (long) (buffer[6] & 0xff) << 48 | (long) (buffer[5] & 0xff) << 40
				| (long) (buffer[4] & 0xff) << 32 | (long) (buffer[3] & 0xff) << 24 | (long) (buffer[2] & 0xff) << 16
				| (long) (buffer[1] & 0xff) << 8 | (long) (buffer[0] & 0xff);
	}

	@Override
	public final short readShort() throws IOException {
		randomAccessFile.readFully(buffer, 0, 2);
		return (short) ((buffer[1] & 0xff) << 8 | (buffer[0] & 0xff));
	}

	@Override
	public final String readUTF() throws IOException {
		return randomAccessFile.readUTF();
	}

	@Override
	public final int readUnsignedByte() throws IOException {
		return randomAccessFile.readUnsignedByte();
	}

	@Override
	public final int readUnsignedShort() throws IOException {
		randomAccessFile.readFully(buffer, 0, 2);
		return ((buffer[1] & 0xff) << 8 | (buffer[0] & 0xff));
	}

	public final void seek(long pos) throws IOException {
		randomAccessFile.seek(pos);
	}

	@Override
	public final int skipBytes(int n) throws IOException {
		return randomAccessFile.skipBytes(n);
	}

	@Override
	public final synchronized void write(int b) throws IOException {
		randomAccessFile.write(b);
	}

	@Override
	public final void write(byte b[]) throws IOException {
		randomAccessFile.write(b, 0, b.length);
	}

	@Override
	public final synchronized void write(byte b[], int off, int len) throws IOException {
		randomAccessFile.write(b, off, len);
	}

	@Override
	public final void writeBoolean(boolean v) throws IOException {
		randomAccessFile.writeBoolean(v);
	}

	@Override
	public final void writeByte(int v) throws IOException {
		randomAccessFile.writeByte(v);
	}

	@Override
	public final void writeBytes(String s) throws IOException {
		randomAccessFile.writeBytes(s);
	}

	@Override
	public final void writeChar(int v) throws IOException {
		buffer[0] = (byte) v;
		buffer[1] = (byte) (v >> 8);
		randomAccessFile.write(buffer, 0, 2);
	}

	@Override
	public final void writeChars(String s) throws IOException {
		int len = s.length();
		for (int i = 0; i < len; i++) {
			writeChar(s.charAt(i));
		}
	}

	@Override
	public final void writeDouble(double v) throws IOException {
		writeLong(Double.doubleToLongBits(v));
	}

	@Override
	public final void writeFloat(float v) throws IOException {
		writeInt(Float.floatToIntBits(v));
	}

	@Override
	public final void writeInt(int v) throws IOException {
		buffer[0] = (byte) v;
		buffer[1] = (byte) (v >> 8);
		buffer[2] = (byte) (v >> 16);
		buffer[3] = (byte) (v >> 24);
		randomAccessFile.write(buffer, 0, 4);
	}

	@Override
	public final void writeLong(long v) throws IOException {
		buffer[0] = (byte) v;
		buffer[1] = (byte) (v >> 8);
		buffer[2] = (byte) (v >> 16);
		buffer[3] = (byte) (v >> 24);
		buffer[4] = (byte) (v >> 32);
		buffer[5] = (byte) (v >> 40);
		buffer[6] = (byte) (v >> 48);
		buffer[7] = (byte) (v >> 56);
		randomAccessFile.write(buffer, 0, 8);
	}

	@Override
	public final void writeShort(int v) throws IOException {
		buffer[0] = (byte) v;
		buffer[1] = (byte) (v >> 8);
		randomAccessFile.write(buffer, 0, 2);
	}

	@Override
	public final void writeUTF(String s) throws IOException {
		randomAccessFile.writeUTF(s);
	}
}
