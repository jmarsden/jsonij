/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardOpenOption.DSYNC;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileChannelDrive implements Drive {

	private final Object lock = new Object();
	private int capacity;
	private boolean connected;
	private final Path path;
	private FileChannel fileChannel;

	private final StringBuilder outputBuilder;

	public FileChannelDrive(int capacity, Path path) {
		this.capacity = capacity;
		this.path = path;
		connected = false;
		this.outputBuilder = new StringBuilder();
	}

	@Override
	public long capacity() {
		return capacity;
	}

	@Override
	public long getStartIndex() {
		return 0;
	}

	@Override
	public long getEndIndex() {
		return capacity;
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public boolean connect() {
		synchronized (lock) {
			try {
				if (!Files.exists(path)) {
					Files.createFile(path);
				}
				fileChannel = FileChannel.open(path, READ, WRITE, DSYNC);
				connected = true;
			} catch (IOException ex) {
				Logger.getLogger(FileChannelDrive.class.getName()).log(Level.SEVERE, null, ex);
				connected = false;
			}
			return connected;
		}

	}

	@Override
	public boolean disconnnect() {
		synchronized (lock) {
			try {
				fileChannel.close();
				connected = false;
			} catch (IOException ex) {
				Logger.getLogger(FileChannelDrive.class.getName()).log(Level.SEVERE, null, ex);
				connected = true;
			}
		}
		return connected;
	}

	@Override
	public boolean isPersistent() {
		return true;
	}

	@Override
	public boolean hasAllocations() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Allocation[] getAllocations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long store(long offset, byte[] buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset < 0 || start < 0 || length < 0) {
			throw new DriveException("Incorrect Args");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.length) {
			throw new DriveException("Input Overflow");
		}
		synchronized (lock) {
			try {
				if (this.fileChannel.position() != offset) {
					this.fileChannel.position((int) offset);
				}
				ByteBuffer tempBuffer = ByteBuffer.wrap(buffer);
				if (tempBuffer.position() != start) {
					tempBuffer.position((int) start);
				}
				if (tempBuffer.limit() != start + length) {
					tempBuffer.limit((int) (start + length));
				}
				this.fileChannel.write(tempBuffer, start);
				return length;
			} catch (IOException ex) {
				Logger.getLogger(FileChannelDrive.class.getName()).log(Level.SEVERE, null, ex);
				throw new DriveException(ex);
			}
		}
	}

	@Override
	public long store(long offset, ByteBuffer buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset < 0 || start < 0 || length < 0) {
			throw new DriveException("Incorrect Args");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.limit()) {
			throw new DriveException("Input Overflow");
		}
		synchronized (lock) {
			try {
				if (this.fileChannel.position() != offset) {
					this.fileChannel.position((int) offset);
				}
				if (buffer.position() != start) {
					buffer.position((int) start);
				}
				if (buffer.limit() != start + length) {
					buffer.limit((int) (start + length));
				}
				int bytes = this.fileChannel.write(buffer);
				if (bytes < length) {
					System.out.println("Under Write");
				}
				return length;
			} catch (IOException ex) {
				Logger.getLogger(FileChannelDrive.class.getName()).log(Level.SEVERE, null, ex);
				throw new DriveException(ex);
			}
		}
	}

	@Override
	public long store(long offset, SeekableByteChannel buffer, long start, long length) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public long load(long offset, byte[] buffer, long start, long length) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public long load(long offset, ByteBuffer buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.capacity()) {
			throw new DriveException("Input Overflow");
		}
		synchronized (lock) {
			try {
				ByteBuffer destination = buffer.duplicate();
				destination.position((int) start);
				destination.limit((int) (start + length));
				int bytes = fileChannel.read(destination, offset);
				if (bytes < length) {
					buffer.position(buffer.position() + (int) length);
				} else {
					buffer.position(destination.position());
				}
				return length;
			} catch (IOException ex) {
				Logger.getLogger(FileChannelDrive.class.getName()).log(Level.SEVERE, null, ex);
				throw new DriveException(ex);
			}
		}
	}

	@Override
	public long load(long offset, SeekableByteChannel buffer, long start, long length) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte readByte(long index) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int readInt(long index) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public String toString() {
		synchronized (outputBuilder) {
			outputBuilder.setLength(0);
			outputBuilder.append("FileChannelDrive(").append(Integer.toHexString(hashCode())).append(")[").append(capacity).append("]");
			return outputBuilder.toString();
		}
	}
}
