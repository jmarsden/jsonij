/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.nio.ByteBuffer;

/**
 * @author J.W.Marsden
 */
public interface Partition {

	public int getAllocationCount();

	public void appendAllocation(AllocationProvider allocation);

	public void appendAllocation(AllocationProvider allocation, long offset, long length);

	public AllocationProvider getPartitionAllocation(long partitionIndex);

	/**
	 * Accessor for the byte capacity for this Partition
	 *
	 * @return The capacity in bytes.
	 */
	public long capacity();

	public int getExtentCount();

	public Extent getExtentAtIndex(int i);

	public Extent getExtent(int id);

	public byte readByte(long index) throws PartitionException, AllocationException, DriveException;

	public int readInt32(long index) throws PartitionException, AllocationException, DriveException;

	/**
	 * Store a length of bytes into the Partition at the offset.
	 *
	 * @param offset The offset (in bytes) to store.
	 * @param buffer The source ByteBuffer to write.
	 * @param start
	 * @param length The count of bytes to store.
	 * @return The length of bytes processed.
	 * @throws PartitionException Partition Exception.
	 */
	public abstract int store(long offset, ByteBuffer buffer, long start, long length) throws PartitionException, DriveException, AllocationException;

	/**
	 * Load the buffer with a length of bytes from the Partition offset. This method
	 * assumes that the ByteBufffer is ready to write to. This method will *not*
	 * flip the ByteBuffer.
	 *
	 * @param offset The offset (in bytes) to read.
	 * @param buffer The ByteBuffer instance to write to.
	 * @param length The size of the bytes to read.
	 * @return The length of bytes processed.
	 */
	public abstract int load(long offset, ByteBuffer buffer, long length) throws PartitionException, DriveException, AllocationException;

	public abstract ByteBuffer dumpMemory() throws DriveException, AllocationException;
}
