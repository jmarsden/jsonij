/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.nio.ByteBuffer;

/**
 * @author J.W.Marsden
 */
public interface Allocation {

	/**
	 * Accessor for the byte capacity for this AllocationProvider
	 *
	 * @return The capacity in bytes.
	 */
	public abstract long capacity();

	public abstract byte readByte(long index) throws DriveException;

	/**
	 * Read and integer from the index.
	 *
	 * @param index The offset to read at.
	 * @return The read integer.
	 */
	public abstract int readInt32(long index) throws DriveException;
	//
	// /**
	// * Store the value into the AllocationProvider. This will write a single
	// * value from the buffer into the Allocation at the limit.
	// *
	// * @param buffer The source ByteBuffer to write.
	// * @return The written ByteBuffer.
	// * @throws AllocationException Exception if not enough space exists in
	// * the Allocation.
	// */
	// public abstract ByteBuffer store(ByteBuffer buffer) throws
	// AllocationException;
	//
	// /**
	// * Store a length of bytes into the AllocationProvider.
	// *
	// * @param buffer The source ByteBuffer to write.
	// * @param length The count of bytes to store.
	// * @return The written ByteBuffer.
	// * @throws AllocationException Exception if not enough space exists in
	// * the Allocation.
	// */
	// public abstract ByteBuffer store(ByteBuffer buffer, long length) throws
	// AllocationException;
	//
	// /**
	// * Store the value into the AllocationProvider. This will write a single
	// * value from the buffer into the Allocation at the offset.
	// *
	// * @param offset Offset (in bytes) to write to.
	// * @param buffer The source ByteBuffer to write.
	// * @return The written ByteBuffer.
	// * @throws AllocationException Exception if not enough space exists in
	// * the Allocation.
	// */
	// public abstract ByteBuffer store(long offset, ByteBuffer buffer) throws
	// AllocationException;

	public abstract long store(long offset, byte[] buffer, long start, long length) throws DriveException, AllocationException;

	/**
	 * Store a length of bytes into the AllocationProvider.
	 *
	 * @param offset Offset (in bytes) to write to.
	 * @param buffer The source ByteBuffer to write.
	 * @param start  The index of the first byte to write to in the ByteBuffer.
	 * @param length The count of bytes to store.
	 * @return The written ByteBuffer.
	 * @throws AllocationException Exception if not enough space exists in the
	 *                             Allocation.
	 */
	public abstract long store(long offset, ByteBuffer buffer, long start, long length) throws DriveException, AllocationException;

	// /**
	// * Load a single value at the offset into the ByteBuffer. This method
	// * assumes that the ByteBufffer is ready to write to. This method will
	// *not*
	// * flip the ByteBuffer.
	// *
	// * @param offset The offset (in bytes) to read.
	// * @param buffer The ByteBuffer instance to write to.
	// * @return The ByteBuffer written to.
	// */
	// public abstract ByteBuffer load(long offset, ByteBuffer buffer) throws
	// AllocationException;
	public abstract long load(long offset, byte[] buffer, long start, long length) throws DriveException, AllocationException;

	public abstract long load(long offset, ByteBuffer buffer, long start, long length) throws DriveException, AllocationException;

	public abstract Drive drive();
}
