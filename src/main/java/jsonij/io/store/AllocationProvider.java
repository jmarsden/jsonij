/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.nio.ByteBuffer;

public class AllocationProvider implements Allocation {

	protected long capacity;

	/**
	 * Drive Attributes
	 */
	protected Drive drive;
	protected long driveBaseOffset;

	/**
	 * Partition Attributes
	 */
	private long partitionOffset;
	private long partitionLength;

	private final StringBuilder outputBuilder;

	public AllocationProvider(Drive drive) {
		this.capacity = drive.capacity();

		this.drive = drive;
		this.driveBaseOffset = 0;

		this.outputBuilder = new StringBuilder();
	}

	public AllocationProvider(ByteBufferDrive drive, long offset, long length) {
		if (offset + length > drive.capacity()) {
			throw new RuntimeException("Allocation over Drive Capacity");
		}

		this.capacity = length;

		this.drive = drive;
		this.driveBaseOffset = offset;

		this.outputBuilder = new StringBuilder();
	}

	@Override
	public long capacity() {
		return this.capacity;
	}

	@Override
	public byte readByte(long index) throws DriveException {
		return drive.readByte(getDriveIndex(index));
	}

	@Override
	public int readInt32(long index) throws DriveException {
		return drive.readInt(getDriveIndex(index));
	}

	@Override
	public long store(long offset, byte[] buffer, long start, long length) throws DriveException, AllocationException {
		if (offset + length > driveBaseOffset + capacity) {
			throw new AllocationException("Buffer Overflow.");
		}
		return drive.store(getDriveIndex(offset), buffer, start, length);
	}

	@Override
	public long store(long offset, ByteBuffer buffer, long start, long length) throws DriveException, AllocationException {
		if (offset + length > driveBaseOffset + capacity) {
			throw new AllocationException("Buffer Overflow.");
		}
		if (buffer.limit() < start + length && buffer.capacity() >= start + length) {
			buffer.limit((int) (start + length));
		}
		return drive.store(getDriveIndex(offset), buffer, start, length);
	}

	@Override
	public long load(long offset, byte[] buffer, long start, long length) throws DriveException, AllocationException {
		if (offset + length > driveBaseOffset + capacity) {
			throw new AllocationException("Buffer Overflow.");
		}
		return drive.load(getDriveIndex(offset), buffer, start, length);
	}

	@Override
	public long load(long offset, ByteBuffer buffer, long start, long length) throws DriveException, AllocationException {
		if (offset + length > driveBaseOffset + capacity) {
			throw new AllocationException("Buffer Overflow.");
		}
		return drive.load(getDriveIndex(offset), buffer, start, length);
	}

	@Override
	public Drive drive() {
		return drive;
	}

	public long getDriveIndex(long allocationIndex) {
		return driveBaseOffset + allocationIndex;
	}

	public void setPartitionOffset(long partitionOffset) {
		this.partitionOffset = partitionOffset;
	}

	public long getPartitionOffset() {
		return partitionOffset;
	}

	public void setPartitionLength(long partitionLength) {
		this.partitionLength = partitionLength;
	}

	public long getPartitionLength() {
		return partitionLength;
	}

	public long getStartIndex() {
		return partitionOffset;
	}

	public long getEndIndex() {
		return partitionOffset + partitionLength;
	}

	public long getAllocationIndex(long partitionIndex) {
		return partitionIndex - partitionOffset;
	}

	@Override
	public String toString() {
		synchronized (outputBuilder) {
			outputBuilder.setLength(0);
			outputBuilder.append("Allocation(").append(Integer.toHexString(hashCode())).append(")").append("->(").append(drive).append(")[").append(driveBaseOffset).append("..")
					.append(driveBaseOffset + capacity).append("]");
			return outputBuilder.toString();
		}
	}
}
