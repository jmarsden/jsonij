/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.nio.ByteBuffer;

public class BasePartition implements Partition {

	long[] allocationStartIndexes;
	long[] allocationEndIndexes;
	long[] allocationLengths;
	AllocationProvider[] allocations;

	int allocationCount;
	int allocationCapacity;
	int growthFactor;

	public BasePartition() {
		this(1);
	}

	public BasePartition(int initialSize) {
		allocationStartIndexes = new long[initialSize];
		allocationEndIndexes = new long[initialSize];
		allocationLengths = new long[initialSize];
		allocations = new AllocationProvider[initialSize];
		allocationCapacity = initialSize;
		allocationCount = 0;
		growthFactor = 2;
	}

	@Override
	public int getAllocationCount() {
		return allocationCount;
	}

	private boolean hasCapacity() {
		return allocationCount < allocationCapacity;
	}

	private void grow() {
		allocationCapacity += growthFactor;
		growthFactor *= 2;
		AllocationProvider[] tempAllocations = allocations;
		allocations = new AllocationProvider[allocationCapacity];
		allocationStartIndexes = new long[allocationCapacity];
		allocationEndIndexes = new long[allocationCapacity];
		allocationLengths = new long[allocationCapacity];
		for (int i = 0; i < allocationCount; i++) {
			allocations[i] = tempAllocations[i];
			allocationStartIndexes[i] = allocations[i].getPartitionOffset();
			allocationEndIndexes[i] = allocations[i].getPartitionOffset() + allocations[i].getPartitionLength();
			allocationLengths[i] = allocations[i].getPartitionLength();
		}
	}

	@Override
	public void appendAllocation(AllocationProvider allocation) {
		if (!hasCapacity()) {
			grow();
		}

		if (allocationCount == 0) {
			appendAllocation(allocation, 0, allocation.capacity());
		} else {
			AllocationProvider previousPartitionAllocation = allocations[allocationCount - 1];
			appendAllocation(allocation,
					previousPartitionAllocation.getPartitionOffset() + previousPartitionAllocation.getPartitionLength(),
					allocation.capacity());
		}

	}

	@Override
	public void appendAllocation(AllocationProvider allocation, long offset, long length) {
		if (!hasCapacity()) {
			grow();
		}
		allocation.setPartitionOffset(offset);
		allocation.setPartitionLength(length);
		allocations[allocationCount] = allocation;
		allocationStartIndexes[allocationCount] = offset;
		allocationEndIndexes[allocationCount] = offset + length;
		allocationLengths[allocationCount] = length;
		allocationCount++;
	}

	@Override
	public long capacity() {
		long cap = 0;
		for (int i = 0; i < allocationCount; i++) {
			cap += allocations[i].getPartitionLength();
		}
		return cap;
	}

	@Override
	public AllocationProvider getPartitionAllocation(long partitionIndex) {
		for (int i = 0; i < allocationCount; i++) {
			if (partitionIndex >= allocationStartIndexes[i] && partitionIndex < allocationEndIndexes[i]) {
				return allocations[i];
			}
		}
		return null;
	}

	@Override
	public int getExtentCount() {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose
																		// Tools | Templates.
	}

	@Override
	public Extent getExtentAtIndex(int i) {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose
																		// Tools | Templates.
	}

	@Override
	public Extent getExtent(int id) {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose
																		// Tools | Templates.
	}

	@Override
	public byte readByte(long index) throws PartitionException, AllocationException, DriveException {
		AllocationProvider allocation = getPartitionAllocation(index);
		return allocation.readByte(allocation.getAllocationIndex(index));
	}

	@Override
	public int readInt32(long index) throws DriveException {
		AllocationProvider allocation = getPartitionAllocation(index);
		return allocation.readInt32(allocation.getAllocationIndex(index));
	}

	@Override
	public int store(long offset, ByteBuffer buffer, long start, long length)
			throws PartitionException, AllocationException, DriveException {
		if (offset + length > capacity()) {
			throw new PartitionException("Partition Overflow");
		}

		StringBuilder writeLog = new StringBuilder();

		long inputStartIndex = start;
		long inputEndIndex = start + length;
		long currentInputIndex = start;

		long outputStartIndex = offset;
		long outputEndIndex = offset + length;
		long currentOutputIndex = offset;

		writeLog.append("Partition[").append(outputStartIndex).append("..").append(outputEndIndex).append("]");
		writeLog.append("<-Input[").append(inputStartIndex).append("..").append(inputEndIndex).append("]").append('\n');

		int total = 0;
		for (int i = 0; i < allocationCount; i++) {
			if (currentOutputIndex < allocationStartIndexes[i] || currentOutputIndex >= allocationEndIndexes[i]) {
				continue;
			}
			AllocationProvider allocation = allocations[i];

			long allocationWriteStartIndex = currentOutputIndex - allocation.getPartitionOffset();
			long allocationWriteEndIndex = Math.min(allocationEndIndexes[i], outputEndIndex) - allocation.getPartitionOffset();

			long writeStartIndex = currentOutputIndex;
			long writeEndIndex = Math.min(allocationEndIndexes[i], outputEndIndex);
			long writeLength = writeEndIndex - writeStartIndex;

			long sourceStartIndex = currentInputIndex;
			long sourceEndIndex = currentInputIndex + writeLength;
			
			long w = allocation.store(allocationWriteStartIndex, buffer, currentInputIndex, writeLength);
			total += w;
			
			writeLog.append("[").append(writeLength).append("] of [").append(w).append("]->Allocation(")
					.append(Integer.toHexString(allocation.hashCode())).append(")[").append(allocationWriteStartIndex)
					.append("..").append(allocationWriteEndIndex).append("]");
			writeLog.append("<-Input").append("[").append(sourceStartIndex).append("..").append(sourceEndIndex)
					.append("]").append('\n');

			currentOutputIndex += w;
			currentInputIndex += w;
		}
		if(total == length) {
			writeLog.append("[").append(total).append("] complete").append('\n');
		} else {
			writeLog.append("[").append(total).append("] incomplete").append('\n');
		}
		System.out.println(writeLog.toString());
		return total;
	}

	@Override
	public int load(long offset, ByteBuffer buffer, long length) throws PartitionException {

		return 0;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Parition(").append(Integer.toHexString(hashCode())).append(")[").append(capacity()).append("]");
		builder.append('\r').append('\n');
		for (int i = 0; i < allocationCount; i++) {
			AllocationProvider allocation = allocations[i];
			builder.append("[").append(allocation.getPartitionOffset()).append("..")
					.append(allocation.getPartitionOffset() + allocation.getPartitionLength()).append("]")
					.append(allocations[i]);
			builder.append('\r').append('\n');
		}
		return builder.toString();
	}

	@Override
	public ByteBuffer dumpMemory() throws DriveException, AllocationException {
		long size = capacity();
		ByteBuffer dumpBuffer = ByteBuffer.allocate((int) size);
		long o = 0;
		for (int i = 0; i < allocationCount; i++) {
			AllocationProvider allocation = allocations[i];
			o += allocation.load(0, dumpBuffer, o, allocation.getPartitionLength());
		}
		dumpBuffer.flip();
		return dumpBuffer;
	}

}
