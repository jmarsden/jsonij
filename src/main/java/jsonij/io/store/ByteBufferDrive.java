/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SeekableByteChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ByteBufferDrive implements Drive {

	private final Object lock = new Object();
	private int capacity;
	private boolean connected;
	private ByteBuffer buffer;

	private final StringBuilder outputBuilder;

	public ByteBufferDrive(int capacity) {
		this.capacity = capacity;
		connected = false;
		this.outputBuilder = new StringBuilder();
	}

	@Override
	public long capacity() {
		return capacity;
	}

	@Override
	public long getStartIndex() {
		return 0;
	}

	@Override
	public long getEndIndex() {
		return capacity;
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public boolean connect() {
		synchronized (lock) {
			buffer = ByteBuffer.allocate((int) capacity);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			connected = true;
			return connected;
		}
	}

	@Override
	public boolean disconnnect() {
		synchronized (lock) {
			/**
			 * @todo: if its possible, force clean this up.
			 */
			buffer.clear();
			connected = false;
			return connected;
		}
	}

	@Override
	public boolean isPersistent() {
		return false;
	}

	@Override
	public boolean hasAllocations() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Allocation[] getAllocations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long store(long offset, byte[] buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset < 0 || start < 0 || length < 0) {
			throw new DriveException("Incorrect Args");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.length) {
			throw new DriveException("Input Overflow");
		}

		synchronized (lock) {
			if (this.buffer.position() != offset) {
				this.buffer.position((int) offset);
			}
			this.buffer.put(buffer, (int) start, (int) length);
			return length;
		}
	}

	@Override
	public long store(long offset, ByteBuffer buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset < 0 || start < 0 || length < 0) {
			throw new DriveException("Incorrect Args");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.limit()) {
			throw new DriveException("Input Overflow");
		}
		synchronized (lock) {
			if (this.buffer.position() != offset) {
				this.buffer.position((int) offset);
			}
			ByteBuffer dup = buffer.duplicate();
			dup.position((int) start);
			dup.limit((int) (start + length));
			this.buffer.put(dup);
			return length;
		}
	}

	@Override
	public long store(long offset, SeekableByteChannel buffer, long start, long length) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public long load(long offset, byte[] buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset < 0 || start < 0 || length < 0) {
			throw new DriveException("Incorrect Args");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.length) {
			throw new DriveException("Input Overflow");
		}
		synchronized (lock) {
			if (this.buffer.position() != offset) {
				this.buffer.position((int) offset);
			}
			this.buffer.get(buffer, (int) start, (int) length);
			return length;
		}
	}

	@Override
	public long load(long offset, ByteBuffer buffer, long start, long length) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		if (offset + length > capacity) {
			throw new DriveException("Capacity Overflow");
		}
		if (start + length > buffer.capacity()) {
			throw new DriveException("Input Overflow");
		}
		synchronized (lock) {
			ByteBuffer destination = buffer.duplicate();
			destination.position((int) start);
			destination.limit((int) (start + length));

			ByteBuffer source = this.buffer.duplicate();
			source.position((int) offset);
			source.limit((int) (offset + length));
			destination.put(source);
			buffer.position(destination.position());
			return length;
		}
	}

	@Override
	public long load(long offset, SeekableByteChannel buffer, long start, long length) throws DriveException {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte readByte(long index) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		byte byte0 = this.buffer.get((int) index);
		return byte0;
	}

	@Override
	public int readInt(long index) throws DriveException {
		if (!connected) {
			throw new DriveException("Drive Not Connected");
		}
		int indx = (int) index;
		if (buffer.order() == ByteOrder.LITTLE_ENDIAN) {
			int int0 = this.buffer.get(indx);
			int int1 = this.buffer.get(indx + 1);
			int int2 = this.buffer.get(indx + 2);
			int int3 = this.buffer.get(indx + 3);
			int int32 = (int3) << 24 | (int2 & 0xff) << 16 | (int1 & 0xff) << 8 | (int0 & 0xff);
			return int32;
		} else if (buffer.order() == ByteOrder.BIG_ENDIAN) {
			int int3 = this.buffer.get(indx);
			int int2 = this.buffer.get(indx + 1);
			int int1 = this.buffer.get(indx + 2);
			int int0 = this.buffer.get(indx + 3);
			int int32 = (int3) << 24 | (int2 & 0xff) << 16 | (int1 & 0xff) << 8 | (int0 & 0xff);
			return int32;
		}
		return -1;
	}

	@Override
	public String toString() {
		synchronized (outputBuilder) {
			outputBuilder.setLength(0);
			outputBuilder.append("ByteBufferDrive(").append(Integer.toHexString(hashCode())).append(")[").append(capacity).append("]");
			return outputBuilder.toString();
		}
	}
}
