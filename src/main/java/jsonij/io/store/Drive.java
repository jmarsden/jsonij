/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.store;

import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

/**
 * @author J.W.Marsden
 */
public interface Drive {

	public long capacity();

	public long getStartIndex();

	public long getEndIndex();

	public boolean isConnected();

	public boolean connect() throws DriveException;

	public boolean disconnnect() throws DriveException;

	public boolean isPersistent();

	public boolean hasAllocations();

	public Allocation[] getAllocations();

	public byte readByte(long index) throws DriveException;

	/**
	 * Read and integer from the index.
	 *
	 * @param index The offset to read at.
	 * @return The read integer.
	 */
	public int readInt(long index) throws DriveException;

	public long store(long offset, byte[] buffer, long start, long length) throws DriveException;

	public long store(long offset, ByteBuffer buffer, long start, long length) throws DriveException;

	public long store(long offset, SeekableByteChannel buffer, long start, long length) throws DriveException;

	public long load(long offset, byte[] buffer, long start, long length) throws DriveException;

	public long load(long offset, ByteBuffer buffer, long start, long length) throws DriveException;

	public long load(long offset, SeekableByteChannel buffer, long start, long length) throws DriveException;
}
