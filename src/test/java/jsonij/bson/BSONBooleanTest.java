/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.bson;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.junit.Test;

import jsonij.BSON;
import jsonij.BSONCodec;

public class BSONBooleanTest {

	BSONCodec codec = new BSONCodec();
	byte[] trueValue = new byte[] {0x1};
	byte[] falseValue = new byte[] {0x0};
	
	public ByteBuffer getBuffer() {
		ByteBuffer buffer = ByteBuffer.allocate(20);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		return buffer;
	}
	
	@Test
	public void testDecodeTrue() {
		ByteBuffer buffer = getBuffer();
		buffer.put(trueValue);
		buffer.rewind();
		boolean trueValue = codec.decodeBSONBoolean(buffer);
		assertEquals(true, trueValue);
	}

	@Test
	public void testDecodeFalse() {
		ByteBuffer buffer = getBuffer();
		buffer.put(falseValue);
		buffer.rewind();
		boolean falseValue = codec.decodeBSONBoolean(buffer);
		assertEquals(false, falseValue);
	}
	
	@Test
	public void testEncodeTrue() {
		BSON.True trueBSON = BSON.TRUE;

		ByteBuffer buffer = getBuffer();		
		trueBSON.toBSON(buffer);
		buffer.rewind();
		
		byte[] result = new byte[1];
		buffer.get(result);
		
		assertArrayEquals(trueValue, result);
		
		assertEquals(1, trueBSON.nestedSize());
	}

	@Test
	public void testEncodeFalse() {
		BSON.False falseBSON = BSON.FALSE;

		ByteBuffer buffer = getBuffer();		
		falseBSON.toBSON(buffer);
		buffer.rewind();
		
		byte[] result = new byte[1];
		buffer.get(result);
		
		assertArrayEquals(falseValue, result);
		
		assertEquals(1, falseBSON.nestedSize());
	}
}
