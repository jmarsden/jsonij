/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.bson;

import static jsonij.Constants.BSON_FALSE;
import static jsonij.Constants.BSON_TRUE;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import jsonij.BSON.Timestamp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jsonij.BSONCodec;
import jsonij.BSONTimestamp;
import static org.junit.Assert.*;

/**
 * @author John
 */
public class BSONCodecTest {

	public BSONCodecTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of validateBufferOrder method, of class BSONCodec.
	 */
	@Test
	public void testValidateBufferOrder() {
		System.out.println("validateBufferOrder");
		ByteBuffer buffer = ByteBuffer.allocate(1);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		BSONCodec instance = new BSONCodec();
		boolean expResult = true;
		boolean result = instance.validateBufferOrder(buffer);
		assertEquals(expResult, result);
		buffer = ByteBuffer.allocate(1);
		buffer.order(ByteOrder.BIG_ENDIAN);
		expResult = false;
		result = instance.validateBufferOrder(buffer);
		assertEquals(expResult, result);
	}

	/**
	 * Test of decodeObjectID method, of class BSONCodec.
	 */
	@Test
	public void testDecodeObjectID() {
		System.out.println("decodeObjectID");
	}

	/**
	 * Test of encodeBoolean method, of class BSONCodec.
	 */
	@Test
	public void testEncodeBoolean() {
		System.out.println("encodeBoolean");
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		boolean value = true;
		BSONCodec instance = new BSONCodec();
		ByteBuffer result = instance.encodeBSONBoolean(buffer, value);
		result.rewind();
		short resultShort = result.getShort();
		assertEquals(BSON_TRUE, resultShort);

		buffer.rewind();
		value = false;
		result = instance.encodeBSONBoolean(buffer, value);
		result.rewind();
		resultShort = result.getShort();
		assertEquals(BSON_FALSE, resultShort);
	}

	/**
	 * Test of decodeBoolean method, of class BSONCodec.
	 */
	@Test
	public void testDecodeBoolean() {
		System.out.println("decodeBoolean");
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putShort(BSON_TRUE);
		buffer.flip();
		buffer.rewind();
		BSONCodec instance = new BSONCodec();
		boolean expResult = true;
		boolean result = instance.decodeBSONBoolean(buffer);
		assertEquals(expResult, result);
		buffer.rewind();
		buffer.putShort(BSON_FALSE);
		buffer.flip();
		buffer.rewind();
		expResult = false;
		result = instance.decodeBSONBoolean(buffer);
		assertEquals(expResult, result);
	}

	@Test
	public void testDecodeInt32() {
		System.out.println("testDecodeInt32");
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putInt(16911);
		buffer.flip();
		buffer.rewind();
		BSONCodec instance = new BSONCodec();
		int intValue = instance.decodeInt32(buffer);
		assertEquals(16911, intValue);

		System.out.println("16911: " + instance.byteBufferToHexString(buffer));
	}

	@Test
	public void testEncodeInt32() {
		System.out.println("testEncodeInt32");
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		BSONCodec instance = new BSONCodec();
		instance.encodeInt32(buffer, 69);
		buffer.flip();
		buffer.rewind();
		int intValue = instance.decodeInt32(buffer);
		assertEquals(69, intValue);
	}

	@Test
	public void testEncodeTimestamp() {
		System.out.println("testEncodeTimestamp");
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.order(ByteOrder.LITTLE_ENDIAN);

		BSONTimestamp inValue = new BSONTimestamp(400, 2800);

		BSONCodec instance = new BSONCodec();
		instance.encodeTimestamp(buffer, inValue);
		buffer.flip();
		buffer.rewind();
		BSONTimestamp outValue = instance.decodeTimestamp(buffer);
		assertEquals(inValue, outValue);
	}

	/**
	 * Test of encodeUTF8String method, of class BSONCodec.
	 */
	@Test
	public void testEncodeUTF8String() {
		System.out.println("encodeUTF8String");
		ByteBuffer buffer = ByteBuffer.allocate(256);
		String value = "test string";
		BSONCodec instance = new BSONCodec();
		ByteBuffer result = instance.encodeUTF8String(buffer, value);
		result.flip();
		result.rewind();
		byte[] resultBytes = new byte[result.limit()];
		result.get(resultBytes);
		System.out.println(instance.byteArrayToHexString(resultBytes));

	}

	/**
	 * Test of decodeUTF8String method, of class BSONCodec.
	 */
	@Test
	public void testDecodeUTF8String() {
		System.out.println("decodeUTF8String");
		ByteBuffer buffer = null;
		BSONCodec instance = new BSONCodec();
		String expResult = "";
		// String result = instance.decodeUTF8String(buffer);
		// assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of encodeCString method, of class BSONCodec.
	 */
	@Test
	public void testEncodeCString() {
		System.out.println("encodeCString");
		ByteBuffer buffer = ByteBuffer.allocate(256);
		String value = "";
		BSONCodec instance = new BSONCodec();
		ByteBuffer expResult = null;
		ByteBuffer result = instance.encodeCString(buffer, value);
	}

	/**
	 * Test of decodeCString method, of class BSONCodec.
	 */
	@Test
	public void testDecodeCString() {
		System.out.println("decodeCString");
		ByteBuffer buffer = null;
		BSONCodec instance = new BSONCodec();
		// String expResult = "";
		// String result = instance.decodeCString(buffer);
		// assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

}
