/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import jsonij.JSON;
import jsonij.jpath.predicate.AllPredicate;
import jsonij.jpath.predicate.ExpressionPredicate;
import jsonij.jpath.predicate.ExpressionPredicate.ExpressionPredicateCondition;
import jsonij.jpath.predicate.ExpressionPredicate.ExpressionPredicateOperator;
import jsonij.jpath.predicate.ExpressionPredicate.OperatorExpressionPredicateCondition;
import jsonij.jpath.predicate.SimpleIndexPredicate;
import jsonij.jpath.predicate.LastIndexPredicate;
import java.io.IOException;
import jsonij.parser.ParserException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class JPathParserTest {

	public JPathParserTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testBasicPath() throws IOException, ParserException {
		System.out.println("testBasicPath");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("/test/path");

		assertEquals(2, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "test");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "path");

		path = jPathParser.parse("$.test.path");
		assertEquals(2, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "test");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "path");
	}
	
	@Test
	public void testResultPath() throws IOException, ParserException {
		System.out.println("testResultPath");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("$['test']['path']");
		assertEquals(2, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "test");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "path");
	}

	@Test
	public void testAllPath() throws IOException, ParserException {
		System.out.println("testAllPath");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("/store/book[*]/author");

		assertEquals(4, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "store");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "book");
		assertEquals(path.get(2).getClass(), AllPredicate.class);
		assertEquals(((KeyComponent) path.get(3)).getValue(), "author");

		path = jPathParser.parse("$.store.book[*].author");
		assertEquals(4, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "store");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "book");
		assertEquals(path.get(2).getClass(), AllPredicate.class);
		assertEquals(((KeyComponent) path.get(3)).getValue(), "author");
	}

	@Test
	public void testBasicSearch() throws IOException, ParserException {
		System.out.println("testBasicSearch");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("//author");
		assertEquals(2, path.size());
		assertEquals(path.get(0).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(1)).getValue(), "author");

		path = jPathParser.parse("$..author");
		assertEquals(2, path.size());
		assertEquals(path.get(0).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(1)).getValue(), "author");
	}

	@Test
	public void testAllComponent() throws IOException, ParserException {
		System.out.println("testAllComponent");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("/store/*");
		assertEquals(2, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "store");
		assertEquals(path.get(1).getClass(), AllComponent.class);

		path = jPathParser.parse("$.store.*");
		assertEquals(2, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "store");
		assertEquals(path.get(1).getClass(), AllComponent.class);
	}

	@Test
	public void testNestedSearch() throws IOException, ParserException {
		System.out.println("testNestedSearch");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("/store//price");
		assertEquals(3, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "store");
		assertEquals(path.get(1).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(2)).getValue(), "price");

		path = jPathParser.parse("$.store..price");
		assertEquals(3, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "store");
		assertEquals(path.get(1).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(2)).getValue(), "price");
	}

	@Test
	public void testSearchIndexPath() throws IOException, ParserException {
		System.out.println("testSearchIndexPath");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("//book[2]");
		assertEquals(3, path.size());
		assertEquals(path.get(0).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(1)).getValue(), "book");
		assertEquals(((SimpleIndexPredicate) path.get(2)).getIndex(), 2);

		path = jPathParser.parse("$..book[2]");
		assertEquals(3, path.size());
		assertEquals(path.get(0).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(1)).getValue(), "book");
		assertEquals(((SimpleIndexPredicate) path.get(2)).getIndex(), 2);
	}

	@Test
	public void testLastFunction() throws IOException, ParserException {
		System.out.println("testLastFunction");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("//book[last()]");
		assertEquals(3, path.size());
		assertEquals(path.get(0).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent) path.get(1)).getValue(), "book");
		assertEquals((path.get(2)).getClass(), LastIndexPredicate.class);

		path = jPathParser.parse("$..book[(@.length-1)]");
		assertEquals(3, path.size());
		assertEquals(path.get(0).getClass(), SearchComponent.class);
		assertEquals(((KeyComponent)path.get(1)).getValue(), "book");
		
	}
	
	@Test
	public void testExpressionParseBase() throws IOException, ParserException {
		System.out.println("testExpressionParseBase");
		JPathParser jPathParser = new JPathParser();
		JPath<?> path = jPathParser.parse("/resume/employment[?(@.city=\"Sydney\")]/employer");
		assertEquals(4, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "resume");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "employment");
		assertEquals(path.get(2).getClass(), ExpressionPredicate.class);
		ExpressionPredicate expression = (ExpressionPredicate) path.get(2);
		assertEquals("@.city=\"Sydney\"", expression.getExpression());
		assertEquals(((KeyComponent) path.get(3)).getValue(), "employer");
		
		path = jPathParser.parse("/resume/employment[?(@.city   = \t\"Sydney\")]/employer");
		assertEquals(4, path.size());
		assertEquals(((KeyComponent) path.get(0)).getValue(), "resume");
		assertEquals(((KeyComponent) path.get(1)).getValue(), "employment");
		assertEquals(path.get(2).getClass(), ExpressionPredicate.class);
		expression = (ExpressionPredicate) path.get(2);
		assertEquals(1, expression.getConditions().size());
		OperatorExpressionPredicateCondition condition = (OperatorExpressionPredicateCondition) expression.getConditions().get(0);
		assertEquals("city", condition.getAttribute());
		assertEquals(ExpressionPredicateOperator.EQUAL, condition.getOperator());
		assertEquals(new JSON.String("Sydney"), condition.getValue());
		assertEquals(((KeyComponent) path.get(3)).getValue(), "employer");
	}
	
	
}
