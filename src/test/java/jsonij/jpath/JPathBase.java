/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.io.IOException;
import jsonij.Value;
import jsonij.parser.JSONParser;
import jsonij.parser.ParserException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class JPathBase {

	
	public static final String STORE_SAMPLE =
	        "{ \"store\": {\r\n"
	        + "    \"book\": [ \r\n"
	        + "      { \"category\": \"reference\",\r\n"
	        + "        \"author\": \"Nigel Rees\",\r\n"
	        + "        \"title\": \"Sayings of the Century\",\r\n"
	        + "        \"price\": 8.95\r\n"
	        + "      },\r\n"
	        + "      { \"category\": \"fiction\",\r\n"
	        + "        \"author\": \"Evelyn Waugh\",\r\n"
	        + "        \"title\": \"Sword of Honour\",\r\n"
	        + "        \"price\": 12.99\r\n"
	        + "      },\r\n"
	        + "      { \"category\": \"fiction\",\r\n"
	        + "        \"author\": \"Herman Melville\",\r\n"
	        + "        \"title\": \"Moby Dick\",\r\n"
	        + "        \"isbn\": \"0-553-21311-3\",\r\n"
	        + "        \"price\": 8.99,\r\n"
	        + "        \"sam\": \"Awesome\"\r\n"
	        + "      },\r\n"
	        + "      { \"category\": \"fiction\",\r\n"
	        + "        \"author\": \"J. R. R. Tolkien\",\r\n"
	        + "        \"title\": \"The Lord of the Rings\",\r\n"
	        + "        \"isbn\": \"0-395-19395-8\",\r\n"
	        + "        \"price\": 22.99\r\n"
	        + "      }\r\n"
	        + "    ],\r\n"
	        + "    \"bicycle\": {\r\n"
	        + "      \"color\": \"red\",\r\n"
	        + "      \"price\": 19.95\r\n"
	        + "    }\r\n"
	        + "  }\r\n"
	        + "}";
	public static final String RESUME_SAMPLE = "{\r"
	        + "	\"resume\": \r"
	        + "	{\r"
	        + "		\"date of birth\": \r"
	        + "		{\r"
	        + "			\"day\": 8, \r"
	        + "			\"month\": 5, \r"
	        + "			\"year\": 1981\r"
	        + "		},\r"
	        + "		\"education\": \r"
	        + "		[\r"
	        + "			{\r"
	        + "				\"institution\": \"The University of Newcastle\",\r"
	        + "				\"qualification\": \"B.Eng(Software)\",\r"
	        + "				\"begin year\": 2000,\r"
	        + "				\"completed\": true,\r"
	        + "				\"complete year\": 2003\r"
	        + "			},\r"
	        + "			{\r"
	        + "				\"institution\": \"The University of Newcastle\",\r"
	        + "				\"qualification\": \"M.Phil(Software Engineering)\",\r"
	        + "				\"begin year\": 2010,\r"
	        + "				\"completed\": false,\r"
	        + "				\"complete year\": null\r"
	        + "			}\r"
	        + "		],\r"
	        + "		\"employment\": \r"
	        + "		[\r"
	        + "			{\r"
	        + "				\"employer\": \"Matrikon\",\r"
	        + "				\"city\": \"Newcastle\",\r"
	        + "				\"start date\":\r"
	        + "				{\r"
	        + "					\"day\": 1, \r"
	        + "					\"month\": 8, \r"
	        + "					\"year\": 2003\r"
	        + "				},\r"
	        + "				\"finish date\":\r"
	        + "				{\r"
	        + "					\"day\": 20, \r"
	        + "					\"month\": 9, \r"
	        + "					\"year\": 2007\r"
	        + "				}\r"
	        + "			},\r"
	        + "			{\r"
	        + "				\"employer\": \"IBM\",\r"
	        + "				\"city\": \"Sydney\",\r"
	        + "				\"start date\":\r"
	        + "				{\r"
	        + "					\"day\": 21, \r"
	        + "					\"month\": 9, \r"
	        + "					\"year\": 2007\r"
	        + "				},\r"
	        + "				\"finish date\":\r"
	        + "				{\r"
	        + "					\"day\": 25, \r"
	        + "					\"month\": 10, \r"
	        + "					\"year\": 2009\r"
	        + "				}\r"
	        + "			},\r"
	        + "			{\r"
	        + "				\"employer\": \"The University of Newcastle\",\r"
	        + "				\"city\": \"Newcastle\",\r"
	        + "				\"start date\":\r"
	        + "				{\r"
	        + "					\"day\": 26, \r"
	        + "					\"month\": 10, \r"
	        + "					\"year\": 2009\r"
	        + "				}\r"
	        + "			},\r"
	        + "			{\r"
	        + "				\"employer\": \"Bureau Veritas\",\r"
	        + "				\"city\": \"Adelaide\",\r"
	        + "				\"start date\":\r"
	        + "				{\r"
	        + "					\"day\": 1, \r"
	        + "					\"month\": 12, \r"
	        + "					\"year\": 2010\r"
	        + "				}\r"
	        + "			}\r"
	        + "		],\r"
	        + "		\"interests\":\r"
	        + "		[\r"
	        + "			\"reading\",\r"
	        + "			\"fitness\",\r"
	        + "			\"programming\"\r"
	        + "		],\r"
	        + "		\"misc\\/other\":\r"
	        + "		[\r"
	        + "			\"Matrikon Asia-Pacific Heart Award 2005.\"\r"
	        + "		]\r"
	        + "	}\r"
	        + "}";
	public static final String METER_SAMPLE = "{"
	        + "\"enrollments\":["
	        + "    {\"key\":\"Carbon\",\"start\":\"2010-01-01\",\"end\":\"2011-01-01\","
	        + "     \"channels\":["
	        + "    {\"channelid\":\"CH-0\",\"start\":\"2010-01-01\",\"end\":\"2011-01-01\",\"mingranularity\":\"MIN15\","
	        + "     \"metrics\":{\"carbon\":{\"categories\":[{\"key\":\"Carbon\",\"fixed\":null}]}}}"
	        + "    ]},"
	        + "    {\"key\":\"TOU Daily\",\"start\":\"2010-01-01\",\"end\":\"2011-01-01\","
	        + "     \"channels\":[{\"channelid\":\"CH-0\",\"start\":\"2010-01-01\",\"end\":\"2011-01-01\",\"mingranularity\":\"MIN15\","
	        + "          \"metrics\":{"
	        + "              \"cost\":{"
	        + "              \"categories\":[{\"key\":\"high\",\"fixed\":null},"
	        + "                    {\"key\":\"low\",\"fixed\":null},"
	        + "                    {\"key\":\"medium\",\"fixed\":null},"
	        + "                    {\"key\":\"other\",\"fixed\":null}]},"
	        + "              \"usage\":{\"categories\":[{\"key\":\"high\",\"fixed\":null},{\"key\":\"low@\",\"fixed\":null},{\"key\":\"medium\",\"fixed\":null}]},"
	        + "              \"demand\":{\"categories\":[{\"key\":\"demand\",\"fixed\":null}]}}}]}]}"
	        + "}";
	public static final String MC_EXAMPLE = "{"
	        + "    \"settings\": {"
	        + "        \"java exe\": \"java\","
	        + "        \"max\": \"512M\","
	        + "        \"min\": \"512M\","
	        + "        \"server file\": \"minecraft_server.jar\","
	        + "        \"command2\" : \"java -Xmx1024M -Xms1024M -jar minecraft_server.jar nogui\""
	        + "    }"
	        + "}";

	public JPathBase() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testGetMethod() throws IOException, ParserException {
		StringBuilder randomDoubleArray = new StringBuilder();
		randomDoubleArray.append("[" + Math.random());
		for (int i = 0; i < 10000; i++) {
			randomDoubleArray.append(",\t" + i);
		}
		randomDoubleArray.append("]");

		String jsonString =
		        "{ \"store\": {\r\n"
		        + "    \"book\": [ \r\n"
		        + "      { \"category\": \"reference\",\r\n"
		        + "        \"author\": \"Nigel Rees\",\r\n"
		        + "        \"title\": \"Sayings of the Century\",\r\n"
		        + "        \"price\": 8.95\r\n"
		        + "      },\r\n"
		        + "      { \"category\": \"fiction\",\r\n"
		        + "        \"author\": \"Evelyn Waugh\",\r\n"
		        + "        \"title\": \"Sword of Honour\",\r\n"
		        + "        \"price\": 12.99\r\n"
		        + "      },\r\n"
		        + "      { \"category\": \"fiction\",\r\n"
		        + "        \"author\": \"Herman Melville\",\r\n"
		        + "        \"title\": \"Moby Dick\",\r\n"
		        + "        \"isbn\": \"0-553-21311-3\",\r\n"
		        + "        \"price\": 8.99,\r\n"
		        + "        \"sam\": \"Awesome\"\r\n"
		        + "      },\r\n"
		        + "      { \"category\": \"fiction\",\r\n"
		        + "        \"author\": \"J. R. R. Tolkien\",\r\n"
		        + "        \"title\": \"The Lord of the Rings\",\r\n"
		        + "        \"isbn\": \"0-395-19395-8\",\r\n"
		        + "        \"price\": 22.99\r\n"
		        + "      }\r\n"
		        + "    ],\r\n"
		        + "    \"bicycle\": {\r\n"
		        + "      \"color\": \"red\",\r\n"
		        + "      \"price\": 19.95\r\n"
		        + "    },\r\n"
		        + "    \"number\": " + randomDoubleArray.toString() + ""
		        + "  }\r\n"
		        + "}";
		JSONParser jsonParser = new JSONParser();
		Value json = jsonParser.parse(jsonString);

		System.out.println(json.getValueType() + "(" + json.nestedSize() + "): " + json.toJSON());

		JPathParser jPathParser = new JPathParser();
		JPath<?> path1 = jPathParser.parse("/store");
		path1.setRecordEvaluateTime(true);
		Value value = path1.evaluate(json);
		System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in "
				+ path1.getLastEvaluateTime() + " ms)");

		JPath<?> path2 = jPathParser.parse("/store/book[-2]");
		path2.setRecordEvaluateTime(true);
		value = path2.evaluate(json);
		System.out.println(value.getValueType() + "--> (" + value.nestedSize() + "): " + value.toJSON() + " (in "
				+ path2.getLastEvaluateTime() + " ms)");

		JPath<?> path3 = jPathParser.parse("/store/book[2]/title");
		path3.setRecordEvaluateTime(true);
		value = path3.evaluate(json);
		System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in "
				+ path3.getLastEvaluateTime() + " ms)");

		JPath<?> path4 = jPathParser.parse("/store/book[$]/title");
		path4.setRecordEvaluateTime(true);
		value = path4.evaluate(json);
		System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in "
				+ path4.getLastEvaluateTime() + " ms)");

		JPath<?> path5 = jPathParser.parse("/store/book[last()-3]/title");
		path5.setRecordEvaluateTime(true);
		value = path5.evaluate(json);
		System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in "
				+ path5.getLastEvaluateTime() + " ms)");

		JPath<?> path6 = jPathParser.parse("/store/book[last()-1337]/title");
		try {
			value = path6.evaluate(json);
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		JPath<?> path7 = JPath.parse("/store/number[$-1337]");
		path7.setRecordEvaluateTime(true);
		value = path7.evaluate(json);
		System.out.println(value.getValueType() + "(" + value.nestedSize() + "): " + value.toJSON() + " (in "
				+ path7.getLastEvaluateTime() + " ms)");
	}

	@Test
	public void testExpression0() throws IOException, ParserException {
		System.out.println("Test Expression0 - Single Expression Query - String and Double equals");
		JSONParser jsonParser = new JSONParser(STORE_SAMPLE);
		Value json = jsonParser.parse();

		JPathParser jPathParser = new JPathParser();
		JPath<?> path1 = jPathParser.parse("/store");
		path1.setRecordEvaluateTime(true);
		Value value1 = path1.evaluate(json);
		System.out.println(value1.getValueType() + "(" + value1.nestedSize() + "): " + value1.toJSON() + " (in "
				+ path1.getLastEvaluateTime() + " ms)");

		JPath<?> path2 = jPathParser.parse("/store/book[?(@.title=\"Sword of Honour\")]/author");
		path2.setRecordEvaluateTime(true);
		Value value2 = path2.evaluate(json);
		System.out.println(value2.getValueType() + "(" + value2.nestedSize() + "): " + value2.toJSON() + " (in "
				+ path2.getLastEvaluateTime() + " ms)");

		JPath<?> path3 = jPathParser.parse("/book[?(@.author=\"J. R. R. Tolkien\")]/title");
		path3.setRecordEvaluateTime(true);
		Value value3 = path3.evaluate(value1);
		System.out.println(value3.getValueType() + "(" + value3.nestedSize() + "): " + value3.toJSON() + " (in "
				+ path3.getLastEvaluateTime() + " ms)");

		JPath<?> path4 = jPathParser.parse("/store/book[?(@.price=8.99)]/title");
		path4.setRecordEvaluateTime(true);
		Value value4 = path4.evaluate(json);
		System.out.println(value4.getValueType() + "(" + value4.nestedSize() + "): " + value4.toJSON() + " (in "
				+ path4.getLastEvaluateTime() + " ms)");
	}

	@Test
	public void testExpression1() throws IOException, ParserException {
		System.out.println("Test Expression1 - Single Expression Query String");
		JSONParser jsonParser = new JSONParser(RESUME_SAMPLE);
		Value json = jsonParser.parse();

		JPathParser jPathParser = new JPathParser();
		JPath<?> path1 = jPathParser.parse("/resume/misc\\/other[0]");
		path1.setRecordEvaluateTime(true);
		Value value1 = path1.evaluate(json);
		System.out.println(value1.getValueType() + "(" + value1.nestedSize() + "): " + value1.toJSON() + " (in "
				+ path1.getLastEvaluateTime() + " ms)");

		assertEquals("Matrikon Asia-Pacific Heart Award 2005.", value1.getString());

		JPath<?> path2 = jPathParser.parse("/resume/employment[?(@.city=\"Sydney\")]/employer");
		path2.setRecordEvaluateTime(true);
		Value value2 = path2.evaluate(json);
		System.out.println(value1.getValueType() + "(" + value2.nestedSize() + "): " + value2.toJSON() + " (in "
				+ path2.getLastEvaluateTime() + " ms)");

		assertEquals("IBM", value2.getString());
	}

	@Test
	public void testExpression2() throws IOException, ParserException {
		System.out.println("Test Expression2 - Multiple and Expression Query");
		JSONParser jsonParser = new JSONParser(RESUME_SAMPLE);
		Value json = jsonParser.parse();

		JPathParser jPathParser = new JPathParser();

		JPath<?> path1 = jPathParser.parse(
				"/resume/employment[?(@.city=\"Newcastle\" && @.start date={\"day\":1,\"month\":8,\"year\":2003})]/employer");
		path1.setRecordEvaluateTime(true);
		Value value1 = path1.evaluate(json);
		System.out.println(value1.getValueType() + "(" + value1.nestedSize() + "): " + value1.toJSON() + " (in "
				+ path1.getLastEvaluateTime() + " ms)");

		assertEquals("Matrikon", value1.getString());
	}

	@Test
	public void testExpression3() throws IOException, ParserException {
		System.out.println("Test Expression3 - Memory Test Overflow");
		String startDate1 = "2010-01-01";
		String endDate1 = "2011-01-01";
		String startDate2 = "2010-01-01";
		JSONParser jsonParser = new JSONParser(METER_SAMPLE);
		Value json = jsonParser.parse();
		assertEquals(startDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/start").evaluate(json).getString());
		assertEquals(endDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/end").evaluate(json).getString());
		try {
			JPath.parse("/enrollments[?(@.key=\"Carbon\"]/channels[?(@.channelid=\"CH-0\"]/start").evaluate(json)
					.getString();
		} catch (JPathParserException e) {
			System.out.println("Error missing ')'" + e.toString());
		}
	}

	@Test
	public void testExpression4() throws IOException, ParserException {
		System.out.println("Test Expression4 - Working Query");
		String startDate1 = "2010-01-01";
		String endDate1 = "2011-01-01";
		String startDate2 = "2010-01-01";
		JSONParser jsonParser = new JSONParser(METER_SAMPLE);
		Value json = jsonParser.parse();
		assertEquals(startDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/start").evaluate(json).getString());
		assertEquals(endDate1, JPath.parse("/enrollments[?(@.key=\"Carbon\")]/end").evaluate(json).getString());
		assertEquals(startDate2,
				JPath.parse("/enrollments[?(@.key=\"Carbon\")]/channels[?(@.channelid=\"CH-0\")]/start").evaluate(json)
						.getString());
	}

	@Test
	public void testExpression5() throws IOException, ParserException {
		System.out.println("Test Expression5 - Working Query");
		JSONParser jsonParser = new JSONParser(MC_EXAMPLE);
		Value json = jsonParser.parse();
		Value settingsValue = JPath.parse("/settings").evaluate(json);
		System.out.println("Settings:" + settingsValue);
		assertEquals("java", JPath.parse("/settings/java exe").evaluate(json).getString());
	}

	@Test
	public void testExpression6() throws IOException, ParserException {
		System.out.println("Test testExpression6 - Union Query");
		JSONParser jsonParser = new JSONParser(STORE_SAMPLE);
		Value json = jsonParser.parse();
		Value[] values = JPath.parse("/store/book[2,3,0,-1]").evaluateAll(json);
		System.out.println("ResultSize:" + values.length);
		for (int i = 0; i < values.length; i++) {
			System.out.println("[" + i + "]" + values[i].toJSON());
		}
	}

	@Test
	public void testExpression7() throws Exception {
		System.out.println("testExpression7");
		JSONParser jsonParser = new JSONParser(STORE_SAMPLE);
		Value json = jsonParser.parse();

		JPathParser jPath = new JPathParser();
		Value[] values = JPath.parse("/store/bicycle[?(regex(@.color,\"^re.*$\"))]").evaluateAll(json);
		System.out.println("ResultSize:" + values.length);
		for (int i = 0; i < values.length; i++) {
			System.out.println("[" + i + "]" + values[i].toJSON());
		}
	}

	@Test
	public void testExpression8() throws Exception {
		System.out.println("testExpression8");
		JSONParser jsonParser = new JSONParser(STORE_SAMPLE);
		Value json = jsonParser.parse();
		Value[] values = JPath.parse("/store/book[?(regex(@.title,\"^.*Lord.*$\"))]").evaluateAll(json);
		System.out.println("ResultSize:" + values.length);
		for (int i = 0; i < values.length; i++) {
			System.out.println("[" + i + "]" + values[i].toJSON());
		}
	}

	@Test
	public void testExpression9() throws Exception {
		System.out.println("testExpression9");
		JSONParser jsonParser = new JSONParser(STORE_SAMPLE);
		Value json = jsonParser.parse();
		Value[] values = JPath.parse("/store/book[?(regex(@.title,\"^.*of.*$\"))]").evaluateAll(json);
		System.out.println("ResultSize:" + values.length);
		for (int i = 0; i < values.length; i++) {
			System.out.println("[" + i + "]" + values[i].toJSON());
		}
	}

	@Test
	public void testExpression10() throws Exception {
		System.out.println("testExpression10");
		JSONParser jsonParser = new JSONParser(STORE_SAMPLE);
		Value json = jsonParser.parse();
		Value[] values = JPath.parse("/store/bicycle[?(regex(@.color,\"^blue.*$\"))]").evaluateAll(json);
		System.out.println("ResultSize:" + values.length);
		for (int i = 0; i < values.length; i++) {
			System.out.println("[" + i + "]" + values[i].toJSON());
		}
	}

	@Test
	public void testExpression11() throws IOException, ParserException {
		System.out.println("Test Expression11 - Array Start Query");
		JSONParser jsonParser = new JSONParser("[{\"blah\":1},{\"blah\":2},{\"blah\":3},{\"blah\":4}]");
		Value json = jsonParser.parse();
		Value value = JPath.parse("/[2]/blah").evaluate(json);
		System.out.println("Value:" + value);
		assertEquals(value.toString(), "3");
	}

	@Test
	public void testExpression12() throws IOException, ParserException {
		System.out.println("Test Expression12 - Array Start Query2");
		JSONParser jsonParser = new JSONParser("   [\r\n" + "     {\r\n" + "       \"category\": \"reference\",\r\n"
				+ "         \"author\": \"Nigel Rees\",\r\n" + "          \"title\": \"Sayings of the Century\",\r\n"
				+ "          \"price\": 8.95\r\n" + "     }\r\n" + "   ]");
		Value json = jsonParser.parse();
		Value value = JPath.parse("/[0]/[?(@.author=\"Nigel Rees\")]/author").evaluate(json);
		System.out.println("Value:" + value);
		assertEquals(value.toString(), "Nigel Rees");
	}
}
