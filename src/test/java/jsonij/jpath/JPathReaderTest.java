/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class JPathReaderTest {

    public JPathReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getJPath method, of class JPathReader.
     */
    @Test
    public void testGetJPath() {
        System.out.println("getJPath");
        JPathReader instance = new JPathReader("TestString");
        String expResult = "TestString";
        String result = instance.getJPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of readNext method, of class JPathReader.
     */
    @Test
    public void testReadNext() throws Exception {
        System.out.println("readNext");
        JPathReader instance = new JPathReader("TEST");
        int expResult = 'T';
        int result = instance.readNext();
        assertEquals(expResult, result);
        expResult = 'E';
        result = instance.readNext();
        assertEquals(expResult, result);
        expResult = 'S';
        result = instance.readNext();
        assertEquals(expResult, result);
        expResult = 'T';
        result = instance.readNext();
        assertEquals(expResult, result);
        expResult = -1;
        result = instance.readNext();
        assertEquals(expResult, result);
    }

    /**
     * Test of skipWhitepace method, of class JPathReader.
     */
    @Test
    public void testSkipWhitepace() throws Exception {
        System.out.println("skipWhitepace");
        JPathReader instance = new JPathReader("T \t \t S");
        char expResult = 'T';
        char result = (char) instance.read();
        assertEquals(expResult, result);
        instance.skipWhitepace();
        expResult = 'S';
        result = (char) instance.read();
        assertEquals(expResult, result);
    }
}
