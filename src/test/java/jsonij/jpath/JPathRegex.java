/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.jpath;

import java.io.IOException;
import jsonij.JSON;
import jsonij.Value;
import jsonij.parser.JSONParser;
import jsonij.parser.ParserException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author JohnMARSDEN
 */
public class JPathRegex {
    
    public JPathRegex() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testNestedArrayRegex() throws ParserException, IOException {
        String jsonString = "{ \n"
                + "\"address_components\" : [\n"
                + "     {\n"
                + "          \"long_name\" : \"Hollywood\", \n"
                + "          \"short_name\" : \"Hollywood\",\n"
                + "          \"types\" : [ \"sublocality\", \"political\" ]\n"
                + "     },\n"
                + "     {\n"
                + "          \"long_name\" : \"Los Angeles\",\n"
                + "          \"short_name\" : \"Los Angeles\",\n"
                + "          \"types\" : [ \"locality\", \"political\" ]\n"
                + "      },\n"
                + "      {\n"
                + "           \"long_name\" : \"Los Angeles\",\n"
                + "           \"short_name\" : \"Los Angeles\",\n"
                + "           \"types\" : [ \"administrative_area_level_2\", \"political\" ]\n"
                + "       },\n"
                + "       {\n"
                + "           \"long_name\" : \"California\",\n"
                + "           \"short_name\" : \"CA\",\n"
                + "           \"types\" : [ \"administrative_area_level_1\", \"political\" ]\n"
                + "       },\n"
                + "       {\n"
                + "           \"long_name\" : \"United States\",\n"
                + "           \"short_name\" : \"US\",\n"
                + "           \"types\" : [ \"country\", \"political\" ]\n"
                + "       }\n"
                + "]}\n";
        
        JSONParser jsonParser = new JSONParser(jsonString);
        Value value = jsonParser.parse();
        
        String regex1 = "\"^.*administrative_area_level_2.*$\"";
        System.out.println("Regex1:" + regex1);
        Value[] values = JPath.parse("/address_components[?(regex(@.types," + regex1 + "))]").evaluateAll(value);
        System.out.println("\tResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("\t[" + i + "]" + values[i].toJSON());
        }
        
        String regex2 = "\"^.*political.*$\"";
        System.out.println("Regex2:" + regex2);
        values = JPath.parse("/address_components[?(regex(@.types," + regex2 + "))]").evaluateAll(value);
        System.out.println("\tResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("\t[" + i + "]" + values[i].toJSON());
        }
    }
}
