/*
 * Copyright (c) 2010-2019 J.W.Marsden
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io.storage;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;

import jsonij.io.BufferUtils;
import jsonij.io.store.Allocation;
import jsonij.io.store.AllocationException;
import jsonij.io.store.AllocationProvider;
import jsonij.io.store.BasePartition;
import jsonij.io.store.ByteBufferDrive;
import jsonij.io.store.DriveException;
import jsonij.io.store.FileChannelDrive;
import jsonij.io.store.Partition;
import jsonij.io.store.PartitionException;

public class TestBootstrap {

    public static void main(String[] args) throws PartitionException, AllocationException, DriveException, IOException {
	ByteBufferDrive drive1 = new ByteBufferDrive(18);
	drive1.connect();
	ByteBufferDrive drive2 = new ByteBufferDrive(12);
	drive2.connect();
	ByteBufferDrive drive3 = new ByteBufferDrive(21);
	drive3.connect();

	File temp = new File(System.getProperty("java.io.tmpdir"));
	String fileName = "bs.bf";
	File file = new File(temp, fileName);
	Path path = file.toPath();
	FileChannelDrive drive4 = new FileChannelDrive(38, path);
	drive4.connect();
	
	ByteBufferDrive drive5 = new ByteBufferDrive(11);
	drive5.connect();
	
	AllocationProvider allocation1 = new AllocationProvider(drive1, 0, 10);
	AllocationProvider allocation2 = new AllocationProvider(drive2);
	AllocationProvider allocation3 = new AllocationProvider(drive1, 10, 8);
	AllocationProvider allocation4 = new AllocationProvider(drive3);
	AllocationProvider allocation5 = new AllocationProvider(drive4);
	AllocationProvider allocation6 = new AllocationProvider(drive5);

	Partition partition1 = new BasePartition();
	partition1.appendAllocation(allocation1);
	partition1.appendAllocation(allocation2);
	partition1.appendAllocation(allocation3);
	partition1.appendAllocation(allocation4);
	partition1.appendAllocation(allocation5);
	partition1.appendAllocation(allocation6);
	
	System.out.println(partition1.toString());

	ByteBuffer testBuffer = ByteBuffer.allocateDirect(15);
	testBuffer.put(new byte[]{'1', '2', '3', '4', '5', '6', 'a', 'b', 'c', 'd'});
	testBuffer.flip();
	for(int i=0;i<10;i++) {
	    partition1.store(i*10, testBuffer, 0, 10);
	}
	testBuffer.rewind();
	testBuffer.put(new byte[]{'!', '@', '#', '$', '%', '^', '&', '*', '(', ')'});
	testBuffer.flip();
	partition1.store(5, testBuffer, 0, 10);	
	partition1.store(25, testBuffer, 0, 10);
	partition1.store(45, testBuffer, 0, 10);
	partition1.store(80, testBuffer, 0, 10);

	ByteBuffer dump = partition1.dumpMemory();

	System.out.println("" + BufferUtils.bufferToString(dump, 10 , true));

	AllocationProvider partitionAllocation = partition1.getPartitionAllocation(18);
	System.out.println(partitionAllocation + " " + partitionAllocation.getAllocationIndex(18) + " -> " + (char) partition1.readByte(18));
	
	drive4.disconnnect();
	if(Files.exists(path)) {
	    Files.delete(path);
	}
		
    }
}
