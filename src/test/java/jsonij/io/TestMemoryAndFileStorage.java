/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestMemoryAndFileStorage {

	private static final Logger LOGGER = Logger.getLogger(TestMemoryAndFileStorage.class.getCanonicalName());
	public static short value;

	public static void main(String[] args) throws FileNotFoundException, IOException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {

		long size = 100000000;

		LOGGER.info(String.format("Testing Size %s", size));

		testMemoryStoreAndRetrieve(size);

		size = 1000000000L;

		LOGGER.info(String.format("Testing Size %s", size));

		testRandomAccessFileStoreAndRetrieve(size);

		testFileChannelStoreAndRetrieve(size);
	}

	private static void testMemoryStoreAndRetrieve(long size) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		long start = System.nanoTime(), end;

		ByteBuffer indexBuffer = ByteBuffer.allocateDirect((int) size);
		indexBuffer.order(ByteOrder.LITTLE_ENDIAN);

		short v;

		LOGGER.info("Allocating Buffer");

		for (int i = 0; i < (size / 2) - 1; i++) {
			v = (short) (1000 * Math.random());
			indexBuffer.putShort(v);
		}
		indexBuffer.putShort((short) 999);
		indexBuffer.flip();

		end = System.nanoTime();

		LOGGER.info("Checking Last Index");
		v = indexBuffer.getShort((int) (size - 2));
		LOGGER.log(Level.INFO, "[{0}]:{1}", new Object[] { size - 2, v });

		LOGGER.info(String.format("Allocation Took %sms", (end - start) / 1000000));

		List<Long> times = new ArrayList<Long>(1000);
		for (int i = 0; i < 1000; i++) {
			int p = (int) (Math.random() * (size - 2));
			if (p % 2 != 0) {
				p += 1;
			}

			start = System.nanoTime();
			indexBuffer.position(p);
			v = indexBuffer.getShort();
			end = System.nanoTime();
			value = v;
			times.add((end - start));
		}

		double total = 0;
		for (Long t : times) {
			total += t;
		}
		LOGGER.info(String.format("Average Retrieval Time %sms", (total / 1000) / 1000000));

		indexBuffer.clear();
	}

	private static void testRandomAccessFileStoreAndRetrieve(long size)
			throws FileNotFoundException, IOException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		File temp = new File(System.getProperty("java.io.tmpdir"));

		String fileName = "raf.bf";
		File file = new File(temp, fileName);

		LOGGER.info(String.format("File: %s ", file));
		if (file.exists()) {
			file.delete();
		}

		long start = System.nanoTime(), end;

		int blocksize = 1000;

		ByteBuffer indexBuffer = ByteBuffer.allocateDirect(blocksize);
		indexBuffer.order(ByteOrder.LITTLE_ENDIAN);

		short v;

		LittleEndianRandomAccessFile raf = new LittleEndianRandomAccessFile(file, "rw");

		int transferSize = 1024;
		int transferCount = 0;
		byte[] chunk = new byte[transferSize];

		double fullCycles = size / blocksize;
		double overflow = size % blocksize;

		LOGGER.info(String.format("Size: %s BlockSize: %s", size, blocksize));
		LOGGER.info(String.format("Full Cycles: %s Overflow: %s", fullCycles, overflow));

		long transferCounter = 0;

		for (int i = 0; i < fullCycles; i++) {
			indexBuffer.rewind();
			for (int j = 0; j < (blocksize / 2); j++) {
				v = (short) (1000 * Math.random());
				indexBuffer.putShort(v);
			}
			indexBuffer.flip();

			while (indexBuffer.position() < indexBuffer.limit()) {
				transferCount = Math.min(transferSize, indexBuffer.limit() - indexBuffer.position());
				indexBuffer.get(chunk, 0, transferCount);
				raf.write(chunk, 0, transferCount);
				transferCounter += transferCount;
			}
		}

		indexBuffer.rewind();
		for (int i = 0; i < (overflow / 2); i++) {
			v = (short) (1000 * Math.random());
			indexBuffer.putShort(v);
		}
		indexBuffer.flip();

		while (indexBuffer.position() < indexBuffer.limit()) {
			transferCount = Math.min(transferSize, indexBuffer.limit() - indexBuffer.position());
			indexBuffer.get(chunk, 0, transferCount);
			raf.write(chunk, 0, transferCount);
			transferCounter += transferCount;
		}

		// indexBuffer.putShort((short) 999);
		end = System.nanoTime();

		indexBuffer.clear();

		LOGGER.info(String.format("Allocation Took %sms", (end - start) / 1000000));
		LOGGER.info(String.format("Transfer Counter %s", transferCounter));
		int samples = 1000;
		double timeSum = 0;
		for (int i = 0; i < samples; i++) {
			int p = (int) Math.min(0, (Math.random() * (size - 2)));
			if (p % 2 != 0) {
				p += 1;
			}

			// System.out.println("[" + p + "]");
			start = System.nanoTime();
			raf.seek(p);
			v = raf.readShort();
			// System.out.println("[" + p + "]:" + v);

			end = System.nanoTime();
			value = v;
			timeSum += (end - start);
		}

		LOGGER.info("Checking Last Index");
		raf.seek(size - 2);
		v = raf.readShort();
		LOGGER.log(Level.INFO, "[{0}]:{1}", new Object[] { size - 2, v });

		LOGGER.info(String.format("Average Retrieval Time %sms", (timeSum / samples / 1000000)));

		raf.close();

		if (file.exists()) {
			file.delete();
		}
	}

	private static void testFileChannelStoreAndRetrieve(long size) throws IOException {
		File temp = new File(System.getProperty("java.io.tmpdir"));

		String fileName = "fc.bf";
		File file = new File(temp, fileName);
		Path path = file.toPath();

		LOGGER.info(String.format("Path: %s ", path));
		if (Files.exists(path)) {
			Files.delete(path);
		}

		long start = System.nanoTime(), end;

		int blocksize = 1000;

		ByteBuffer indexBuffer = ByteBuffer.allocateDirect(blocksize);
		indexBuffer.order(ByteOrder.LITTLE_ENDIAN);

		short v;

		LittleEndianRandomAccessFile raf = new LittleEndianRandomAccessFile(file, "rw");

		int transferSize = 1024;
		int transferCount = 0;
		byte[] chunk = new byte[transferSize];

		double fullCycles = size / blocksize;
		double overflow = size % blocksize;

		LOGGER.info(String.format("Size: %s BlockSize: %s", size, blocksize));
		LOGGER.info(String.format("Full Cycles: %s Overflow: %s", fullCycles, overflow));

		long transferCounter = 0;

		for (int i = 0; i < fullCycles; i++) {
			indexBuffer.rewind();
			for (int j = 0; j < (blocksize / 2); j++) {
				v = (short) (1000 * Math.random());
				indexBuffer.putShort(v);
			}
			indexBuffer.flip();

			while (indexBuffer.position() < indexBuffer.limit()) {
				transferCount = Math.min(transferSize, indexBuffer.limit() - indexBuffer.position());
				indexBuffer.get(chunk, 0, transferCount);
				raf.write(chunk, 0, transferCount);
				transferCounter += transferCount;
			}
		}

		indexBuffer.rewind();
		for (int i = 0; i < (overflow / 2); i++) {
			v = (short) (1000 * Math.random());
			indexBuffer.putShort(v);
		}
		indexBuffer.flip();

		while (indexBuffer.position() < indexBuffer.limit()) {
			transferCount = Math.min(transferSize, indexBuffer.limit() - indexBuffer.position());
			indexBuffer.get(chunk, 0, transferCount);
			raf.write(chunk, 0, transferCount);
			transferCounter += transferCount;
		}

		// indexBuffer.putShort((short) 999);
		end = System.nanoTime();

		indexBuffer.clear();

		LOGGER.info(String.format("Allocation Took %sms", (end - start) / 1000000));
		LOGGER.info(String.format("Transfer Counter %s", transferCounter));
		int samples = 1000;
		double timeSum = 0;
		for (int i = 0; i < samples; i++) {
			int p = (int) Math.min(0, (Math.random() * (size - 2)));
			if (p % 2 != 0) {
				p += 1;
			}

			// System.out.println("[" + p + "]");
			start = System.nanoTime();
			raf.seek(p);
			v = raf.readShort();
			// System.out.println("[" + p + "]:" + v);

			end = System.nanoTime();
			value = v;
			timeSum += (end - start);
		}

		LOGGER.info("Checking Last Index");
		raf.seek(size - 2);
		v = raf.readShort();
		LOGGER.log(Level.INFO, "[{0}]:{1}", new Object[] { size - 2, v });

		LOGGER.info(String.format("Average Retrieval Time %sms", (timeSum / samples / 1000000)));

		raf.close();

		if (Files.exists(path)) {
			Files.delete(path);
		}
	}
}
