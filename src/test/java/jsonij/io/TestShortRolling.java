/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.io;

import jsonij.BitWiseException;
import jsonij.BitWiseUtils;

/**
 *
 * @author John
 */
public class TestShortRolling {

	public static void main(String[] args) throws BitWiseException {
		byte recycle = 127;
		byte notRecycle = 2;

		boolean bitFlagRecycle = (recycle & (0x1 << 8)) != 0;
		boolean bitFlagNotRecycle = (notRecycle & (0x1 << 8)) != 0;

		byte rotation = (byte) (0x1 << 3);
		byte extract = (byte) (recycle & rotation);
		byte rotate = (byte) (extract >> 3);

		byte first = (byte) ((recycle & (0x1 << 6)) >> 3);
		byte second = (byte) ((recycle & (0x1 << 5)) >> 3);
		byte third = (byte) ((recycle & (0x1 << 4)) >> 3);
		byte fourth = (byte) ((recycle & (0x1 << 3)) >> 3);

		byte recycleType = (byte) (((recycle & (0x1 << 6)) >> 3) | ((recycle & (0x1 << 5)) >> 3)
				| ((recycle & (0x1 << 4)) >> 3) | ((recycle & (0x1 << 3)) >> 3));

		String outputString = BitWiseUtils.convertToHexString(recycleType);

		String outputString2 = BitWiseUtils.convertToBinaryString((short) recycleType);
		String outputString3 = BitWiseUtils.convertToHexString((short) recycleType);
		short output = (1 & 0xff) << 8 | (1 & 0xff);

		byte testValue = BitWiseUtils.extractByteFromBits(recycle, 1, 4);
		boolean boolValue = BitWiseUtils.extractBooleanFromBits(recycle, 0);

		short pattern = (short) 0xC400;

		System.out.println("" + BitWiseUtils.convertToBinaryString((short) (recycleType)));
		System.out.println("" + BitWiseUtils.convertToHexString((short) (recycleType)));
	}

}
