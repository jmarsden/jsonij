/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import jsonij.parser.BSONParser;
import jsonij.parser.ParserException;
import jsonij.BSON;
import jsonij.Value;
import jsonij.io.LittleEndianInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class BSONTest {

	public BSONTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of parse method, of class BSON.
	 */
	@Test
	public void testParse1() throws ParserException, IOException {
		System.out.println("parse1");

		URL file = ClassLoader.class.getResource("/bson/test1.bson");
		if (file == null) {
			return;
		}
		InputStream inputStream = file.openStream();

		BSONParser bsonParser = new BSONParser(new LittleEndianInputStream(inputStream));

		BSON.Object<String, Value> document = bsonParser.parse();

		System.out.println(document.toJSON());

		document = bsonParser.parse();

		System.out.println(document.toJSON());
	}

	/**
	 * Test of parse method, of class BSON.
	 */
	@Test
	public void testParse2() throws IOException, ParserException {
		System.out.println("parse2");

		URL file = ClassLoader.class.getResource("/bson/test2.bson");
		if (file == null) {
			return;
		}
		InputStream inputStream = file.openStream();

		BSONParser bsonParser = new BSONParser(new LittleEndianInputStream(inputStream));

		BSON.Object<String, Value> document = bsonParser.parse();

		System.out.println("1:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("2:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("3:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("4:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("5:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("6:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("7:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("8:" + document.toJSON());

		document = bsonParser.parse();

		System.out.println("9:" + document.toJSON());
	}

	@Test
	public void testParse3() throws IOException, ParserException, BSONParserException {
		System.out.println("parse3");

		URL file = ClassLoader.class.getResource("/bson/test2.bson");
		if (file == null) {
			return;
		}
		InputStream inputStream = file.openStream();

		BSONParser bsonParser = new BSONParser(new LittleEndianInputStream(inputStream));

		BSON.Object<String, Value> document = bsonParser.parse(3);

		System.out.println("3:" + document.toJSON());

		document = bsonParser.parse(5);

		System.out.println("5:" + document.toJSON());

		try {
			document = bsonParser.parse(2);
			fail();
		} catch (BSONParserException be) {

		}
	}
}
