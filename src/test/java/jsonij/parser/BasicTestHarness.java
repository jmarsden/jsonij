/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import jsonij.Value;

public class BasicTestHarness {

	public static void main(String[] args) throws IOException, ParserException {
		URL file = ClassLoader.class.getResource("/json/citylots.json");
		if (file == null) {
			return;
		}
		InputStream inputStream = file.openStream();
		JSONParser jsonParser = new JSONParser(inputStream);
		Value value = jsonParser.parse();
		inputStream.close();
		int count = 50;

		long start = System.currentTimeMillis();

		for (int i = 0; i < count; i++) {
			inputStream = file.openStream();
			jsonParser = new JSONParser(inputStream);
			value = jsonParser.parse();
			inputStream.close();
		}

		long end = System.currentTimeMillis();
		System.out.println("Duration: " + (double) ((double) (end - start) / (double) count));
	}
}
