/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import jsonij.parser.JSONParser;
import jsonij.parser.ParserException;
import jsonij.Value;
import java.io.IOException;
import java.io.StringReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JSONParserTest {

	public JSONParserTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of validateBufferOrder method, of class BSONCodec.
	 */
	@Test
	public void testFlexibleParseKey() throws IOException, ParserException {
		System.out.println("testFlexibleParseKey");
		JSONParser jsonParser = new JSONParser();
		// jsonParser.dialect = JSONParser.Dialect.FLEXIBLE_RFC_4627;

		Value value;
		String string = "{    key dsf      : 'Value   '   , blah: 10  }";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		string = "[10    ,null,    true, 'blah']";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		string = "{test:'blah \" blah', meh:[1,2,3,4,5]}";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		string = "{test:blah}";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		string = "{test:blah, this is also a key: \r   and this    is a  \\t  value   , haha:10e-300}";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());
	}

	@Test
	public void testParseCommentString() throws IOException, ParserException {
		System.out.println("testParseCommentString");
		JSONParser jsonParser = new JSONParser();

		Value value;
		String string;

		string = "{v:\r\nval\r\n}";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("{\"v\":\"val\"}", value.toString());

		string = "-- Test comment\r\n\n{\n\t--Value\nvalue\r\n:\r\nblah\r\n}\r\n\r\n";
		value = jsonParser.parse(string);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("{\"value\":\"blah\"}", value.toString());
	}

	@Test
	public void testParseCommentReader() throws IOException, ParserException {
		System.out.println("testParseCommentReader");
		JSONParser jsonParser = new JSONParser();

		Value value;
		String string;

		string = "{v:\r\nval\r\n}";
		StringReader reader = new StringReader(string);
		value = jsonParser.parse(reader);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("val", value.get("v").getString());

		string = "\r\n------------------------\r\n-- Test comment\r\n------------------------\n\r{\t\t\t-- Open Object\n\tvalue : significantly \tmore     complex value!!! \t-- Set a Value!!!\r\n}\t\t\t-- Close Object\r\n\r\n------------------------\r\n-- File End\r\n------------------------\r\n";

		reader = new StringReader(string);
		value = jsonParser.parse(reader);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		String expected = "\"significantly \\tmore     complex value!!!\"";
		String actual = value.get("value").toJSON();

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testParseMultipleDocuments1() throws IOException, ParserException {
		System.out.println("testParseMultipleDocuments1");
		JSONParser jsonParser = new JSONParser();

		Value value;
		String string;

		string = "{}[]";
		StringReader reader = new StringReader(string);
		value = jsonParser.parse(reader);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("{}", value.getString());

		value = jsonParser.parse(reader);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("[]", value.getString());

		value = jsonParser.parse(reader);
		Assert.assertNull(value);
	}

	@Test
	public void testParseMultipleDocuments2() throws IOException, ParserException {
		System.out.println("testParseMultipleDocuments2");

		Value value;
		String string;

		string = "{}[][1,2,3,4]{\"value\":\"This is not art!\"}";
		StringReader reader = new StringReader(string);

		JSONParser jsonParser = new JSONParser(reader);

		value = jsonParser.parse(2);
		System.out.println("Input:\t" + string);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("[]", value.getString());

		value = jsonParser.parse(4);
		System.out.println("Output:\t" + value.toString());

		Assert.assertEquals("{\"value\":\"This is not art!\"}", value.getString());
	}
}
