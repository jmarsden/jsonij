/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import jsonij.parser.ReaderJSONReader;
import jsonij.parser.BaseJSONReader;
import jsonij.parser.StringJSONReader;
import jsonij.parser.JSONReader;
import jsonij.parser.ParserException;
import java.io.IOException;
import java.io.StringReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ReaderCommentNewLineTest {

	public ReaderCommentNewLineTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testParseProblematicReader() throws IOException, ParserException {
		System.out.println("testParseProblematicReader");

		String string = "\r\nblah\r\n";
		ReaderJSONReader reader = new ReaderJSONReader(new StringReader(string));

		JSONReader reader2 = reader.getStringReader(BaseJSONReader.Mode.UN_QUOTED_READING);

		Assert.assertEquals('b', reader2.peek());
		Assert.assertEquals('b', reader2.read());
		Assert.assertEquals('l', reader2.peek());
		Assert.assertEquals('l', reader2.read());
		Assert.assertEquals('a', reader2.peek());
		Assert.assertEquals('a', reader2.read());
		Assert.assertEquals('h', reader2.peek());
		Assert.assertEquals('h', reader2.read());

		Assert.assertEquals(-1, reader2.peek());
		Assert.assertEquals(-1, reader2.read());
	}

	@Test
	public void testParseProblematicString() throws IOException, ParserException {
		System.out.println("testParseProblematicString");

		String string = "\r\nblah\r\n";
		StringJSONReader reader = new StringJSONReader(string);

		JSONReader reader2 = reader.getStringReader(BaseJSONReader.Mode.UN_QUOTED_READING);

		Assert.assertEquals('b', reader2.peek());
		Assert.assertEquals('b', reader2.read());
		Assert.assertEquals('l', reader2.peek());
		Assert.assertEquals('l', reader2.read());
		Assert.assertEquals('a', reader2.peek());
		Assert.assertEquals('a', reader2.read());
		Assert.assertEquals('h', reader2.peek());
		Assert.assertEquals('h', reader2.read());

		Assert.assertEquals(-1, reader2.peek());
		Assert.assertEquals(-1, reader2.read());
	}

	@Test
	public void testParseCase1() throws IOException, ParserException {
		System.out.println("testParseCase1");

		String string = "\r--Comment\r\n[]--Comment\r\n--Comment";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseCase2() throws IOException, ParserException {
		System.out.println("testParseCase2");

		String string = "\n\r--Comment\r\n[]";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseCase3() throws IOException, ParserException {
		System.out.println("testParseCase3");

		String string = "--Comment\r\n[\n]\n--Comment";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseCase4() throws IOException, ParserException {
		System.out.println("testParseCase4");

		String string = "------------\r\n\r--Comment--Comment--Comment\r\r\r\r--Comment\n\r\n--Comment\r\n[]--Comment\r\n--Comment------------------";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseCase5() throws IOException, ParserException {
		System.out.println("testParseCase5");

		String string = "------------\r\n\r--Comment\t--\tComment\t--Comment\r\r\r\r\t--Comment\n\r\n--Comment\r\n\t[\t]\t--Comment\r\n--C\to\tm\tm\te\tn\tt------------------\t";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseProblematicCommentReader() throws IOException, ParserException {
		System.out.println("testParseProblematicCommentReader");

		String string = "\r\n------------------------\r\n-- Test comment\r\n------------------------\r\n\r\n\n{\n\t--Value\nvalue\r\n:\r\nblah\r\n}\r\n\r\n--- File End\r\n";
		ReaderJSONReader reader = new ReaderJSONReader(new StringReader(string));

		Assert.assertEquals('{', reader.peek());
		Assert.assertEquals('{', reader.read());
		Assert.assertEquals('v', reader.peek());
		Assert.assertEquals('v', reader.read());
		Assert.assertEquals('a', reader.peek());
		Assert.assertEquals('a', reader.read());
		Assert.assertEquals('l', reader.peek());
		Assert.assertEquals('l', reader.read());
		Assert.assertEquals('u', reader.peek());
		Assert.assertEquals('u', reader.read());
		Assert.assertEquals('e', reader.peek());
		Assert.assertEquals('e', reader.read());
		Assert.assertEquals(':', reader.peek());
		Assert.assertEquals(':', reader.read());
		Assert.assertEquals('b', reader.peek());
		Assert.assertEquals('b', reader.read());
		Assert.assertEquals('l', reader.peek());
		Assert.assertEquals('l', reader.read());
		Assert.assertEquals('a', reader.peek());
		Assert.assertEquals('a', reader.read());
		Assert.assertEquals('h', reader.peek());
		Assert.assertEquals('h', reader.read());
		Assert.assertEquals('}', reader.peek());
		Assert.assertEquals('}', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseProblematicCommentString() throws IOException, ParserException {
		System.out.println("testParseProblematicCommentString");

		String string = "\r\n------------------------\r\n-- Test comment\r\n------------------------\r\n\r\n\n{\n\t--Value\nvalue\r\n:\r\nblah\r\n}\r\n\r\n-- File End\r\n";
		StringJSONReader reader = new StringJSONReader(string);

		Assert.assertEquals('{', reader.peek());
		Assert.assertEquals('{', reader.read());
		Assert.assertEquals('v', reader.peek());
		Assert.assertEquals('v', reader.read());
		Assert.assertEquals('a', reader.peek());
		Assert.assertEquals('a', reader.read());
		Assert.assertEquals('l', reader.peek());
		Assert.assertEquals('l', reader.read());
		Assert.assertEquals('u', reader.peek());
		Assert.assertEquals('u', reader.read());
		Assert.assertEquals('e', reader.peek());
		Assert.assertEquals('e', reader.read());
		Assert.assertEquals(':', reader.peek());
		Assert.assertEquals(':', reader.read());
		Assert.assertEquals('b', reader.peek());
		Assert.assertEquals('b', reader.read());
		Assert.assertEquals('l', reader.peek());
		Assert.assertEquals('l', reader.read());
		Assert.assertEquals('a', reader.peek());
		Assert.assertEquals('a', reader.read());
		Assert.assertEquals('h', reader.peek());
		Assert.assertEquals('h', reader.read());
		Assert.assertEquals('}', reader.peek());
		Assert.assertEquals('}', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseNumericCase1() throws IOException, ParserException {
		System.out.println("testParseNumericCase1");

		String string = "\r--Comment\r\n[1e-3]--Comment\r\n--Comment";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals('1', reader.peek());
		Assert.assertEquals('1', reader.read());
		Assert.assertEquals('e', reader.peek());
		Assert.assertEquals('e', reader.read());
		Assert.assertEquals('-', reader.peek());
		Assert.assertEquals('-', reader.read());
		Assert.assertEquals('3', reader.peek());
		Assert.assertEquals('3', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals('1', reader.peek());
		Assert.assertEquals('1', reader.read());
		Assert.assertEquals('e', reader.peek());
		Assert.assertEquals('e', reader.read());
		Assert.assertEquals('-', reader.peek());
		Assert.assertEquals('-', reader.read());
		Assert.assertEquals('3', reader.peek());
		Assert.assertEquals('3', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseNumericCase2() throws IOException, ParserException {
		System.out.println("testParseNumericCase2");

		String string = "\r--Comment\r\n[1\te -\t\t3]--Comment\r\n--Comment";

		JSONReader reader = new ReaderJSONReader(new StringReader(string));
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals('1', reader.peek());
		Assert.assertEquals('1', reader.read());
		Assert.assertEquals('e', reader.peek());
		Assert.assertEquals('e', reader.read());
		Assert.assertEquals('-', reader.peek());
		Assert.assertEquals('-', reader.read());
		Assert.assertEquals('3', reader.peek());
		Assert.assertEquals('3', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		Assert.assertEquals('[', reader.peek());
		Assert.assertEquals('[', reader.read());
		Assert.assertEquals('1', reader.peek());
		Assert.assertEquals('1', reader.read());
		Assert.assertEquals('e', reader.peek());
		Assert.assertEquals('e', reader.read());
		Assert.assertEquals('-', reader.peek());
		Assert.assertEquals('-', reader.read());
		Assert.assertEquals('3', reader.peek());
		Assert.assertEquals('3', reader.read());
		Assert.assertEquals(']', reader.peek());
		Assert.assertEquals(']', reader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseStringReaderCase1() throws IOException, ParserException {
		System.out.println("testParseStringReaderCase1");

		String string = " bl  ah \t\t\t-- Set a Value!!!\r\n";

		BaseJSONReader reader = new ReaderJSONReader(new StringReader(string));
		JSONReader stringReader = reader.getStringReader(BaseJSONReader.Mode.UN_QUOTED_READING);
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('b', stringReader.peek());
		Assert.assertEquals('b', stringReader.read());
		Assert.assertEquals('l', stringReader.peek());
		Assert.assertEquals('l', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('a', stringReader.peek());
		Assert.assertEquals('a', stringReader.read());
		Assert.assertEquals('h', stringReader.peek());
		Assert.assertEquals('h', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		stringReader = reader.getStringReader(BaseJSONReader.Mode.UN_QUOTED_READING);
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('b', stringReader.peek());
		Assert.assertEquals('b', stringReader.read());
		Assert.assertEquals('l', stringReader.peek());
		Assert.assertEquals('l', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('a', stringReader.peek());
		Assert.assertEquals('a', stringReader.read());
		Assert.assertEquals('h', stringReader.peek());
		Assert.assertEquals('h', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}

	@Test
	public void testParseStringReaderCase2() throws IOException, ParserException {
		System.out.println("testParseStringReaderCase2");

		String string = "\r\n\n-- Comment\r\n bl  ah \t\t\t-- Set a Value!!!\r\n";

		BaseJSONReader reader = new ReaderJSONReader(new StringReader(string));
		JSONReader stringReader = reader.getStringReader(BaseJSONReader.Mode.UN_QUOTED_READING);
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('b', stringReader.peek());
		Assert.assertEquals('b', stringReader.read());
		Assert.assertEquals('l', stringReader.peek());
		Assert.assertEquals('l', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('a', stringReader.peek());
		Assert.assertEquals('a', stringReader.read());
		Assert.assertEquals('h', stringReader.peek());
		Assert.assertEquals('h', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());

		reader = new StringJSONReader(string);
		stringReader = reader.getStringReader(BaseJSONReader.Mode.UN_QUOTED_READING);
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('b', stringReader.peek());
		Assert.assertEquals('b', stringReader.read());
		Assert.assertEquals('l', stringReader.peek());
		Assert.assertEquals('l', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals('a', stringReader.peek());
		Assert.assertEquals('a', stringReader.read());
		Assert.assertEquals('h', stringReader.peek());
		Assert.assertEquals('h', stringReader.read());
		Assert.assertEquals(' ', stringReader.peek());
		Assert.assertEquals(' ', stringReader.read());
		Assert.assertEquals(-1, reader.peek());
		Assert.assertEquals(-1, reader.read());
	}
}
