/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.parser;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import jsonij.Value;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JSONFileParserTest {

	public JSONFileParserTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testFile1() throws IOException, ParserException {
		System.out.println("testFile1");
		URL file = ClassLoader.class.getResource("/json/people.json");
		if (file != null) {
			InputStream inputStream = file.openStream();
			long start = System.nanoTime();
			JSONParser jsonParser = new JSONParser(inputStream);
			Value value = jsonParser.parse();
			long end = System.nanoTime();
			System.out.println("Size:" + value.size() + " " + ((end - start) / 1000000));
		}
	}

	@Test
	public void testFile2() throws IOException, ParserException {
		System.out.println("testFile2");
		URL file = ClassLoader.class.getResource("/json/citylots.json");
		if (file != null) {
			InputStream inputStream = file.openStream();
			long start = System.nanoTime();
			JSONParser jsonParser = new JSONParser(inputStream);
			Value value = jsonParser.parse();
			long end = System.nanoTime();
			System.out.println("Size:" + value.size() + " " + ((end - start) / 1000000));
		}

	}
}
