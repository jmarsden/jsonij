/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.ubjson;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import jsonij.UBJSON;
import jsonij.UBJSONCodec;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UBJSONBooleanTest {

	UBJSONCodec codec = new UBJSONCodec();
	byte[] trueValue = new byte[] { 'T' };
	byte[] falseValue = new byte[] { 'F' };

	public ByteBuffer getBuffer() {
		ByteBuffer buffer = ByteBuffer.allocate(20);
		buffer.order(ByteOrder.BIG_ENDIAN);
		return buffer;
	}

	@Test
	public void testDecodeTrue() {
		ByteBuffer buffer = getBuffer();
		buffer.put(trueValue);
		buffer.rewind();
		boolean trueValue = codec.decodeUBJSONBoolean(buffer);
		assertEquals(true, trueValue);
	}

	@Test
	public void testDecodeFalse() {
		ByteBuffer buffer = getBuffer();
		buffer.put(falseValue);
		buffer.rewind();
		boolean falseValue = codec.decodeUBJSONBoolean(buffer);
		assertEquals(false, falseValue);
	}

	@Test
	public void testEncodeTrue() {
		UBJSON.True trueBSON = UBJSON.TRUE;
		ByteBuffer buffer = getBuffer();
		trueBSON.toUniversalBinaryJSON(buffer);
		buffer.rewind();
		byte[] result = new byte[1];
		buffer.get(result);
		assertArrayEquals(trueValue, result);
		assertEquals(1, trueBSON.nestedSize());
	}

	@Test
	public void testEncodeFalse() {
		UBJSON.False falseBSON = UBJSON.FALSE;
		ByteBuffer buffer = getBuffer();
		falseBSON.toUniversalBinaryJSON(buffer);
		buffer.rewind();
		byte[] result = new byte[1];
		buffer.get(result);
		assertArrayEquals(falseValue, result);
		assertEquals(1, falseBSON.nestedSize());
	}
}
