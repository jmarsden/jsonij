/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.ubjson;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import jsonij.UBJSON;
import jsonij.UBJSONCodec;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UBJSONNullTest {

	UBJSONCodec codec = new UBJSONCodec();
	byte[] nullValue = new byte[] { 'Z' };

	public ByteBuffer getBuffer() {
		ByteBuffer buffer = ByteBuffer.allocate(20);
		buffer.order(ByteOrder.BIG_ENDIAN);
		return buffer;
	}

	@Test
	public void testDecodeNull() {
		ByteBuffer buffer = getBuffer();
		buffer.put(nullValue);
		buffer.rewind();
		byte nullValue = codec.decodeUBJSONNull(buffer);
		assertEquals('Z', nullValue);
	}


	@Test
	public void testEncodeNull() {
		UBJSON.Null nullUBJSON = UBJSON.NULL;
		ByteBuffer buffer = getBuffer();
		nullUBJSON.toUniversalBinaryJSON(buffer);
		buffer.rewind();
		byte[] result = new byte[1];
		buffer.get(result);
		assertArrayEquals(nullValue, result);
		assertEquals(0, nullUBJSON.nestedSize());
	}
}
