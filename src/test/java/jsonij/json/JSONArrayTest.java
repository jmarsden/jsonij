/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.json;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jsonij.JSON;
import jsonij.Value;
import static org.junit.Assert.*;

/**
 * JSON.Array Type Tests
 */
public class JSONArrayTest {

	public JSONArrayTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testConstruct() {
		System.out.println("construct");
		JSON.Object<JSON.String, Value> value1 = new JSON.Object<JSON.String, Value>();
		value1.put(new JSON.String("attribute1"), new JSON.Numeric(33));
		value1.put(new JSON.String("attribute2"), new JSON.String("value"));
		JSON.Array<Value> value2 = new JSON.Array<Value>();
		value2.add(new JSON.Numeric(10));
		value2.add(new JSON.String("value"));
		value2.add(JSON.TRUE);
		value2.add(JSON.FALSE);
		value2.add(JSON.NULL);
		value2.add(value1);
		System.out.println(value2.toJSON());
	}

	@Test
	public void testEquals() {
		System.out.println("equals");
		JSON.Object<JSON.String, Value> value1 = new JSON.Object<JSON.String, Value>();
		value1.put(new JSON.String("attribute1"), new JSON.Numeric(33));
		value1.put(new JSON.String("attribute2"), new JSON.String("value"));
		JSON.Array<Value> value2 = new JSON.Array<Value>();
		value2.add(new JSON.Numeric(10));
		value2.add(new JSON.String("value"));
		value2.add(JSON.TRUE);
		value2.add(JSON.FALSE);
		value2.add(JSON.NULL);
		value2.add(value1);

		JSON.Object<JSON.String, Value> value3 = new JSON.Object<JSON.String, Value>();
		value3.put(new JSON.String("attribute1"), new JSON.Numeric(33));
		value3.put(new JSON.String("attribute2"), new JSON.String("value"));
		JSON.Array<Value> value4 = new JSON.Array<Value>();
		value4.add(new JSON.Numeric(10));
		value4.add(new JSON.String("value"));
		value4.add(JSON.TRUE);
		value4.add(JSON.FALSE);
		value4.add(JSON.NULL);
		value4.add(value3);

		assertEquals(value2, value4);
	}

	@Test
	public void testToJSON() {
		System.out.println("toJSON");
		JSON.Object<JSON.String, Value> value1 = new JSON.Object<JSON.String, Value>();
		value1.put(new JSON.String("attribute1"), new JSON.Numeric(33));
		value1.put(new JSON.String("attribute2"), new JSON.String("value"));
		JSON.Array<Value> value2 = new JSON.Array<Value>();
		value2.add(new JSON.Numeric(10));
		value2.add(new JSON.String("value"));
		value2.add(JSON.TRUE);
		value2.add(JSON.FALSE);
		value2.add(JSON.NULL);
		value2.add(value1);
		assertEquals(value2.toString(), "[10,\"value\",true,false,null,{\"attribute1\":33,\"attribute2\":\"value\"}]");
	}

	@Test
	public void testToString() {
		System.out.println("toString");
		JSON.Object<JSON.String, Value> value1 = new JSON.Object<JSON.String, Value>();
		value1.put(new JSON.String("attribute1"), new JSON.Numeric(33));
		value1.put(new JSON.String("attribute2"), new JSON.String("value"));
		JSON.Array<Value> value2 = new JSON.Array<Value>();
		value2.add(new JSON.Numeric(10));
		value2.add(new JSON.String("value"));
		value2.add(JSON.TRUE);
		value2.add(JSON.FALSE);
		value2.add(JSON.NULL);
		value2.add(value1);
		assertEquals(value2.toString(), "[10,\"value\",true,false,null,{\"attribute1\":33,\"attribute2\":\"value\"}]");
	}
}