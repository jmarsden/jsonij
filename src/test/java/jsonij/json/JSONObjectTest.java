/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.json;

import jsonij.BSON;
import jsonij.ObjectIdentifier;
import jsonij.JSON;
import jsonij.Value;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JSON.Object Type Tests
 */
public class JSONObjectTest {

	public JSONObjectTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testConstruct() {
		System.out.println("construct");
		JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
		value1.put("attribute1", JSON.NULL);
		value1.put("attribute2", JSON.TRUE);
		value1.put("attribute3", JSON.FALSE);
		value1.put("attribute4", new JSON.Numeric(33.0));
		value1.put("attribute5", new JSON.String("I am a String"));
		value1.put("attribute6", new JSON.Object<String, Value>());
	}

	@Test
	public void testEquals() {
		System.out.println("equals");
		JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
		value1.put("attribute1", JSON.NULL);
		value1.put("attribute2", JSON.TRUE);
		value1.put("attribute3", JSON.FALSE);
		value1.put("attribute4", new JSON.Numeric(33.0));
		value1.put("attribute5", new JSON.String("I am a String"));
		value1.put("attribute6", new JSON.Object<String, Value>());

		JSON.Object<String, Value> value2 = new JSON.Object<String, Value>();
		value2.put("attribute6", new JSON.Object<String, Value>());
		value2.put("attribute5", new JSON.String("I am a String"));
		value2.put("attribute4", new JSON.Numeric(33.0));
		value2.put("attribute1", JSON.NULL);
		value2.put("attribute2", JSON.TRUE);
		value2.put("attribute3", JSON.FALSE);

		assertEquals(value1, value2);
	}

	@Test
	public void testToJSON() {
		System.out.println("toJSON");
		JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
		value1.put("attribute1", JSON.NULL);
		value1.put("attribute2", JSON.TRUE);
		value1.put("attribute3", JSON.FALSE);
		value1.put("attribute4", new JSON.Numeric(33.0));
		value1.put("attribute5", new JSON.String("I am a String"));
		value1.put("attribute6", new JSON.Object<String, Value>());
		System.out.println(value1.toJSON());
		assertEquals(value1.toJSON(), "{\"attribute1\":null,\"attribute2\":true,\"attribute3\":false,\"attribute4\":33.0,\"attribute5\":\"I am a String\",\"attribute6\":{}}");
	}

	@Test
	public void testToString() {
		System.out.println("toString");
		JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
		value1.put("attribute1", JSON.NULL);
		value1.put("attribute2", JSON.TRUE);
		value1.put("attribute3", JSON.FALSE);
		value1.put("attribute4", new JSON.Numeric(33.0));
		value1.put("attribute5", new JSON.String("I am a String"));
		value1.put("attribute6", new JSON.Object<String, Value>());
		System.out.println(value1.toString());
		assertEquals(value1.toString(), "{\"attribute1\":null,\"attribute2\":true,\"attribute3\":false,\"attribute4\":33.0,\"attribute5\":\"I am a String\",\"attribute6\":{}}");
	}

	@Test
	public void testGet() {
		System.out.println("get");
		JSON.Object<String, Value> value1 = new JSON.Object<String, Value>();
		value1.put("attribute1", JSON.NULL);
		value1.put("attribute2", JSON.TRUE);
		value1.put("attribute3", JSON.FALSE);
		value1.put("attribute4", new JSON.Numeric(33.0));
		value1.put("attribute5", new JSON.String("I am a String"));
		value1.put("attribute6", new JSON.Object<String, Value>());

		assertEquals(value1.get("attribute3"), JSON.FALSE);
		assertEquals(value1.get(new JSON.String("attribute3")), JSON.FALSE);

		JSON.Object<JSON.String, Value> value2 = new JSON.Object<JSON.String, Value>();
		value2.put(new JSON.String("attribute6"), new JSON.Object<String, Value>());
		value2.put(new JSON.String("attribute5"), new JSON.String("I am a String"));
		value2.put(new JSON.String("attribute4"), new JSON.Numeric(33.0));
		value2.put(new JSON.String("attribute1"), JSON.NULL);
		value2.put(new JSON.String("attribute2"), JSON.TRUE);
		value2.put(new JSON.String("attribute3"), JSON.FALSE);

		assertEquals(value2.get(new JSON.String("attribute5")), "I am a String");
		assertEquals(value2.get("attribute5"), new JSON.String("I am a String"));

		assertEquals(value1, value2);
	}

	@Test
	public void testToBSONBasic() {
		System.out.println("testToBSONBasic");
		BSON.Object<String, Value> objectValue = new BSON.Object<String, Value>();
		objectValue.put("_id", new BSON.ObjectID(new ObjectIdentifier("5466fc7435519ab66899598d")));
		objectValue.put("name", new BSON.String("mongo"));

		ByteBuffer buffer = BSON.allocateBuffer(200, false);

		objectValue.toBSON(buffer);

		String hex = "26 00 00 00 07 5f 69 64 00 54 66 fc 74 35 51 9a b6 68 99 59 8d 02 6e 61 6d 65 00 06 00 00 00 6d 6f 6e 67 6f 00 00";
		byte[] hexByteArray = BSON.hexStringToByteArray(hex);
		ByteBuffer parsedBuffer = BSON.allocateBuffer(Array.getLength(hexByteArray));

		System.out.println(objectValue.toJSON());
		System.out.println(BSON.CODEC.byteBufferToHexString(buffer));
		System.out.println(hex);

		assertEquals(hex, BSON.CODEC.byteBufferToHexString(buffer));
	}
}