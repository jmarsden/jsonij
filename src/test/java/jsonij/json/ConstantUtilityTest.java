/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.json;

import jsonij.ConstantUtility;
import junit.framework.TestCase;

/**
 * ConstantUtility Test Cases
 * 
 * @author jmarsden@plural.cc
 */
public class ConstantUtilityTest extends TestCase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link jsonij.ConstantUtility#isDigitChar(char)}.
	 */
	public void testIsDigitChar() {
		String testCaseString = "J0hn M45d3n\r";
		boolean[] testCaseResults = new boolean[] { false, true, false, false, false, false, true, true, false, true, false, false };
		int i = 0;
		for (char c : testCaseString.toCharArray()) {
			boolean expected = testCaseResults[i++];
			boolean result = ConstantUtility.isDigit(c);
			assertEquals(String.format("Incorrect Result for Char '%s' Expected '%s' Result '%s'", String.valueOf(c), expected, result), expected, result);
		}
	}

	/**
	 * Test method for {@link jsonij.ConstantUtility#testIsHexDigitChar(char)}.
	 */
	public void testIsHexDigitChar() {
		String testCaseString = "J0hf M45den\r";
		boolean[] testCaseResults = new boolean[] { false, true, false, true, false, false, true, true, true, true, false, false };
		int i = 0;
		for (char c : testCaseString.toCharArray()) {
			boolean expected = testCaseResults[i++];
			boolean result = ConstantUtility.isHexDigit(c);
			assertEquals(String.format("Incorrect Result for Char '%s' Expected '%s' Result '%s'", String.valueOf(c), expected, result), expected, result);
		}
	}

	/**
	 * Test method for {@link jsonij.ConstantUtility#isWhiteSpace(int)}.
	 */
	public void testIsWhiteSpaceInt() {
		String testCaseString = "W\rhi\te Sp\n";
		boolean[] testCaseResults = new boolean[] { false, false, false, false, true, false, true, false, false, false };
		int i = 0;
		for (char c : testCaseString.toCharArray()) {
			int r = (int) c;
			boolean expected = testCaseResults[i++];
			boolean result = ConstantUtility.isWhiteSpace(r);
			assertEquals(String.format("Incorrect Result for Int '%s' ('%s') Expected '%s' Result '%s'", String.valueOf(r), String.valueOf(c), expected, result), expected, result);
		}
	}

	/**
	 * Test method for {@link jsonij.ConstantUtility#isWhiteSpace(char)}.
	 */
	public void testIsWhiteSpaceChar() {
		String testCaseString = "W\rhi\te Sp\n\t";
		boolean[] testCaseResults = new boolean[] { false, false, false, false, true, false, true, false, false, false, true };
		int i = 0;
		for (char c : testCaseString.toCharArray()) {
			boolean expected = testCaseResults[i++];
			boolean result = ConstantUtility.isWhiteSpace(c);
			assertEquals(String.format("Incorrect Result for Char '%s' Expected '%s' Result '%s'", Integer.toHexString(c), expected, result), expected, result);
		}
	}
}
