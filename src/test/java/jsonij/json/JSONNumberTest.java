/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.json;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jsonij.JSON;
import jsonij.Value;
import static org.junit.Assert.*;

/**
 * JSON.Number Type Tests
 */
public class JSONNumberTest {

	public JSONNumberTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testConstruct() {
		System.out.println("construct");
		new JSON.Numeric((Number) null);
		new JSON.Numeric(0);
		new JSON.Numeric(1337F);
		new JSON.Numeric(-0.001);
		new JSON.Numeric(1000e23D);
		new JSON.Numeric(1234567890);
		new JSON.Numeric(new Long(98765432));
		new JSON.Numeric(new BigDecimal("123451234512345123451234512345.5432154321543215432154321543215432154321"));
	}

	@Test
	public void testEqualsZero() {
		System.out.println("equalsZero");

		Value value1 = new JSON.Numeric((Number) null);
		Value value2 = new JSON.Numeric(0);
		Value value3 = new JSON.Numeric(0.0D);

		assertEquals(value1, value2);
		assertEquals(value1, value3);
		assertEquals(value2, value3);

		Value value4 = new JSON.Numeric(-0.0D);

		assertEquals(value1, value4);
		assertEquals(value2, value4);
		assertEquals(value3, value4);

		Value value5 = new JSON.Numeric(new Float(0F));

		assertEquals(value1, value5);
		assertEquals(value2, value5);
		assertEquals(value3, value5);
		assertEquals(value4, value5);

		Value value6 = new JSON.Numeric(-0.0e00D);

		assertEquals(value1, value6);
		assertEquals(value2, value6);
		assertEquals(value3, value6);
		assertEquals(value4, value6);
		assertEquals(value5, value6);

		Value value7 = new JSON.Numeric(new BigDecimal("0"));
		assertEquals(value1, value7);
		assertEquals(value2, value7);
		assertEquals(value3, value7);
		assertEquals(value4, value7);
		assertEquals(value5, value7);
		assertEquals(value6, value7);
	}

	@Test
	public void testEqualsOne() {
		System.out.println("equalsOne");

		Value value1 = new JSON.Numeric(1F);
		Value value2 = new JSON.Numeric(1);
		Value value3 = new JSON.Numeric(1.0D);

		assertEquals(value1, value2);
		assertEquals(value1, value3);
		assertEquals(value2, value3);

		Value value4 = new JSON.Numeric(new Double(1.0));

		assertEquals(value1, value4);
		assertEquals(value2, value4);
		assertEquals(value3, value4);

		Value value5 = new JSON.Numeric(new Float(1F));

		assertEquals(value1, value5);
		assertEquals(value2, value5);
		assertEquals(value3, value5);
		assertEquals(value4, value5);

		Value value6 = new JSON.Numeric(1.0e00D);

		assertEquals(value1, value6);
		assertEquals(value2, value6);
		assertEquals(value3, value6);
		assertEquals(value4, value6);
		assertEquals(value5, value6);

		Value value7 = new JSON.Numeric(new BigDecimal("1"));
		assertEquals(value1, value7);
		assertEquals(value2, value7);
		assertEquals(value3, value7);
		assertEquals(value4, value7);
		assertEquals(value5, value7);
		assertEquals(value6, value7);
	}

	@Test
	public void testEqualsZeroZeroOne() {
		System.out.println("equalsZeroZeroOne");

		Value value1 = new JSON.Numeric(0.001F);
		Value value2 = new JSON.Numeric(0.001);
		Value value3 = new JSON.Numeric(0.001D);

		assertEquals(value1.getDouble(), value1.getDouble(), 0.0001);
		assertEquals(value1.getDouble(), value3.getDouble(), 0.0001);
		assertEquals(value2.getDouble(), value3.getDouble(), 0.0001);

		Value value4 = new JSON.Numeric(0.001);

		assertEquals(value1.getDouble(), value4.getDouble(), 0.0001);
		assertEquals(value2.getDouble(), value4.getDouble(), 0.0001);
		assertEquals(value3.getDouble(), value4.getDouble(), 0.0001);

		Value value5 = new JSON.Numeric(new Float(0.001));

		assertEquals(value1.getDouble(), value5.getDouble(), 0.0001);
		assertEquals(value2.getDouble(), value5.getDouble(), 0.0001);
		assertEquals(value3.getDouble(), value5.getDouble(), 0.0001);
		assertEquals(value4.getDouble(), value5.getDouble(), 0.0001);

		Value value6 = new JSON.Numeric(0.001);

		assertEquals(value1.getDouble(), value6.getDouble(), 0.0001);
		assertEquals(value2.getDouble(), value6.getDouble(), 0.0001);
		assertEquals(value3.getDouble(), value6.getDouble(), 0.0001);
		assertEquals(value4.getDouble(), value6.getDouble(), 0.0001);
		assertEquals(value5.getDouble(), value6.getDouble(), 0.0001);

		Value value7 = new JSON.Numeric(new BigDecimal("0.001"));
		assertEquals(value1.getDouble(), value7.getDouble(), 0.0001);
		assertEquals(value2.getDouble(), value7.getDouble(), 0.0001);
		assertEquals(value3.getDouble(), value7.getDouble(), 0.0001);
		assertEquals(value4.getDouble(), value7.getDouble(), 0.0001);
		assertEquals(value5.getDouble(), value7.getDouble(), 0.0001);
		assertEquals(value6.getDouble(), value7.getDouble(), 0.0001);
	}

	@Test
	public void testToJSON() {
		System.out.println("toJSON");
		Value value1 = new JSON.Numeric((Number) null);
		assertEquals(value1.toJSON(), "0");
		Value value2 = new JSON.Numeric(0);
		assertEquals(value2.toJSON(), "0");
		Value value3 = new JSON.Numeric(1337F);
		assertEquals(value3.toJSON(), "1337.0");
		Value value4 = new JSON.Numeric(-0.001);
		assertEquals(value4.toJSON(), "-0.001");
		Value value5 = new JSON.Numeric(1000e23D);
		assertEquals(value5.toJSON(), "1.0E26");
		Value value6 = new JSON.Numeric(1234567890);
		assertEquals(value6.toJSON(), "1234567890");
		Value value7 = new JSON.Numeric(new Long(98765432));
		assertEquals(value7.toJSON(), "98765432");
		Value value8 = new JSON.Numeric(new BigDecimal("123451234512345123451234512345.5432154321543215432154321543215432154321"));
		assertEquals(value8.toJSON(), "123451234512345123451234512345.5432154321543215432154321543215432154321");
	}

	@Test
	public void testToString() {
		System.out.println("toString");
		Value value1 = new JSON.Numeric((Number) null);
		assertEquals(value1.toJSON(), "0");
		Value value2 = new JSON.Numeric(0);
		assertEquals(value2.toJSON(), "0");
		Value value3 = new JSON.Numeric(1337F);
		assertEquals(value3.toJSON(), "1337.0");
		Value value4 = new JSON.Numeric(-0.001);
		assertEquals(value4.toJSON(), "-0.001");
		Value value5 = new JSON.Numeric(1000e23D);
		assertEquals(value5.toJSON(), "1.0E26");
		Value value6 = new JSON.Numeric(1234567890);
		assertEquals(value6.toJSON(), "1234567890");
		Value value7 = new JSON.Numeric(new Long(98765432));
		assertEquals(value7.toJSON(), "98765432");
		Value value8 = new JSON.Numeric(new BigDecimal("123451234512345123451234512345.5432154321543215432154321543215432154321"));
		assertEquals(value8.toJSON(), "123451234512345123451234512345.5432154321543215432154321543215432154321");
	}
}