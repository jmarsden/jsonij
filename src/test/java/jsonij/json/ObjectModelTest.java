/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.json;

import jsonij.JSON;
import jsonij.Value;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ObjectModelTest {

	public ObjectModelTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testCreateObject() {
		System.out.println("testCreateObject");
		JSON.Object<String, Value> jsonObject = new JSON.Object<String, Value>();
		jsonObject.put("First Name", "John");
		jsonObject.put("Last Name", "Marsden");
		jsonObject.put("City", "Sydney");
		jsonObject.put("Age", 37);
		jsonObject.put("Created this test", true);
		jsonObject.put("Thinks this is a good use of JSON", false);
		jsonObject.putNull("Political Alignment");
		JSON.Object<String, Value> objects = new JSON.Object<String, Value>();
		objects.put("transport", "car");
		objects.put("write", "pen");
		objects.putNull("beer");
		objects.put("coffee", 3);
		objects.put("write", "highlighter");
		jsonObject.put("Random Thoughts", objects);
		JSON.Array<Value> things = new JSON.Array<Value>();
		things.add("PIE");
		things.add(3.14159265359);
		things.addNull();
		things.add(true);
		jsonObject.put("Favorite Things", things);
		System.out.println(jsonObject.toJSON());
		String expected = "{\"First Name\":\"John\",\"Last Name\":\"Marsden\",\"City\":\"Sydney\",\"Age\":37,\"Created this test\":true,\"Thinks this is a good use of JSON\":false,\"Political Alignment\":null,\"Random Thoughts\":{\"transport\":\"car\",\"write\":\"highlighter\",\"beer\":null,\"coffee\":3},\"Favorite Things\":[\"PIE\",3.14159265359,null,true]}";
		String actual = jsonObject.toJSON();
		assertEquals(expected, actual);
	}
}
