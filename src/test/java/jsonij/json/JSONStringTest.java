/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2009-2019 J.W.Marsden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jsonij.json;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jsonij.JSON;
import jsonij.Value;
import static org.junit.Assert.*;

/**
 * JSON.String Type Tests
 */
public class JSONStringTest {

	public JSONStringTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testConstruct() {
		System.out.println("construct");
		new JSON.String(null);
		new JSON.String("Value 2");
		new JSON.String("this is a string with a tab \t character");
		new JSON.String("this is a string with a %7BURL-encoded%7D character");
	}

	@Test
	public void testEquals() {
		System.out.println("equals");
		Value value1 = new JSON.String(null);
		assertEquals(value1, new JSON.String(""));

		Value value2 = new JSON.String("Value 2");
		assertEquals(value2, new JSON.String("Value 2"));

		Value value3 = new JSON.String("this is a string with a tab \t character");
		assertEquals(value3, new JSON.String("this is a string with a tab \t character"));

		Value value4 = new JSON.String("this is a string with a %7BURL-encoded%7D character");
		assertEquals(value4.toJSON(), "\"this is a string with a %7BURL-encoded%7D character\"");

		assertNotSame(value1, value2);
		assertNotSame(value2, value3);
		assertNotSame(value3, value4);

		assertEquals(value1, "");
		assertEquals(value2, "Value 2");
		assertEquals(value3, "this is a string with a tab \t character");
		assertEquals(value4, "this is a string with a %7BURL-encoded%7D character");
	}

	@Test
	public void testToJSON() {
		System.out.println("toJSON");
		Value value1 = new JSON.String(null);
		assertEquals(value1.toJSON(), "\"\"");
		Value value2 = new JSON.String("Value 2");
		assertEquals(value2.toJSON(), "\"Value 2\"");
		Value value3 = new JSON.String("this is a string with a tab \t character");
		assertEquals(value3.toJSON(), "\"this is a string with a tab \\t character\"");
		Value value4 = new JSON.String("this is a string with a %7BURL-encoded%7D character");
		assertEquals(value4.toJSON(), "\"this is a string with a %7BURL-encoded%7D character\"");
	}

	@Test
	public void testToString() {
		System.out.println("toString");
		Value value1 = new JSON.String(null);
		assertEquals(value1.toString(), "");
		Value value2 = new JSON.String("Value 2");
		assertEquals(value2.toString(), "Value 2");
		Value value3 = new JSON.String("this is a string with a tab \t character");
		assertEquals(value3.toString(), "this is a string with a tab \t character");
		Value value4 = new JSON.String("this is a string with a %7BURL-encoded%7D character");
		assertEquals(value4.toString(), "this is a string with a %7BURL-encoded%7D character");
	}
}